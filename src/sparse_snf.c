/*
 * Input data is data dumped by lil_matrix_dump.
 * First line is matrix data that # and three numbers: ROW_SIZE COLUMN_SIZE and MODULO.
 * The rest of lines are row data that contains pairs of a column number and an element number.
 */

#include <argtable2.h>
#include "lilmatrix.h"
#include "intern.h"

typedef enum __mode {
  PRINT_BINARY_MATRIX,
  PRINT_MATRIX_STATS,
  REDUCE_BY_ROW_OPS,
  CALC_SNF
} MODE;


int main (int argc, char **argv)
{
  LIL_MATRIX *mat;
  LIL_SNF_ARG *snf_arg;
  LIL_MATRIX_DATA_FORMAT mat_format;
  MODE mode;
  const char *path_reduced;

  const char *progname = "sparse_snf";

  struct arg_lit *help = arg_lit0("h", "help", "Print this help and exit");
  struct arg_int *info_number = arg_int0("i", "info", "<num>", "Set the frequency of infomation output");
  struct arg_lit *precise_estimate = arg_lit0("c", "precise-estimate", "Estimate increase of elements precisely");
  struct arg_int *reduce_by_divisors = arg_int0("d", "reduce-by-divisors", "<num>", "Reduce elements by obtaining divisors");
  struct arg_int *reduce_at_increase = arg_int0("I", "reduce-by-increase", "<num>", "Reduce elements by increase of elements");
  struct arg_lit *auto_delete_duplicates = arg_lit0("D", "auto-reduction", "Reduce elements automatically");
  struct arg_lit *reduce_repeatedly = arg_lit0("r", "reduce-repeatedly", "Reduce elements repeatedly");
  struct arg_int *pivot_number = arg_int0("P", "min-pivot-number", "<num>", "Minimum pivot number of calculation");
  struct arg_int *estimate_increase = arg_int0("E", "max-elements-increase", "<num>", "Maximum number of estimation of elements");
  struct arg_lit *load_directory = arg_lit0("L", "load-directory", "Load directory");
  struct arg_file *input_file = arg_file0(NULL, NULL, "<file>", "Input file. Input directory if load option is on");
  struct arg_lit *matrix_info = arg_lit0(NULL, "matrix", "Show size and modulo of matrix of binary format");
  struct arg_lit *matrix_stat = arg_lit0("S", "stat", "Show statistics of matrix");
  struct arg_file *column_ops_save = arg_file0("C", "column", "<output>", "Save column operations to the file");
  struct arg_str *data_format = arg_str0("f", "format", "<format>", "Data format of matrix: text or binary");
  struct arg_str *reduce_by_row_operations = arg_str0("R", "reduce-by-row-operations", "<output>",
                                                      "Reduce elements of matrix by only row operations");
  struct arg_int *thread_number = arg_int0("t", "thread", "<num>", "Maximum number of threads");
  struct arg_int *validate = arg_int0("V", "validate", "<num>", "Validate matrix");
  struct arg_lit *test_matrix = arg_lit0("T", "test", "Test validation of matrix");
  struct arg_lit *debug = arg_lit0(NULL, "debug", "Debug mode");

  struct arg_end *end = arg_end(20);
  void* argtable[] = {
    info_number, precise_estimate, reduce_by_divisors, reduce_at_increase, auto_delete_duplicates, reduce_repeatedly,
    pivot_number, estimate_increase, load_directory, input_file, matrix_info, matrix_stat,
    column_ops_save, data_format, reduce_by_row_operations, thread_number, validate, test_matrix, debug, help, end
  };

  int nerrors = arg_parse(argc, argv, argtable);
  
  if (help->count > 0) {
    printf("Usage: %s", progname);
    arg_print_syntax(stdout, argtable,"\n");
    printf("Display points in files.\n\n");
    arg_print_glossary(stdout, argtable," %-30s %s\n");
    printf("\nReport bugs to <d@ytak.info>.\n");
    exit(0);
  }

  if (nerrors > 0){
    /* Display the error details contained in the arg_end struct.*/
    arg_print_errors(stdout, end, progname);
    printf("Try '%s --help' for more information.\n", progname);
    exit(1);
  }

  if (thread_number->count > 0) {
    lil_matrix_thread_pool_init(thread_number->ival[0]);
  }

  snf_arg = lil_snf_argument_new();
  snf_arg->signal_use_p = true;
  if (estimate_increase->count > 0) {
    snf_arg->max_estimate_increase = estimate_increase->ival[0];
  }
  if (pivot_number->count > 0) {
    snf_arg->min_pivot_number = pivot_number->ival[0];
  }
  if (column_ops_save->count > 0) {
    snf_arg->column_ops_output = (char *) column_ops_save->filename[0];
  }
  if (info_number->count > 0) {
    snf_arg->info = info_number->ival[0];
  }
  if (validate->count > 0) {
    snf_arg->validate = validate->ival[0];
  }
  if (reduce_by_divisors->count > 0) {
    snf_arg->reduce_by_divisors = reduce_by_divisors->ival[0];
  }
  if (reduce_at_increase->count > 0) {
    snf_arg->reduce_by_increase = reduce_at_increase->ival[0];
  }
  if (auto_delete_duplicates->count > 0) {
    snf_arg->auto_delete_duplicates = true;
  }
  if (reduce_repeatedly->count > 0) {
    snf_arg->reduce_repeatedly = true;
  }
  if (debug->count > 0) {
    snf_arg->debug = true;
  }
  if (precise_estimate->count > 0) {
    snf_arg->precise_estimate = true;
  }

  if (matrix_info->count > 0) {
    FILE *fp;
    if (input_file->count > 0) {
      fp = fopen(input_file->filename[0], "rb");
    } else {
      fp = stdin;
    }
    lil_matrix_print_matrix_size_of_dump(fp);
    if (input_file->count > 0) {
      fclose(fp);
    }
    return EXIT_SUCCESS;
  }

  mat_format = LIL_MATRIX_DATA_TEXT;
  if (data_format->count > 0) {
    if (data_format->sval[0][0] == 'b') {
      mat_format = LIL_MATRIX_DATA_BINARY;
    }
  }

  if (load_directory->count > 0) {
    snf_arg->load_directory = g_strdup(input_file->filename[0]);
    mat = lil_matrix_snf_load_directory_matrix(snf_arg);
  } else if (input_file->count > 0) {
    mat = lil_matrix_load_file(input_file->filename[0], mat_format);
  } else {
    mat = lil_matrix_load(stdin, mat_format);
  }

  if (test_matrix->count > 0) {
    mode = PRINT_BINARY_MATRIX;
  } else if (reduce_by_row_operations->count > 0) {
    path_reduced = reduce_by_row_operations->sval[0];
    mode = REDUCE_BY_ROW_OPS;
  } else if (matrix_stat->count > 0){
    mode = PRINT_MATRIX_STATS;
  } else {
    mode = CALC_SNF;
  }

  arg_freetable(argtable,sizeof(argtable) / sizeof(argtable[0]));

  if (mode == PRINT_BINARY_MATRIX) {
    lil_matrix_debug_print_valid(mat);
  } else if (mode == REDUCE_BY_ROW_OPS) {
    lil_matrix_reduce_by_row_operations(mat, snf_arg);
    lil_matrix_dump_to_file(mat, path_reduced, LIL_MATRIX_DATA_TEXT);
  } else if (mode == PRINT_MATRIX_STATS) {
    lil_matrix_print_stat(mat);
  } else {
    GArray *snf;
    snf = lil_matrix_snf(mat, snf_arg);
    lil_matrix_snf_print(snf);
    lil_matrix_snf_array_free(snf);
  }
  lil_snf_argument_free(snf_arg);
  lil_matrix_clear(mat);
  lil_matrix_thread_pool_free();
  return EXIT_SUCCESS;
}
