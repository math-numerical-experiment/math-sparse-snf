#ifndef _LILMATRIX_H_
#define _LILMATRIX_H_

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include <time.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <gmp.h>

typedef struct _lil_matrix {
  GSList *rows;
  int row_size;
  int column_size;
  int modulo;
  bool use_gmp_p;
} LIL_MATRIX;

typedef struct {
  int column;
  int value;
} LIL_MATRIX_ELEMENT;

typedef struct {
  int column;
  mpz_t value;
} LIL_MATRIX_ELEMENT_MPZ;

typedef struct {
  int step;
  int elementary_divisors;
  int empty_rows;
  int row_operations;
  int last_validating;
  int last_output;
  int last_reducing;;
  bool increasing;
} CalcStat;

typedef enum {
  CACHE_NOT_CHANGED,
  CACHE_NONE,
  CACHE_ROW_MINIMAL,
  CACHE_BOTH
} LIL_MATRIX_CACHE_TYPE;

typedef struct {
  bool set_p;
  GPtrArray *column_index;
  GPtrArray *row_minimal;
  GArray *row_changed;
  GArray *row_recreated;
  GArray *cumulative_row_changed;
  LIL_MATRIX_CACHE_TYPE type;
} LIL_MATRIX_CACHE;

typedef struct {
  int info;
  bool debug;
  int validate;
  int reduce_by_divisors;
  int reduce_by_increase;
  int max_estimate_increase;
  int min_pivot_number;
  bool precise_estimate;
  bool auto_delete_duplicates;
  bool reduce_repeatedly;
  char *column_ops_output;
  char *load_directory;
  CalcStat *stat;
  LIL_MATRIX_CACHE *cache;
  bool signal_use_p;
} LIL_SNF_ARG;

typedef enum {
  LIL_MATRIX_DATA_BINARY,
  LIL_MATRIX_DATA_TEXT
} LIL_MATRIX_DATA_FORMAT;

LIL_MATRIX * lil_matrix_new (int row_size, int column_size, int modulo);
void lil_matrix_clear (LIL_MATRIX *mat);
LIL_MATRIX *lil_matrix_copy (LIL_MATRIX *mat);
void lil_matrix_dump (LIL_MATRIX *mat, FILE *fp, LIL_MATRIX_DATA_FORMAT format);
void lil_matrix_dump_to_file (LIL_MATRIX *mat, const char *path, LIL_MATRIX_DATA_FORMAT format);
LIL_MATRIX *lil_matrix_load (FILE *fp, LIL_MATRIX_DATA_FORMAT format);
LIL_MATRIX *lil_matrix_load_file (const char *path, LIL_MATRIX_DATA_FORMAT format);
void lil_matrix_print_matrix_size_of_dump (FILE *fp);
bool lil_matrix_row_list_equal_p (LIL_MATRIX *mat1, LIL_MATRIX *mat2, GSList *row_list_node1, GSList *row_list_node2);
bool lil_matrix_equal_p (LIL_MATRIX *mat1, LIL_MATRIX *mat2);
LIL_MATRIX * lil_matrix_new2 (int row_size, int column_size, int modulo, int *data);
void lil_matrix_row_set (LIL_MATRIX *mat, int row, int row_data_num, int *ptr);
void lil_matrix_row_append (LIL_MATRIX *mat, int row_data_num, int *ptr);
int lil_matrix_element(LIL_MATRIX *mat, int row, int col);
void lil_matrix_element_mpz (mpz_t val, LIL_MATRIX *mat, int row, int col);
void lil_matrix_row_delete (LIL_MATRIX *mat, int row);
void lil_matrix_column_delete (LIL_MATRIX *mat, int col);
void lil_matrix_row_exchange(LIL_MATRIX *mat, int row1, int row2);
void lil_matrix_column_exchange(LIL_MATRIX *mat, int col1, int col2);
void lil_matrix_row_mul_scalar (LIL_MATRIX *mat, int row, int factor);
void lil_matrix_column_mul_scalar (LIL_MATRIX *mat, int col, int factor);
void lil_matrix_divide_elements (LIL_MATRIX *mat, int num);
void lil_matrix_divide_elements_mpz (LIL_MATRIX *mat, mpz_t num);
void lil_matrix_add_rows (LIL_MATRIX *mat, int row_target, int row_source, int factor);
void lil_matrix_add_rows_mpz (LIL_MATRIX *mat, int row_target, int row_source, mpz_t factor);
void lil_matrix_add_columns (LIL_MATRIX *mat, int col_target, int col_source, int factor);
void lil_matrix_add_columns_mpz (LIL_MATRIX *mat, int col_target, int col_source, mpz_t factor);
void lil_matrix_transpose (LIL_MATRIX *mat);
void lil_matrix_snf_print (GArray *snf);
GArray *lil_matrix_snf (LIL_MATRIX *mat, LIL_SNF_ARG *arg);
GArray *lil_matrix_snf2 (LIL_MATRIX *mat);
void lil_matrix_snf_array_free (GArray *snf);
bool lil_matrix_valid_p (LIL_MATRIX *mat);
void lil_matrix_fprint (FILE *fp, LIL_MATRIX *mat);
void lil_matrix_print (LIL_MATRIX *mat);
void lil_matrix_print_row (LIL_MATRIX *mat, int row_num);
void lil_matrix_debug_print (LIL_MATRIX *mat);
void lil_matrix_debug_print2 (LIL_MATRIX *mat);
void lil_matrix_debug_print_range (LIL_MATRIX *mat, int row_start, int row_end);
void lil_matrix_debug_print_info (LIL_MATRIX *mat);
void lil_matrix_debug_print_count_column (LIL_MATRIX *mat);
void lil_matrix_debug_print_valid (LIL_MATRIX *mat);
void lil_matrix_set_from_input_file (LIL_MATRIX *mat, FILE *fp);
LIL_SNF_ARG *lil_snf_argument_new ();
void lil_snf_argument_free (LIL_SNF_ARG *arg);
LIL_MATRIX *lil_matrix_snf_load_directory_matrix (LIL_SNF_ARG *arg);
void lil_matrix_reduce_by_row_operations (LIL_MATRIX *mat, LIL_SNF_ARG *arg);
void lil_matrix_print_stat (LIL_MATRIX *mat);
void lil_matrix_thread_pool_init (int thread_num);

#define abort_with_line_number() { fprintf(stderr, "abort at %s:%d\n", __FILE__, __LINE__); abort(); }

#define ARRAY_EMPTY -1
#define ARRAY_ALL_LESS -2

#endif /* _LILMATRIX_H_ */
