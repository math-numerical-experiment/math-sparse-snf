#include "lilmatrix.h"
#include "intern.h"

#define DEFAULT_MAX_THREAD_NUM 4
#define MIN_ROW_NUM 2000
#define MAX_EMPTY_ROWS 5000
#define DEFAULT_PIVOT_NUM 500
#define DEFAULT_MAX_ELEMENTS_INCREASE 1000
#define DEFAULT_REDUCTION_ELEMENTS_INCREASE 300000
#define PIVOT_CANDIDATE_NUM 3

#define THREAD_NUMBER_DEFAULT 0
#define THREAD_NUMBER_ADJUST -1

typedef struct {
  LIL_MATRIX *matrix;
  int thread_total_number;
  int thread_index_number;
  int task_number;
  int number_start;
  int number_end;
  gpointer arguments;
  gpointer data;
  gpointer func;
} ThreadArgs;

typedef struct {
  GCond cond;
  GMutex mutex;
  bool finished;
} ThreadSignal;

typedef struct {
  GThreadPool *pool;
  GPtrArray *signal;
  int max_thread_number;
  int max_task_number;
} ThreadPool;

typedef struct {
  const char *thread_name;
  int thread_num;
  int row_num_step;
  LIL_MATRIX *mat;
  int num_start;
  int num_end;
  gpointer additional_data;
  void (*args_alloc)(int thread_num, GPtrArray *args, gpointer additional_data);
  gpointer (*args_free)(int thread_num, GPtrArray *args, gpointer additional_data);
  gpointer (*thread_func)(ThreadArgs *thread_args);
} ThreadExecArgs;

static ThreadPool *thread_pool = NULL;

#define STATIC_ASSERT(exp) typedef char _dummy[(exp) ? 1 : -1]

#define MAX_READ_BUFFER_SIZE 64
#define FILE_PATH_SIZE 1024
#define PRINT_COLUMN_COUNT_LF 15
#define TIME_STRING_SIZE 64

/* We can not detect overflow of multiplication */
STATIC_ASSERT(sizeof(long long) >= 2 * sizeof(int));

#define check_row_number_argument(arg, mat)                             \
  {                                                                     \
    if (arg < 0 || arg >= mat->row_size) {                              \
      fprintf(stderr, "Invalid row number %d: it must be nonnegative integer less than %d\n", arg, mat->row_size); \
      abort_with_line_number();                                                          \
    }                                                                   \
  }

#define check_column_number_argument(arg, mat)                          \
  {                                                                     \
    if (arg < 0 || arg >= mat->column_size) {                           \
      fprintf(stderr, "Invalid column number %d: it must be nonnegative integer less than %d\n", arg, mat->column_size); \
      abort_with_line_number();                                                          \
    }                                                                   \
  }

/* Note that we must substitute NULL with list_backward before we use lil_g_slist_append_with_last. */
#define lil_g_slist_append_with_last(list_head, list_backward, val)     \
  if (list_backward) {                                                  \
    list_backward = g_slist_append(list_backward, val);                 \
  } else {                                                              \
    list_head = g_slist_append(list_head, val);                         \
    list_backward = g_slist_last(list_head);                            \
  }

static void lil_matrix_thread_exec_args_set (ThreadExecArgs *thread_exec_args,
                                             const char *thread_name, int thread_num, LIL_MATRIX *mat,
                                             int num_start, int num_end, gpointer additional_data,
                                             void (*args_alloc)(int thread_num, GPtrArray *args, gpointer additional_data),
                                             gpointer (*args_free)(int thread_num, GPtrArray *args, gpointer additional_data),
                                             gpointer (*thread_func)(ThreadArgs *thread_args));
static gpointer lil_matrix_thread_exec (ThreadExecArgs *thread_exec_args);
static void lil_matrix_debug_print_info_with_stat (LIL_MATRIX *mat, CalcStat *stat);
static void lil_snf_argument_cache_row_recreated_append (LIL_SNF_ARG *arg, int row_num);

/* Multiplication with overflow check */
static int lil_matrix_multiply (int num1, int num2)
{
  signed long long tmp;
  tmp = (signed long long) num1 * (signed long long) num2;

  if ((tmp > INT_MAX) || (tmp < INT_MIN)) {
    fprintf(stderr, "Overflow!\n");
    abort_with_line_number ();
  }
  return (int) tmp;
}

static void lil_matrix_element_free (LIL_MATRIX_ELEMENT *el)
{
  g_free(el);
}

static void lil_matrix_element_mpz_free (LIL_MATRIX_ELEMENT_MPZ *el)
{
  mpz_clear(el->value);
  g_free(el);
}

static GSList *lil_matrix_element_link_deletion_with_free (LIL_MATRIX *mat, GSList *row_node, GSList *el_node)
{
  if (mat->use_gmp_p) {
    lil_matrix_element_mpz_free((LIL_MATRIX_ELEMENT_MPZ *) el_node->data);
  } else {
    lil_matrix_element_free((LIL_MATRIX_ELEMENT *) el_node->data);
  }
  return g_slist_delete_link(row_node, el_node);
}

static GSList *lil_matrix_next_element_node_with_link_deletion (LIL_MATRIX *mat, GSList *row_node, GSList **el_node)
{
  GSList *el_node_next;
  el_node_next = g_slist_next(*el_node);
  row_node = lil_matrix_element_link_deletion_with_free (mat, row_node, *el_node);
  *el_node = el_node_next;
  return row_node;
}

/* count must be initialized ahead. */
void lil_matrix_number_of_non_zero_elements (LIL_MATRIX *mat, mpz_t count)
{
  GSList *row_list_node, *row_node;
  mpz_set_si(count, 0);
  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    mpz_add_ui(count, count, g_slist_length(row_node));
    row_list_node = g_slist_next(row_list_node);
  }
}

void lil_matrix_mpi_total_number_of_elements (LIL_MATRIX *mat, CalcStat *stat, mpz_t total)
{
  if (stat) {
    mpz_set_si(total, mat->row_size - stat->empty_rows);
    mpz_mul_si(total, total, mat->column_size - stat->elementary_divisors);
  } else {
    mpz_set_si(total, mat->row_size);
    mpz_mul_si(total, total, mat->column_size);
  }
}

void lil_matrix_percentage_of_non_zero_elements (LIL_MATRIX *mat, CalcStat *stat, mpz_t total, mpz_t non_zero_count, mpf_t non_zero_percentage)
{
  if (mpz_cmp_ui(total, 0) > 0) {
    mpf_t t;
    mpf_init2(t, MPF_PREC);
    mpf_set_z(non_zero_percentage, non_zero_count);
    mpf_set_z(t, total);
    mpf_div(non_zero_percentage, non_zero_percentage, t);
    mpf_clear(t);
  } else {
    mpf_set_ui(non_zero_percentage, 0);
  }
}

static GSList *lil_matrix_rows_new (int row_size)
{
  int i;
  GSList *rows_new;
  rows_new = NULL;
  for (i = 0; i < row_size; i++) {
    rows_new = g_slist_prepend(rows_new, NULL);
  }
  return rows_new;
}

LIL_MATRIX *lil_matrix_new (int row_size, int column_size, int modulo)
{
  LIL_MATRIX *mat;
  mat = g_malloc(sizeof(LIL_MATRIX));
  mat->row_size = row_size;
  mat->column_size = column_size;
  mat->rows = lil_matrix_rows_new(row_size);
  if (modulo < 0) modulo = 0;
  mat->modulo = modulo;
  mat->use_gmp_p = (mat->modulo == 0);
  return mat;
}

void lil_matrix_row_data_clear (GSList *row, bool use_gmp)
{
  if (use_gmp) {
    g_slist_foreach(row, (GFunc) lil_matrix_element_mpz_free, NULL);
  } else {
    g_slist_foreach(row, (GFunc) lil_matrix_element_free, NULL);
  }
  g_slist_free(row);
}

static void lil_matrix_row_data_clear2 (gpointer row_ptr, gpointer use_gmp_ptr)
{
  lil_matrix_row_data_clear((GSList *) row_ptr, *((bool *) use_gmp_ptr));
}

static void lil_matrix_clear_rows (GSList *rows, bool use_gmp)
{
  g_slist_foreach(rows, (GFunc) lil_matrix_row_data_clear2, &use_gmp);
  g_slist_free(rows);
}

LIL_MATRIX_ELEMENT *lil_matrix_element_new (int column, int value)
{
  LIL_MATRIX_ELEMENT *el_new;
  el_new = g_malloc(sizeof(LIL_MATRIX_ELEMENT));
  if (column < 0) {
    fprintf(stderr, "Invalid new element: column=%d, value=%d\n", column, value);
    abort_with_line_number();
  }
  el_new->column = column;
  el_new->value = value;
  return el_new;
}

static LIL_MATRIX_ELEMENT_MPZ *lil_matrix_element_mpz_init (int column)
{
  LIL_MATRIX_ELEMENT_MPZ *el_new;
  el_new = g_malloc(sizeof(LIL_MATRIX_ELEMENT_MPZ));
  if (column < 0) {
    gmp_fprintf(stderr, "Invalid column of new element: column=%d\n", column);
    abort_with_line_number();
  }
  el_new->column = column;
  mpz_init(el_new->value);
  return el_new;
}

static LIL_MATRIX_ELEMENT_MPZ *lil_matrix_element_mpz_new (int column, mpz_t value)
{
  LIL_MATRIX_ELEMENT_MPZ *el_new;
  el_new = lil_matrix_element_mpz_init(column);
  mpz_set(el_new->value, value);
  return el_new;
}

static LIL_MATRIX_ELEMENT_MPZ *lil_matrix_element_mpz_new2 (int column, int value)
{
  LIL_MATRIX_ELEMENT_MPZ *el_new;
  el_new = lil_matrix_element_mpz_init(column);
  mpz_set_si(el_new->value, value);
  return el_new;
}

static LIL_MATRIX_ELEMENT_MPZ *lil_matrix_element_mpz_new3 (int column, gchar *value)
{
  LIL_MATRIX_ELEMENT_MPZ *el_new;
  el_new = lil_matrix_element_mpz_init(column);
  mpz_set_str(el_new->value, value, 10);
  return el_new;
}

static gpointer lil_matrix_element_suitable_new (LIL_MATRIX *mat, int column, int value)
{
  return (mat->use_gmp_p ? (gpointer) lil_matrix_element_mpz_new2(column, value) : (gpointer) lil_matrix_element_new(column, value));
}

/* Set value of element to val and return column number. */
static int lil_matrix_element_value (mpz_t val, LIL_MATRIX *mat, gpointer el_data)
{
  int col;
  if (mat->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el_mpz;
    el_mpz = (LIL_MATRIX_ELEMENT_MPZ *) el_data;
    mpz_set(val, el_mpz->value);
    col = el_mpz->column;
  } else {
    LIL_MATRIX_ELEMENT *el_int;
    el_int = (LIL_MATRIX_ELEMENT *) el_data;
    mpz_set_si(val, el_int->value);
    col = el_int->column;
  }
  return col;
}

void lil_matrix_clear (LIL_MATRIX *mat)
{
  lil_matrix_clear_rows(mat->rows, mat->use_gmp_p);
  g_free(mat);
}

static gpointer lil_matrix_element_copy (LIL_MATRIX *mat, gpointer el)
{
  if (mat->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el_mpz;
    el_mpz = el;
    return (gpointer) lil_matrix_element_mpz_new(el_mpz->column, el_mpz->value);
  } else {
    LIL_MATRIX_ELEMENT *el_int;
    el_int = el;
    return (gpointer) lil_matrix_element_new(el_int->column, el_int->value);
  }
}

LIL_MATRIX *lil_matrix_copy (LIL_MATRIX *mat)
{
  LIL_MATRIX *mat_new;
  GSList *row_list_node, *row_node, *el_node, *row_list_node_new, *row_node_new;
  mat_new = lil_matrix_new(mat->row_size, mat->column_size, mat->modulo);
  row_list_node = g_slist_nth(mat->rows, 0);
  row_list_node_new = g_slist_nth(mat_new->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      row_node_new = (GSList *) row_list_node_new->data;
      el_node = g_slist_nth(row_node, 0);
      while (el_node) {
        row_node_new = g_slist_append(row_node_new, lil_matrix_element_copy(mat, el_node->data));
        el_node = g_slist_next(el_node);
      }
      if (row_node_new) {
        row_list_node_new->data = row_node_new;
      }
    }
    row_list_node = g_slist_next(row_list_node);
    row_list_node_new = g_slist_next(row_list_node_new);
  }
  return mat_new;
}

static void lil_matrix_dump_fwrite(int32_t *ptr, int write_size, FILE *fp)
{
  if (fwrite(ptr, sizeof(int32_t), write_size, fp) != write_size) {
    fprintf(stderr, "Can not dump\n");
    abort_with_line_number();
  }
}

/*
  3 ROW COLUMN MODULO
  DATA_SIZE COL VAL COL VAL ...
  DATA_SIZE COL VAL COL VAL ...
  ...

  If DATA_SIZE is odd, the data is matrix information: ROW COLUMN MODULO
*/
static void lil_matrix_dump_binary (LIL_MATRIX *mat, FILE *fp)
{
  int32_t row_data_size, el_data[2], mat_data[4];
  GSList *row_list_node, *row_node, *el_node;
  LIL_MATRIX_ELEMENT *el_int;
  LIL_MATRIX_ELEMENT_MPZ *el_mpz;

  mat_data[0] = 3;
  mat_data[1] = mat->row_size;
  mat_data[2] = mat->column_size;
  mat_data[3] = mat->modulo;
  lil_matrix_dump_fwrite(mat_data, 4, fp);

  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      row_data_size = g_slist_length(row_node) * 2;
      lil_matrix_dump_fwrite(&row_data_size, 1, fp);
      el_node = g_slist_nth(row_node, 0);
      while (el_node) {
        if (mat->use_gmp_p) {
          el_mpz = (LIL_MATRIX_ELEMENT_MPZ *) el_node->data;
          el_data[0] = el_mpz->column;
          if (mpz_cmp_si(el_mpz->value, INT_MAX) >= 0) {
            gmp_fprintf(stdout, "Can not convert mpz_t to int: %Zd\n", el_mpz->value);
          }
          el_data[1] = mpz_get_si(el_mpz->value);
        } else {
          el_int = (LIL_MATRIX_ELEMENT *) el_node->data;
          el_data[0] = el_int->column;
          el_data[1] = el_int->value;
        }
        lil_matrix_dump_fwrite(el_data, 2, fp);
        el_node = g_slist_next(el_node);
      }
    } else {
      row_data_size = 0;
      lil_matrix_dump_fwrite(&row_data_size, 1, fp);
    }
    row_list_node = g_slist_next(row_list_node);
  }
}

static void lil_matrix_dump_element_text (FILE *fp, LIL_MATRIX *mat, gpointer el_data)
{
  if (mat->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el_mpz;
    el_mpz = (LIL_MATRIX_ELEMENT_MPZ *) el_data;
    gmp_fprintf(fp, "%d %Zd", el_mpz->column, el_mpz->value);
  } else {
    LIL_MATRIX_ELEMENT *el_int;
    el_int = (LIL_MATRIX_ELEMENT *) el_data;
    fprintf(fp, "%d %d", el_int->column, el_int->value);
  }
}

void lil_matrix_rows_dump_text (LIL_MATRIX *mat, int row_start, int row_end, FILE *fp)
{
  GSList *row_list_node, *row_node, *el_node;
  int row_num;
  bool first_el;
  row_num = row_start;
  row_list_node = g_slist_nth(mat->rows, row_num);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      first_el = true;
      el_node = g_slist_nth(row_node, 0);
      while (el_node) {
        if (!first_el) {
          fprintf(fp, " ");
        } else {
          first_el = false;
        }
        lil_matrix_dump_element_text(fp, mat, el_node->data);
        el_node = g_slist_next(el_node);
      }
    }
    fprintf(fp, "\n");
    row_list_node = g_slist_next(row_list_node);
    row_num += 1;
    if (row_num > row_end) {
      break;
    }
  }
}

static void lil_matrix_dump_text (LIL_MATRIX *mat, FILE *fp)
{
  fprintf(fp, "# %d %d %d\n", mat->row_size, mat->column_size, mat->modulo);
  lil_matrix_rows_dump_text(mat, 0, mat->row_size - 1, fp);
}

void lil_matrix_dump (LIL_MATRIX *mat, FILE *fp, LIL_MATRIX_DATA_FORMAT format)
{
  if (format == LIL_MATRIX_DATA_TEXT) {
    lil_matrix_dump_text(mat, fp);
  } else if (format == LIL_MATRIX_DATA_BINARY) {
    lil_matrix_dump_binary(mat, fp);
  } else {
    fprintf(stderr, "Invalid format of matrix data.");
    abort_with_line_number();
  }
}

void lil_matrix_dump_to_file (LIL_MATRIX *mat, const char *path, LIL_MATRIX_DATA_FORMAT format)
{
  FILE *fp;
  fp = fopen(path, "wb");
  if (fp == NULL) {
    fprintf(stderr, "Can not open file %s\n", path);
    abort_with_line_number();
  }
  lil_matrix_dump(mat, fp, format);
  fclose(fp);
}

static void lil_matrix_init_buffer_to_read (int *max_buf_size, char **buf, int size)
{
  *max_buf_size = MAX_READ_BUFFER_SIZE;
  *buf = (char *) malloc(size * (*max_buf_size));
}

static bool lil_matrix_gets_one_chunk (char **buf, int32_t *buf_size, int *max_buf_size, FILE *fp)
{
  if (fread(buf_size, sizeof(int32_t), 1, fp) != 1) {
    return false;
  }
  if (max_buf_size < buf_size) {
    *max_buf_size += *buf_size;
    *buf = (char *) realloc(*buf, sizeof(int32_t) * (*max_buf_size));
  }
  if (fread(*buf, sizeof(int32_t), *buf_size, fp) != *buf_size) {
    fprintf(stderr, "Can not read chunk of binary format\n");
    abort_with_line_number();
  }
  return true;
}

static LIL_MATRIX *lil_matrix_load_binary (FILE *fp)
{
  LIL_MATRIX *mat;
  GSList *row_node, *row_node_backward;
  int max_buf_size, row_size, i, line;
  char *buf;
  int32_t buf_size, *buf_ptr;

  line = 0;
  row_node_backward = NULL;
  mat = lil_matrix_new(0, 0, INT_MAX);
  lil_matrix_init_buffer_to_read(&max_buf_size, &buf, sizeof(int32_t));
  while (lil_matrix_gets_one_chunk(&buf, &buf_size, &max_buf_size, fp)) {
    buf_ptr = (int32_t *) buf;
    if (line == 0) {
      if (buf_size != 3) {
        fprintf(stderr, "Invalid header\n");
        abort_with_line_number();
      }
      mat->row_size = buf_ptr[0];
      mat->column_size = buf_ptr[1];
      mat->modulo = buf_ptr[2];
      if (mat->modulo < 0) {
        mat->modulo = INT_MAX;
      }
      mat->use_gmp_p = (mat->modulo == 0);
    } else {
      if (buf_size == 0) {
        lil_g_slist_append_with_last(mat->rows, row_node_backward, NULL);
      } else {
        row_node = NULL;
        if (mat->use_gmp_p) {
          for (i = 0; i < buf_size / 2; i++) {
            row_node = g_slist_append(row_node, lil_matrix_element_mpz_new2 (buf_ptr[i * 2], buf_ptr[i * 2 + 1]));
          }
        } else {
          for (i = 0; i < buf_size / 2; i++) {
            row_node = g_slist_append(row_node, lil_matrix_element_new(buf_ptr[i * 2], buf_ptr[i * 2 + 1]));
          }
        }
        lil_g_slist_append_with_last(mat->rows, row_node_backward, row_node);
        if (mat->column_size < buf_ptr[buf_size - 2]) {
          mat->column_size = buf_ptr[buf_size - 2];
        }
      }
    }
    line += 1;
  }
  free(buf);

  row_size = g_slist_length(mat->rows);
  if (row_size < mat->row_size) {
    for (; row_size < mat->row_size; row_size++) {
      lil_g_slist_append_with_last(mat->rows, row_node_backward, NULL);
    }
  } else if (row_size > mat->row_size) {
    mat->row_size = row_size;
  }

  return mat;
}

static int fileread_gets_line(char **line, int *line_size, int *max_line_size, FILE *fp)
{
  if (fgets(*line, *max_line_size - 1, fp) != NULL) {
    do {
      *line_size = strlen(*line);
      if((*line)[*line_size - 1] == '\n'){
        (*line)[*line_size - 1] = '\0';
        *line_size -= 1;
        break;
      }
      *max_line_size += MAX_READ_BUFFER_SIZE;
      *line = (char *) realloc(*line, sizeof(char) * (*max_line_size));
    } while (fgets(*line + (*line_size), MAX_READ_BUFFER_SIZE, fp) != NULL);
    return true;
  }
  return false;
}

static gpointer lil_matrix_parse_element_token (gchar *token_column, gchar *token_value, bool use_gmp)
{
  if (use_gmp) {
    return (gpointer) lil_matrix_element_mpz_new3(atoi(token_column), token_value);
  } else {
    return (gpointer) lil_matrix_element_new(atoi(token_column), atoi(token_value));
  }
}

static GSList *lil_matrix_row_list_from_tokens (gchar **tokens, bool use_gmp)
{
  int i;
  GSList *row_node, *row_node_last;
  gpointer el_ptr;
  row_node = NULL;
  row_node_last = NULL;
  i = 0;
  while (tokens[i]) {
    if (tokens[i + 1]) {
      el_ptr = lil_matrix_parse_element_token(tokens[i], tokens[i + 1], use_gmp);
      lil_g_slist_append_with_last(row_node, row_node_last, el_ptr);
      i += 2;
    } else {
      fprintf(stderr, "Invalid matrix row data: odd number of integers\n");
      abort_with_line_number();
    }
  }
  return row_node;
}

static bool lil_matrix_text_get_arguments (int mat_args[], char *line) {
  int i;
  gchar **tokens;
  if (line[0] != '#') {
    return false;
  }
  tokens = g_strsplit(line, " ", -1);
  i = 0;
  while (tokens[i]) {
    if (i > 0) {
      mat_args[i - 1] = atoi(tokens[i]);
    }
    if (i == 3) {
      break;
    }
    i += 1;
  }
  g_strfreev(tokens);
  if (i < 3) {
    return false;
  }
  return true;
}

bool lil_matrix_text_file_get_matrix_arguments (int mat_args[], const char *path)
{
  bool ret;
  char *line;
  int max_line_size, line_size;
  FILE *fp;
  fp = fopen(path, "rb");
  if (fp == NULL) {
    fprintf(stderr, "Can not open file %s\n", path);
    abort_with_line_number();
  }
  lil_matrix_init_buffer_to_read(&max_line_size, &line, sizeof(char));
  if (fileread_gets_line(&line, &line_size, &max_line_size, fp)) {
    ret = lil_matrix_text_get_arguments(mat_args, line);
  } else {
    ret = false;
  }
  free(line);
  fclose(fp);
  return ret;
}

/*
  row_start and row_end are -1 if we want to load all rows.
  In order to put matrix data on different memories, we can use these parameters.
*/
static LIL_MATRIX *lil_matrix_load_text (FILE *fp, int row_start, int row_end)
{
  LIL_MATRIX *mat;
  int max_line_size, line_size, mat_args[3], row_num;
  char *line;
  gchar **tokens;
  GSList *row_list_node;
  bool invalid_data;
  invalid_data = false;
  mat = NULL;
  lil_matrix_init_buffer_to_read(&max_line_size, &line, sizeof(char));
  row_list_node = NULL;
  row_num = 0;
  while (fileread_gets_line(&line, &line_size, &max_line_size, fp)) {
    if (mat) {
      if (row_num >= row_start) {
        tokens = g_strsplit(line, " ", -1);
        row_list_node->data = lil_matrix_row_list_from_tokens(tokens, mat->use_gmp_p);
        g_strfreev(tokens);
      }
      row_list_node = g_slist_next(row_list_node);
      row_num += 1;
      if (!row_list_node || row_num > row_end) {
        break;
      }
    } else {
      if (!lil_matrix_text_get_arguments(mat_args, line)) {
        invalid_data = true;
        break;
      }
      mat = lil_matrix_new(mat_args[0], mat_args[1], mat_args[2]);
      if (row_start < 0) {
        row_start = 0;
      }
      if (row_end < 0) {
        row_end = mat_args[0];
      }
      row_list_node = g_slist_nth(mat->rows, 0);
    }
  }
  free(line);
  if (invalid_data) {
    fprintf(stderr, "Invalid data file\n");
    abort_with_line_number();
  }
  return mat;
}

LIL_MATRIX *lil_matrix_load (FILE *fp, LIL_MATRIX_DATA_FORMAT format)
{
  if (format == LIL_MATRIX_DATA_TEXT) {
    return lil_matrix_load_text(fp, -1, -1);
  } else if (format == LIL_MATRIX_DATA_BINARY) {
    return lil_matrix_load_binary(fp);
  } else {
    fprintf(stderr, "Invalid format of matrix data.");
    abort_with_line_number();
  }
}

LIL_MATRIX *lil_matrix_load_file (const char *path, LIL_MATRIX_DATA_FORMAT format)
{
  LIL_MATRIX *mat;
  FILE *fp;
  fp = fopen(path, "rb");
  if (fp == NULL) {
    fprintf(stderr, "Can not open file %s\n", path);
    abort_with_line_number();
  }
  mat = lil_matrix_load(fp, format);
  fclose(fp);
  return mat;
}

LIL_MATRIX *lil_matrix_load_file_row_range (const char *path, int row_start, int row_end)
{
  LIL_MATRIX *mat;
  FILE *fp;
  fp = fopen(path, "rb");
  if (fp == NULL) {
    fprintf(stderr, "Can not open file %s\n", path);
    abort_with_line_number();
  }
  mat = lil_matrix_load_text(fp, row_start, row_end);
  fclose(fp);
  return mat;
}

void lil_matrix_print_matrix_size_of_dump (FILE *fp)
{
  int max_buf_size;
  char *buf;
  int32_t buf_size, *buf_ptr;
  lil_matrix_init_buffer_to_read(&max_buf_size, &buf, sizeof(int32_t));
  while (lil_matrix_gets_one_chunk(&buf, &buf_size, &max_buf_size, fp)) {
    buf_ptr = (int32_t *) buf;
    if (buf_size % 2 == 1) {
      printf("%d x %d modulo %d\n", buf_ptr[0], buf_ptr[1], buf_ptr[2]);
      break;
    }
  }
  free(buf);
}

static bool lil_matrix_element_node_equal_p(LIL_MATRIX *mat1, GSList *el_node1, LIL_MATRIX *mat2, GSList *el_node2)
{
  if (mat1->use_gmp_p && mat2->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el1, *el2;
    el1 = (LIL_MATRIX_ELEMENT_MPZ *) el_node1->data;
    el2 = (LIL_MATRIX_ELEMENT_MPZ *) el_node2->data;
    return ((mpz_cmp(el1->value, el2->value) == 0) && el1->column == el2->column);
  } else if (mat1->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el1;
    LIL_MATRIX_ELEMENT *el2;
    el1 = (LIL_MATRIX_ELEMENT_MPZ *) el_node1->data;
    el2 = (LIL_MATRIX_ELEMENT *) el_node2->data;
    return ((mpz_cmp_si(el1->value, el2->value) == 0)  && el1->column == el2->column);
  } else if (mat2->use_gmp_p) {
    LIL_MATRIX_ELEMENT *el1;
    LIL_MATRIX_ELEMENT_MPZ *el2;
    el1 = (LIL_MATRIX_ELEMENT *) el_node1->data;
    el2 = (LIL_MATRIX_ELEMENT_MPZ *) el_node2->data;
    return ((mpz_cmp_si(el2->value, el1->value) == 0)  && el1->column == el2->column);
  } else {
    LIL_MATRIX_ELEMENT *el1, *el2;
    el1 = (LIL_MATRIX_ELEMENT *) el_node1->data;
    el2 = (LIL_MATRIX_ELEMENT *) el_node2->data;
    return (el1->value == el2->value && el1->column == el2->column);
  }
}

bool lil_matrix_row_list_equal_p (LIL_MATRIX *mat1, LIL_MATRIX *mat2, GSList *row_list_node1, GSList *row_list_node2)
{
  GSList *row_node1, *row_node2, *el_node1, *el_node2;
  row_node1 = (GSList *) row_list_node1->data;
  row_node2 = (GSList *) row_list_node2->data;
  if (row_node1 && row_node2) {
    el_node1 = g_slist_nth(row_node1, 0);
    el_node2 = g_slist_nth(row_node2, 0);
    while (el_node1 && el_node2) {
      if (!lil_matrix_element_node_equal_p(mat1, el_node1, mat2, el_node2)) {
        return false;
      }
      el_node1 = g_slist_next(el_node1);
      el_node2 = g_slist_next(el_node2);
    }
    if (el_node1 || el_node2) {
      return false;
    }
  } else if (row_node1 || row_node2) {
    return false;
  }
  return true;
}

bool lil_matrix_equal_p (LIL_MATRIX *mat1, LIL_MATRIX *mat2)
{
  GSList *row_list_node1, *row_list_node2;
  if (mat1->column_size != mat2->column_size || mat1->row_size !=  mat2->row_size || mat1->modulo != mat2->modulo) {
    return false;
  }
  row_list_node1 = g_slist_nth(mat1->rows, 0);
  row_list_node2 = g_slist_nth(mat2->rows, 0);
  while (row_list_node1 && row_list_node2) {
    if (!lil_matrix_row_list_equal_p(mat1, mat2, row_list_node1, row_list_node2)) {
      return false;
    }
    row_list_node1 = g_slist_next(row_list_node1);
    row_list_node2 = g_slist_next(row_list_node2);
  }
  if (row_list_node1 || row_list_node2) {
    return false;
  }
  return true;
}

/* *data is an array of row_size * column_size elements data */
LIL_MATRIX *lil_matrix_new2 (int row_size, int column_size, int modulo, int *data)
{
  int i, j, k, val;
  LIL_MATRIX *mat;
  GSList *row_list_node, *row_node, *row_node_backward;
  mat = lil_matrix_new(row_size, column_size, modulo);
  row_list_node = g_slist_nth(mat->rows, 0);
  k = 0;
  for (i = 0; i < mat->row_size; i++) {
    row_node = (GSList *) row_list_node->data;
    row_node_backward = g_slist_last(row_node);
    for (j = 0; j < column_size; j++) {
      val = data[k];
      if (!mat->use_gmp_p) {
        val = val % mat->modulo;
      }
      if (val != 0) {
        lil_g_slist_append_with_last(row_node, row_node_backward, lil_matrix_element_suitable_new(mat, j, val));
      }
      k++;
    }
    row_list_node->data = row_node;
    row_list_node = g_slist_next(row_list_node);
  }
  return mat;
}

void lil_matrix_row_set (LIL_MATRIX *mat, int row, int row_data_num, int *ptr)
{
  GSList *el_node, *row_node;
  LIL_MATRIX_ELEMENT *el;
  int i, col, val, row_list_size;
  bool inserted;
  check_row_number_argument(row, mat);
  row_node = (GSList *) g_slist_nth(mat->rows, row)->data;
  for (i = 0; i < row_data_num; i++) {
    inserted = false;
    col = ptr[2 * i];
    if (col >= mat->column_size) {
      mat->column_size = col + 1;
    }
    val = ptr[2 * i + 1];
    if (!mat->use_gmp_p) {
      val = val % mat->modulo;
    }
    row_list_size = g_slist_length(row_node);
    if (row_list_size > 0) {
      el_node = g_slist_nth(row_node, 0);
      while (el_node) {
        el = (LIL_MATRIX_ELEMENT *) el_node->data;
        if (el->column == col) {
          if (val == 0) {
            row_node = lil_matrix_element_link_deletion_with_free(mat, row_node, el_node);
          } else {
            el->value = val;
          }
          inserted = true;
          break;
        } else if (el->column > col) {
          if (val != 0) {
            row_node = g_slist_insert_before(row_node, el_node, lil_matrix_element_suitable_new(mat, col, val));
          }
          inserted = true;
          break;
        }
        el_node = g_slist_next(el_node);
      }
    }
    if (!inserted && val!= 0) {
      row_node = g_slist_append(row_node, lil_matrix_element_suitable_new(mat, col, val));
    }
  }
  ((GSList *) g_slist_nth(mat->rows, row))->data = row_node;
}

void lil_matrix_row_append (LIL_MATRIX *mat, int row_data_num, int *ptr)
{
  mat->rows = g_slist_append(mat->rows, NULL);
  mat->row_size += 1;
  lil_matrix_row_set(mat, mat->row_size - 1, row_data_num, ptr);
}

static int lil_matrix_element_column_number (LIL_MATRIX *mat, gpointer el_node_data)
{
  if (mat->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el;
    el = (LIL_MATRIX_ELEMENT_MPZ *) el_node_data;
    return el->column;
  } else {
    LIL_MATRIX_ELEMENT *el;
    el = (LIL_MATRIX_ELEMENT *) el_node_data;
    return el->column;
  }
}

static gpointer lil_matrix_find_element_in_row (LIL_MATRIX *mat, GSList *row_node, int col)
{
  int el_col;
  GSList *el_node;
  el_node = g_slist_nth(row_node, 0);
  while (el_node) {
    el_col = lil_matrix_element_column_number(mat, el_node->data);
    if (el_col >= col) {
      if (el_col == col) {
        return el_node->data;
      }
      break;
    }
    el_node = g_slist_next(el_node);
  }
  return NULL;
}

static bool lil_matrix_find_get_element_in_row (mpz_t val, LIL_MATRIX *mat, GSList *row_node, int col)
{
  gpointer el_data;
  el_data = lil_matrix_find_element_in_row(mat, row_node, col);
  if (el_data) {
    lil_matrix_element_value(val, mat, el_data);
    return true;
  }
  return false;
}

static int lil_matrix_element_for_int (LIL_MATRIX *mat, int row, int col)
{
  GSList *row_node;
  LIL_MATRIX_ELEMENT *el;
  check_row_number_argument(row, mat);
  check_column_number_argument(col, mat);
  row_node = (GSList *) g_slist_nth(mat->rows, row)->data;
  el = (LIL_MATRIX_ELEMENT *) lil_matrix_find_element_in_row(mat, row_node, col);
  return (el ? el->value : 0);
}

static void lil_matrix_element_for_mpz (mpz_t val, LIL_MATRIX *mat, int row, int col)
{
  GSList *row_node;
  LIL_MATRIX_ELEMENT_MPZ *el;
  check_row_number_argument(row, mat);
  check_column_number_argument(col, mat);
  row_node = (GSList *) g_slist_nth(mat->rows, row)->data;
  el = (LIL_MATRIX_ELEMENT_MPZ *) lil_matrix_find_element_in_row(mat, row_node, col);
  if (el) {
    mpz_set(val, el->value);
  } else {
    mpz_set_si(val, 0);
  }
}

int lil_matrix_element (LIL_MATRIX *mat, int row, int col)
{
  if (mat->use_gmp_p) {
    int v;
    mpz_t val;
    mpz_init(val);
    lil_matrix_element_for_mpz(val, mat, row, col);
    if (mpz_cmp_si(val, INT_MAX) > 0) {
      gmp_fprintf(stderr, "Can not convert to int: %Zd\n", val);
    }
    v = mpz_get_si(val);
    mpz_clear(val);
    return v;
  } else {
    return lil_matrix_element_for_int(mat, row, col);
  }
}

void lil_matrix_element_mpz (mpz_t val, LIL_MATRIX *mat, int row, int col)
{
  if (mat->use_gmp_p) {
    lil_matrix_element_for_mpz(val, mat, row, col);
  } else {
    mpz_set_si(val, lil_matrix_element_for_int(mat, row, col));
  }
}

static void lil_matrix_row_list_node_delete (LIL_MATRIX *mat, GSList *row_list_node)
{
  GSList *row_node;
  if (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    lil_matrix_row_data_clear(row_node, mat->use_gmp_p);
    mat->rows = g_slist_delete_link(mat->rows, row_list_node);
  }
  mat->row_size -= 1;
}

void lil_matrix_row_delete (LIL_MATRIX *mat, int row)
{
  check_row_number_argument(row, mat);
  lil_matrix_row_list_node_delete(mat, g_slist_nth(mat->rows, row));
}

static void lil_matrix_column_last_delete (LIL_MATRIX *mat)
{
  GSList *row_list_node, *row_node, *el_node;
  LIL_MATRIX_ELEMENT *el;
  mat->column_size -= 1;
  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    el_node = g_slist_last(row_node);
    if (el_node) {
      el = (LIL_MATRIX_ELEMENT *) el_node->data;
      if (el->column == mat->column_size) {
        row_node = lil_matrix_element_link_deletion_with_free(mat, row_node, el_node);
        row_list_node->data = row_node;
      }
    }
    row_list_node = g_slist_next(row_list_node);
  }
}

static void lil_matrix_decrease_column_number (GSList *el_node)
{
  LIL_MATRIX_ELEMENT *el;
  while (el_node) {
    el = (LIL_MATRIX_ELEMENT *) el_node->data;
    el->column -= 1;
    el_node = g_slist_next(el_node);
  }
}

void lil_matrix_column_delete (LIL_MATRIX *mat, int col)
{
  GSList *row_node, *row_list_node, *el_node;
  int el_col;
  check_column_number_argument(col, mat);
  if (col == (mat->column_size - 1)) {
    lil_matrix_column_last_delete(mat);
    return;
  }
  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    el_node = g_slist_nth(row_node, 0);
    while (el_node) {
      el_col = lil_matrix_element_column_number(mat, el_node->data);
      if (el_col == col) {
        row_node = lil_matrix_next_element_node_with_link_deletion(mat, row_node, &el_node);
        lil_matrix_decrease_column_number(el_node);
        break;
      } else if (el_col > col) {
        lil_matrix_decrease_column_number(el_node);
        break;
      }
      el_node = g_slist_next(el_node);
    }
    row_list_node->data = row_node;
    row_list_node = g_slist_next(row_list_node);
  }
  mat->column_size -= 1;
}

void lil_matrix_row_exchange(LIL_MATRIX *mat, int row1, int row2)
{
  gpointer ptr;
  GSList *row_list_node1, *row_list_node2;
  check_row_number_argument(row1, mat);
  check_row_number_argument(row2, mat);
  if (row1 == row2) return;
  row_list_node1 = g_slist_nth(mat->rows, row1);
  row_list_node2 = g_slist_nth(mat->rows, row2);
  ptr = row_list_node1->data;
  row_list_node1->data = row_list_node2->data;
  row_list_node2->data = ptr;
}

static void lil_matrix_column_exchange_int (LIL_MATRIX *mat, int col1, int col2)
{
  int val1, val2;
  GSList *row_list_node, *row_node, *el_node, *el_node1, *el_node2;
  LIL_MATRIX_ELEMENT *el;
  check_column_number_argument(col1, mat);
  check_column_number_argument(col2, mat);
  if (col1 == col2) {
    return;
  } else if (col1 > col2) {
    val1 = col1;
    col1 = col2;
    col2 = val1;
  }
  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      el_node = (GSList *) g_slist_nth(row_node, 0);
      el_node1 = NULL;
      el_node2 = NULL;
      val1 = 0;
      val2 = 0;
      while (el_node) {
        el = (LIL_MATRIX_ELEMENT *) el_node->data;
        if (!el_node1 && el->column >= col1) {
          if (el->column == col1) {
            val1 = el->value;
          }
          el_node1 = el_node;
        }
        if (el->column >= col2) {
          if (el->column == col2) {
            val2 = el->value;
          }
          el_node2 = el_node;
          break;
        }
        el_node = g_slist_next(el_node);
      }
      if (val1 != 0 && val2 != 0) {
        ((LIL_MATRIX_ELEMENT *) el_node1->data)->value = val2;
        ((LIL_MATRIX_ELEMENT *) el_node2->data)->value = val1;
        row_node = NULL;
      } else if (val1 != 0) {
        el = (LIL_MATRIX_ELEMENT *) el_node1->data;
        el->column = col2;
        if (el_node2) {
          row_node = g_slist_insert_before(row_node, el_node2, el_node1->data);
        } else {
          row_node = g_slist_append(row_node, el_node1->data);
        }
        row_node = g_slist_delete_link(row_node, el_node1);
      } else if (val2 != 0) {
        el = (LIL_MATRIX_ELEMENT *) el_node2->data;
        el->column = col1;
        row_node = g_slist_insert_before(row_node, el_node1, el_node2->data);
        row_node = g_slist_delete_link(row_node, el_node2);
      } else {
        row_node = NULL;
      }
    }
    if (row_node) {
      row_list_node->data = row_node;
    }
    row_list_node = g_slist_next(row_list_node);
  }
}

void lil_matrix_column_exchange(LIL_MATRIX *mat, int col1, int col2)
{
  if (mat->use_gmp_p) {
    fprintf(stderr, "Not yet implement lil_matrix_column_exchange for mpz case\n");
    abort_with_line_number();
  } else {
    lil_matrix_column_exchange_int(mat, col1, col2);
  }
}

/* Return true if the value is zero. */
static bool lil_matrix_divide_element_node (LIL_MATRIX *mat, GSList *el_node, gpointer num)
{
  if (mat->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el;
    el = (LIL_MATRIX_ELEMENT_MPZ *) el_node->data;
    mpz_tdiv_q(el->value, el->value, (mpz_ptr) num);
    return (mpz_cmp_ui(el->value, 0) == 0);
  } else {
    LIL_MATRIX_ELEMENT *el;
    el = (LIL_MATRIX_ELEMENT *) el_node->data;
    el->value = el->value / *((int *) num);
    return (el->value == 0);
  }
}

static void lil_matrix_divide_elements_with_gpointer (LIL_MATRIX *mat, gpointer num)
{
  GSList *row_list_node, *row_node, *el_node;
  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      el_node = g_slist_nth(row_node, 0);
      while (el_node) {
        if (lil_matrix_divide_element_node(mat, el_node, num)) {
          row_node = lil_matrix_next_element_node_with_link_deletion(mat, row_node, &el_node);
        } else {
          el_node = g_slist_next(el_node);
        }
      }
    }
    row_list_node->data = row_node;
    row_list_node = g_slist_next(row_list_node);
  }
}

void lil_matrix_divide_elements (LIL_MATRIX *mat, int num)
{
  if (mat->use_gmp_p) {
    mpz_t n;
    mpz_init(n);
    mpz_set_ui(n, num);
    lil_matrix_divide_elements_with_gpointer(mat, (gpointer) n);
    mpz_clear(n);
  } else {
    lil_matrix_divide_elements_with_gpointer(mat, (gpointer) &num);
  }
}

void lil_matrix_divide_elements_mpz (LIL_MATRIX *mat, mpz_t num)
{
  if (mat->use_gmp_p) {
    lil_matrix_divide_elements_with_gpointer(mat, (gpointer) num);
  } else {
    int n;
    n = mpz_get_si(num);
    lil_matrix_divide_elements_with_gpointer(mat, (gpointer) &n);
  }
}

static void lil_matrix_add_rows_for_int (LIL_MATRIX *mat, GSList *row_list_node_target, GSList *row_list_node_source, int factor)
{
  int val;
  GSList *row_node_target, *row_node_source, *el_node_target, *el_node_source, *el_node_target_last;
  LIL_MATRIX_ELEMENT *el_target, *el_source;
  bool already_added;
  if (!row_list_node_source) return;
  row_node_source = (GSList *) row_list_node_source->data;
  el_node_source = g_slist_nth(row_node_source, 0);
  row_node_target = (row_list_node_target ? (GSList *) row_list_node_target->data : NULL);
  el_node_target = (row_node_target ? g_slist_nth(row_node_target, 0) : NULL);
  el_node_target_last = NULL;
  while (el_node_source) {
    already_added = false;
    el_source = (LIL_MATRIX_ELEMENT *) el_node_source->data;
    while (el_node_target) {
      el_target = (LIL_MATRIX_ELEMENT *) el_node_target->data;
      if (el_source->column == el_target->column) {
        el_target->value = (el_target->value + lil_matrix_multiply(el_source->value, factor)) % mat->modulo;
        if (el_target->value == 0) {
          row_node_target = lil_matrix_next_element_node_with_link_deletion(mat, row_node_target, &el_node_target);
          el_node_target_last = NULL;
        }
        already_added = true;
        break;
      } else if (el_source->column < el_target->column) {
        val = lil_matrix_multiply(el_source->value, factor) % mat->modulo;
        if (val != 0) {
          if (el_node_target_last) {
            el_node_target_last->next = g_slist_alloc();
            el_node_target_last->next->data = lil_matrix_element_new(el_source->column, val);
            el_node_target_last->next->next = el_node_target;
            el_node_target_last = g_slist_next(el_node_target_last);
          } else {
            row_node_target = g_slist_insert_before(row_node_target, el_node_target, lil_matrix_element_new(el_source->column, val));
          }
        }
        already_added = true;
        break;
      }
      el_node_target_last = el_node_target;
      el_node_target = g_slist_next(el_node_target);
    }
    if (!already_added) {
      val = lil_matrix_multiply(el_source->value, factor) % mat->modulo;
      if (val != 0) {
        lil_g_slist_append_with_last(row_node_target, el_node_target_last, lil_matrix_element_new(el_source->column, val));
      }
    }
    el_node_source = g_slist_next(el_node_source);
  }
  row_list_node_target->data = row_node_target;
}

/* Note that modulo is zero, that is, we do not use modulo,  when we use mpz */
static void lil_matrix_add_rows_for_mpz (LIL_MATRIX *mat, GSList *row_list_node_target, GSList *row_list_node_source, mpz_t factor)
{
  GSList *row_node_target, *row_node_source, *el_node_target, *el_node_source, *el_node_target_last;
  LIL_MATRIX_ELEMENT_MPZ *el_target, *el_source;
  bool already_added;
  mpz_t val;
  mpz_init(val);
  if (!row_list_node_source) return;
  row_node_source = (GSList *) row_list_node_source->data;
  el_node_source = g_slist_nth(row_node_source, 0);
  row_node_target = (row_list_node_target ? (GSList *) row_list_node_target->data : NULL);
  el_node_target = (row_node_target ? g_slist_nth(row_node_target, 0) : NULL);
  el_node_target_last = NULL;
  while (el_node_source) {
    already_added = false;
    el_source = (LIL_MATRIX_ELEMENT_MPZ *) el_node_source->data;
    while (el_node_target) {
      el_target = (LIL_MATRIX_ELEMENT_MPZ *) el_node_target->data;
      if (el_source->column == el_target->column) {
        mpz_mul(val, el_source->value, factor);
        mpz_add(el_target->value, val, el_target->value);
        if (mpz_cmp_ui(el_target->value, 0) == 0) {
          row_node_target = lil_matrix_next_element_node_with_link_deletion(mat, row_node_target, &el_node_target);
          el_node_target_last = NULL;
        }
        already_added = true;
        break;
      } else if (el_source->column < el_target->column) {
        mpz_mul(val, el_source->value, factor);
        if (mpz_cmp_si(val, 0) != 0) {
          if (el_node_target_last) {
            el_node_target_last->next = g_slist_alloc();
            el_node_target_last->next->data = lil_matrix_element_mpz_new(el_source->column, val);
            el_node_target_last->next->next = el_node_target;
            el_node_target_last = g_slist_next(el_node_target_last);
          } else {
            row_node_target = g_slist_insert_before(row_node_target, el_node_target, lil_matrix_element_mpz_new(el_source->column, val));
          }
        }
        already_added = true;
        break;
      }
      el_node_target_last = el_node_target;
      el_node_target = g_slist_next(el_node_target);
    }
    if (!already_added) {
      mpz_mul(val, el_source->value, factor);
      if (mpz_cmp_si(val, 0) != 0) {
        lil_g_slist_append_with_last(row_node_target, el_node_target_last, lil_matrix_element_mpz_new(el_source->column, val));
      }
    }
    el_node_source = g_slist_next(el_node_source);
  }
  row_list_node_target->data = row_node_target;
  mpz_clear(val);
}

static void lil_matrix_add_rows_mpz_factor_for_int (LIL_MATRIX *mat, GSList *row_list_node_target, GSList *row_list_node_source, int factor)
{
  mpz_t factor_mpz;
  mpz_init(factor_mpz);
  mpz_set_si(factor_mpz, factor);
  lil_matrix_add_rows_for_mpz(mat, row_list_node_target, row_list_node_source, factor_mpz);
  mpz_clear(factor_mpz);
}

void lil_matrix_add_rows (LIL_MATRIX *mat, int row_target, int row_source, int factor)
{
  GSList *row_list_node_target, *row_list_node_source;
  check_row_number_argument(row_target, mat);
  check_row_number_argument(row_source, mat);
  if (row_target == row_source) {
    lil_matrix_print(mat);
    lil_matrix_debug_print(mat);
    fprintf(stderr, "It is not yet implemented to add a row to the same row: %d %d\n", row_target, row_source);
    abort_with_line_number();
  }
  row_list_node_target = g_slist_nth(mat->rows, row_target);
  row_list_node_source = g_slist_nth(mat->rows, row_source);
  if (mat->use_gmp_p) {
    lil_matrix_add_rows_mpz_factor_for_int(mat, row_list_node_target, row_list_node_source, factor);
  } else {
    lil_matrix_add_rows_for_int(mat, row_list_node_target, row_list_node_source, factor);
  }
}

static void lil_matrix_add_row_nodes_mpz (LIL_MATRIX *mat, GSList *row_list_node_target, GSList *row_list_node_source, mpz_t factor)
{
  if (mat->use_gmp_p) {
    lil_matrix_add_rows_for_mpz(mat, row_list_node_target, row_list_node_source, factor);
  } else {
    lil_matrix_add_rows_for_int(mat, row_list_node_target, row_list_node_source, mpz_get_si(factor));
  }
}

void lil_matrix_add_rows_mpz (LIL_MATRIX *mat, int row_target, int row_source, mpz_t factor)
{
  GSList *row_list_node_target, *row_list_node_source;
  check_row_number_argument(row_target, mat);
  check_row_number_argument(row_source, mat);
  if (row_target == row_source) {
    lil_matrix_print(mat);
    lil_matrix_debug_print(mat);
    fprintf(stderr, "It is not yet implemented to add a row to the same row: %d %d\n", row_target, row_source);
    abort_with_line_number();
  }
  row_list_node_target = g_slist_nth(mat->rows, row_target);
  row_list_node_source = g_slist_nth(mat->rows, row_source);
  lil_matrix_add_row_nodes_mpz(mat, row_list_node_target, row_list_node_source, factor);
}

/* Return true if the value is zero. */
static bool lil_matrix_mul_element_node (LIL_MATRIX *mat, GSList *el_node, int num)
{
  if (mat->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el;
    el = (LIL_MATRIX_ELEMENT_MPZ *) el_node->data;
    mpz_mul_si(el->value, el->value, num);
    return (mpz_cmp_ui(el->value, 0) == 0);
  } else {
    LIL_MATRIX_ELEMENT *el;
    el = (LIL_MATRIX_ELEMENT *) el_node->data;
    el->value = lil_matrix_multiply(el->value, num) % mat->modulo;
    return (el->value == 0);
  }
}

void lil_matrix_row_mul_scalar (LIL_MATRIX *mat, int row, int factor)
{
  GSList *row_list_node, *row_node, *el_node;
  row_list_node = g_slist_nth(mat->rows, row);
  if (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    el_node = g_slist_nth(row_node, 0);
    while (el_node) {
      if (lil_matrix_mul_element_node(mat, el_node, factor)) {
        row_node = lil_matrix_next_element_node_with_link_deletion(mat, row_node, &el_node);
      } else {
        el_node = g_slist_next(el_node);
      }
    }
    row_list_node->data = row_node;
  }
}

void lil_matrix_column_mul_scalar (LIL_MATRIX *mat, int col, int factor)
{
  GSList *row_list_node, *row_node, *el_node;
  int el_col;
  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      el_node = g_slist_nth(row_node, 0);
      while (el_node) {
        el_col = lil_matrix_element_column_number(mat, el_node->data);
        if (el_col == col) {
          if (lil_matrix_mul_element_node(mat, el_node, factor)) {
            row_node = lil_matrix_next_element_node_with_link_deletion(mat, row_node, &el_node);
          } else {
            el_node = g_slist_next(el_node);
          }
        } else if (el_col > col) {
          break;
        } else {
          el_node = g_slist_next(el_node);
        }
      }
      row_list_node->data = row_node;
    }
    row_list_node = g_slist_next(row_list_node);
  }
}

static void lil_matrix_add_columns_for_int (LIL_MATRIX *mat, int col_target, int col_source, int factor)
{
  int val_target, val_source, val;
  GSList *row_list_node, *row_node, *el_node, *el_node_target, *el_node_source;
  LIL_MATRIX_ELEMENT *el;
  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      el_node = (GSList *) g_slist_nth(row_node, 0);
      el_node_target = NULL;
      el_node_source = NULL;
      val_target = 0;
      val_source = 0;
      while (el_node) {
        el = (LIL_MATRIX_ELEMENT *) el_node->data;
        if (!el_node_target && el->column >= col_target) {
          el_node_target = el_node;
          if (el->column == col_target) {
            val_target = el->value;
          }
        }
        if (!el_node_source && el->column >= col_source) {
          el_node_source = el_node;
          if (el->column == col_source) {
            val_source = el->value;
          } else {
            break;
          }
        }
        el_node = g_slist_next(el_node);
      }
      if (val_source != 0) {
        if (val_target != 0) {
          el = (LIL_MATRIX_ELEMENT *) el_node_target->data;
          el->value = (el->value + lil_matrix_multiply(val_source, factor)) % mat->modulo;
          if (el->value == 0) {
            row_node = lil_matrix_element_link_deletion_with_free(mat, row_node, el_node_target);
          }
        } else {
          val = lil_matrix_multiply(val_source, factor) % mat->modulo;
          if (val != 0) {
            row_node = g_slist_insert_before(row_node, el_node_target, lil_matrix_element_new(col_target, val));
          }
        }
      }
    }
    row_list_node->data = row_node;
    row_list_node = g_slist_next(row_list_node);
  }
}

/* Note that modulo is zero, that is, we do not use modulo,  when we use mpz */
static void lil_matrix_add_columns_for_mpz (LIL_MATRIX *mat, int col_target, int col_source, mpz_t factor)
{
  GSList *row_list_node, *row_node, *el_node, *el_node_target, *el_node_source;
  LIL_MATRIX_ELEMENT_MPZ *el;
  mpz_t val_target, val_source, val;
  mpz_init(val_target);
  mpz_init(val_source);
  mpz_init(val);
  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      el_node = (GSList *) g_slist_nth(row_node, 0);
      el_node_target = NULL;
      el_node_source = NULL;
      mpz_set_si(val_target, 0);
      mpz_set_si(val_source, 0);
      while (el_node) {
        el = (LIL_MATRIX_ELEMENT_MPZ *) el_node->data;
        if (!el_node_target && el->column >= col_target) {
          el_node_target = el_node;
          if (el->column == col_target) {
            mpz_set(val_target, el->value);
          }
        }
        if (!el_node_source && el->column >= col_source) {
          el_node_source = el_node;
          if (el->column == col_source) {
            mpz_set(val_source, el->value);
          } else {
            break;
          }
        }
        el_node = g_slist_next(el_node);
      }
      if (mpz_cmp_si(val_source, 0) != 0) {
        if (mpz_cmp_si(val_target, 0) != 0) {
          el = (LIL_MATRIX_ELEMENT_MPZ *) el_node_target->data;
          mpz_mul(val, val_source, factor);
          mpz_add(el->value, el->value, val);
          if (mpz_cmp_si(el->value, 0) == 0) {
            row_node = lil_matrix_element_link_deletion_with_free(mat, row_node, el_node_target);
          }
        } else {
          mpz_mul(val, val_source, factor);
          if (mpz_cmp_si(val, 0) != 0) {
            row_node = g_slist_insert_before(row_node, el_node_target, lil_matrix_element_mpz_new(col_target, val));
          }
        }
      }
    }
    row_list_node->data = row_node;
    row_list_node = g_slist_next(row_list_node);
  }
  mpz_clear(val_target);
  mpz_clear(val_source);
  mpz_clear(val);
}

void lil_matrix_add_columns (LIL_MATRIX *mat, int col_target, int col_source, int factor)
{
  check_column_number_argument(col_target, mat);
  check_column_number_argument(col_source, mat);
  if (col_target == col_source) {
    fprintf(stderr, "It is not yet implemented to add a column to the same column (lil_matrix_add_columns):\n  col_target=%d, col_source=%d\n",
            col_target, col_source);
    abort_with_line_number();
  }
  if (mat->use_gmp_p) {
    mpz_t factor_mpz;
    mpz_init(factor_mpz);
    mpz_set_si(factor_mpz, factor);
    lil_matrix_add_columns_for_mpz(mat, col_target, col_source, factor_mpz);
    mpz_clear(factor_mpz);
  } else {
    lil_matrix_add_columns_for_int(mat, col_target, col_source, factor);
  }
}

void lil_matrix_add_columns_mpz (LIL_MATRIX *mat, int col_target, int col_source, mpz_t factor)
{
  check_column_number_argument(col_target, mat);
  check_column_number_argument(col_source, mat);
  if (col_target == col_source) {
    fprintf(stderr, "It is not yet implemented to add a column to the same column (lil_matrix_add_columns_mpz):\n  col_target=%d, col_source=%d\n",
            col_target, col_source);
    abort_with_line_number();
  }
  if (mat->use_gmp_p) {
    lil_matrix_add_columns_for_mpz(mat, col_target, col_source, factor);
  } else {
    lil_matrix_add_columns_for_int(mat, col_target, col_source, mpz_get_si(factor));
  }
}

void lil_matrix_transpose (LIL_MATRIX *mat)
{
  int tmp, row_num, col;
  GSList *row_old_list_node, *row_old_node, *row_list_node, *row_list_old, *el_old_node;
  gpointer el_new;
  row_list_old = mat->rows;
  tmp = mat->column_size;
  mat->column_size = mat->row_size;
  mat->row_size = tmp;
  mat->rows = lil_matrix_rows_new(mat->row_size);
  row_old_list_node = g_slist_nth(row_list_old, 0);
  row_num = 0;
  while (row_old_list_node) {
    row_old_node = row_old_list_node->data;
    if (row_old_node) {
      el_old_node = g_slist_nth(row_old_node, 0);
      while (el_old_node) {
        col = lil_matrix_element_column_number(mat, el_old_node->data);
        row_list_node = g_slist_nth(mat->rows, col);
        el_new = lil_matrix_element_copy(mat, el_old_node->data);
        if (mat->use_gmp_p) {
          ((LIL_MATRIX_ELEMENT_MPZ *) el_new)->column = row_num;
        } else {
          ((LIL_MATRIX_ELEMENT *) el_new)->column = row_num;
        }
        row_list_node->data = g_slist_append((GSList *) row_list_node->data, el_new);
        el_old_node = g_slist_next(el_old_node);
      }
    }
    lil_matrix_row_data_clear(row_old_list_node->data, mat->use_gmp_p);
    row_old_list_node->data = NULL;
    row_old_list_node = g_slist_next(row_old_list_node);
    row_num += 1;
  }
  lil_matrix_clear_rows(row_list_old, mat->use_gmp_p);
}

/* Return true if the element is not zero and set the absolute value to val_abs. */
static bool lil_matrix_abs_non_zero_element_p(mpz_t val_abs, LIL_MATRIX *mat, gpointer el_data)
{
  if (mat->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el_mpz;
    el_mpz = (LIL_MATRIX_ELEMENT_MPZ *) el_data;
    mpz_abs(val_abs, el_mpz->value);
    return (mpz_cmp_ui(val_abs, 0) != 0);
  } else {
    LIL_MATRIX_ELEMENT *el_int;
    el_int = (LIL_MATRIX_ELEMENT *) el_data;
    mpz_set_si(val_abs, abs(el_int->value));
    return (el_int->value != 0);
  }
}

/*
 * *col_min is a negative integer if all columns are zero.
 */
static void lil_matrix_minimal_non_zero_element_at_row (LIL_MATRIX *mat, GSList *row_node, int *col_min, mpz_t val_abs_min, int *el_num)
{
  GSList *el_node;
  gpointer el_data;
  bool nonzero;
  int col;
  mpz_t val_abs;
  mpz_init(val_abs);
  *col_min = -1;
  mpz_set_ui(val_abs_min, 0);
  *el_num = 0;
  if (row_node) {
    el_node = g_slist_nth(row_node, 0);
    while (el_node) {
      *el_num += 1;
      el_data = el_node->data;
      col = lil_matrix_element_column_number(mat, el_data);
      if (col <= mat->column_size) {
        nonzero = lil_matrix_abs_non_zero_element_p(val_abs, mat, el_data);
        if (nonzero && (mpz_cmp_ui(val_abs, 0) > 0 && (*col_min < 0 || mpz_cmp(val_abs, val_abs_min) < 0))) {
          mpz_set(val_abs_min, val_abs);
          *col_min = col;
        }
      }
      el_node = g_slist_next(el_node);
    }
  }
  mpz_clear(val_abs);
}

static LIL_MATRIX_MIN_VALUE_CACHE *lil_matrix_min_value_data_new ()
{
  LIL_MATRIX_MIN_VALUE_CACHE *data;
  data = g_malloc(sizeof(LIL_MATRIX_MIN_VALUE_CACHE));
  mpz_init(data->value);
  return data;
}

GPtrArray *lil_matrix_column_index_cache_new (int column_size)
{
  int i;
  GPtrArray *column_index_cache;
  column_index_cache = g_ptr_array_sized_new(column_size);
  for (i = 0; i < column_size; i++) {
    g_ptr_array_add(column_index_cache, NULL);
  }
  return column_index_cache;
}

static GArray *lil_matrix_column_index_cache_get (GPtrArray *column_index_cache, int col)
{
  return (GArray *) g_ptr_array_index(column_index_cache, col);
}

static GArray *lil_matrix_column_index_cache_get_create (GPtrArray *column_index_cache, int col)
{
  GArray *cache;
  cache = lil_matrix_column_index_cache_get(column_index_cache, col);
  if (!cache) {
    g_ptr_array_index(column_index_cache, col) = (gpointer) g_array_new(false, false, sizeof(int));
    cache = g_ptr_array_index(column_index_cache, col);
  }
  return cache;
}

static void lil_matrix_column_index_cache_add (GPtrArray *column_index_cache, int col, int row)
{
  g_array_append_val(lil_matrix_column_index_cache_get_create(column_index_cache, col), row);
}

/* For debug */
void lil_matrix_column_index_cache_save (GPtrArray *column_index_cache, GString *path)
{
  int i, j;
  GArray *row_nums;
  FILE *fp;
  fp = fopen(path->str, "w");
  for (i = 0; i < column_index_cache->len; i++) {
    row_nums = g_ptr_array_index(column_index_cache, i);
    if (row_nums) {
      if (row_nums->len == 0) {
        fprintf(fp, "empty\n");
      } else {
        for (j = 0; j < row_nums->len; j++) {
          fprintf(fp, "%d", g_array_index(row_nums, int, j));
          if (j == (row_nums->len - 1)) {
            fprintf(fp, "\n");
          } else {
            fprintf(fp, " ");
          }
        }
      }
    } else {
      fprintf(fp, "NULL\n");
    }
  }
  fclose(fp);
}

static void lil_matrix_column_index_set_row (GPtrArray *column_index_cache, LIL_MATRIX *mat, GSList *row_node, int row_ind)
{
  int col_ind;
  GSList *el_node;
  if (row_node) {
    el_node = g_slist_nth(row_node, 0);
    while (el_node) {
      col_ind = lil_matrix_element_column_number(mat, el_node->data);
      lil_matrix_column_index_cache_add(column_index_cache, col_ind, row_ind);
      el_node = g_slist_next(el_node);
    }
  }
}

/* static GPtrArray *lil_matrix_column_index_set (GPtrArray *column_index_cache, LIL_MATRIX *mat, int row_num_start, int row_num_end) */
/* { */
/*   int row_ind; */
/*   GSList *row_list_node, *row_node; */
/*   row_ind = row_num_start; */
/*   row_list_node = g_slist_nth(mat->rows, row_ind); */
/*   while (row_list_node) { */
/*     row_node = (GSList *) row_list_node->data; */
/*     lil_matrix_column_index_set_row(column_index_cache, mat, row_node, row_ind); */
/*     row_list_node = g_slist_next(row_list_node); */
/*     row_ind += 1; */
/*     if (row_ind > row_num_end) { */
/*       break; */
/*     } */
/*   } */
/*   return column_index_cache; */
/* } */

/* static GPtrArray *lil_matrix_column_index_cache_create (LIL_MATRIX *mat) */
/* { */
/*   GPtrArray *column_index_cache; */
/*   column_index_cache = lil_matrix_column_index_cache_new(mat->column_size); */
/*   return lil_matrix_column_index_set(column_index_cache, mat, 0, mat->row_size - 1); */
/* } */

int compare_integers(gconstpointer a, gconstpointer b) {
  return *((int *) a) - *((int *) b);
}

static GArray *array_intersection (GArray *ary1, GArray *ary2)
{
  int i, j, r, r2;
  GArray *ary_ret;
  ary_ret = g_array_sized_new(false, false, sizeof(int), MIN(ary1->len, ary2->len));
  j = 0;
  for (i = 0; i < ary1->len; i++) {
    r = g_array_index(ary1, int, i);
    r2 = g_array_index(ary2, int, j);
    while (r > r2) {
      j += 1;
      if (j >= ary2->len) {
        j = -1;
        break;
      }
      r2 = g_array_index(ary2, int, j);
    }
    if (j < 0) {
      break;
    }
    if (r == r2) {
      g_array_append_val(ary_ret, r);
      j += 1;
      if (j >= ary2->len) {
        break;
      }
    }
  }
  if (ary_ret->len == 0) {
    g_array_free(ary_ret, TRUE);
    ary_ret = NULL;
  }
  return ary_ret;
}

static bool array_intersect_p (GArray *ary1, GArray *ary2)
{
  int i, j, r, r2;
  j = 0;
  for (i = 0; i < ary1->len; i++) {
    r = g_array_index(ary1, int, i);
    r2 = g_array_index(ary2, int, j);
    while (r > r2) {
      j += 1;
      if (j >= ary2->len) {
        return false;
      }
      r2 = g_array_index(ary2, int, j);
    }
    if (r == r2) {
      return true;
    }
  }
  return false;
}

static GArray *array_union (GArray *ary1, GArray *ary2)
{
  int i, ind_last;
  GArray *ary_ret;
  ary_ret = g_array_sized_new(false, false, sizeof(int), ary1->len + ary2->len);
  g_array_append_vals(ary_ret, ary1->data, ary1->len);
  g_array_append_vals(ary_ret, ary2->data, ary2->len);
  g_array_sort(ary_ret, compare_integers);
  ind_last = 0;
  for (i = 1; i < ary_ret->len; i++) {
    if (g_array_index(ary_ret, int, ind_last) != g_array_index(ary_ret, int, i)) {
      ind_last += 1;
      if (ind_last != i) {
        g_array_index(ary_ret, int, ind_last) = g_array_index(ary_ret, int, i);
      }
    }
  }
  ind_last += 1;
  if (ind_last < ary_ret->len) {
    g_array_remove_range(ary_ret, ind_last, ary_ret->len - ind_last);
  }
  return ary_ret;
}

static bool array_include_p (GArray *array, int num)
{
  return (bsearch(&num, array->data, array->len, sizeof(int), compare_integers) ? true : false);
}

/*
  array must be sorted to use binary search.
  If array->len is zero, we get -1. Otherwise, the first index number whose value is larger than or equal to num.
*/
int array_find_number (GArray *array, int num, bool *find_p)
{
  int head, tail, ind_new, num_new, ind_ret;
  if (array->len == 0) {
    if (find_p) {
      *find_p = false;
    }
    return ARRAY_EMPTY;
  }
  head = 0;
  tail = array->len - 1;
  num_new = g_array_index(array, int, tail);
  if (num_new < num) {
    if (find_p) {
      *find_p = false;
    }
    return ARRAY_ALL_LESS;
  }
  num_new = g_array_index(array, int, head);
  if (num_new >= num) {
    ind_ret = head;
  } else {
    while (tail - head > 1) {
      ind_new = (head + tail) / 2;
      num_new = g_array_index(array, int, ind_new);
      if (num_new == num) {
        if (find_p) {
          *find_p = true;
        }
        return ind_new;
      } else if (num_new > num) {
        tail = ind_new;
      } else {
        head = ind_new;
      }
    }
    ind_ret = tail;
    num_new = g_array_index(array, int, tail);
  }
  if (find_p) {
    *find_p = (num_new == num);
  }
  return ind_ret;
}

LIL_SNF_ARG *lil_snf_argument_new ()
{
  LIL_SNF_ARG *arg;
  arg = (LIL_SNF_ARG *) g_malloc(sizeof(LIL_SNF_ARG));
  arg->debug = false;
  arg->info = 0;
  arg->validate = 0;
  arg->reduce_by_divisors = 0;
  arg->reduce_by_increase = DEFAULT_REDUCTION_ELEMENTS_INCREASE;
  arg->max_estimate_increase = DEFAULT_MAX_ELEMENTS_INCREASE;
  arg->min_pivot_number = DEFAULT_PIVOT_NUM;
  arg->precise_estimate = false;
  arg->auto_delete_duplicates = false;
  arg->reduce_repeatedly = false;
  arg->column_ops_output = NULL;
  arg->load_directory = NULL;
  arg->stat = NULL;
  arg->cache = NULL;
  arg->signal_use_p = false;
  return arg;
}

void lil_snf_argument_free (LIL_SNF_ARG *arg)
{
  if (arg->load_directory) {
    g_free(arg->load_directory);
  }
  g_free(arg);
}

static void lil_matrix_min_value_data_free (LIL_MATRIX_MIN_VALUE_CACHE *data)
{
  mpz_clear(data->value);
  g_free(data);
}

static void lil_snf_argument_cache_row_minimal_clear (LIL_SNF_ARG *arg)
{
  int i;
  LIL_MATRIX_MIN_VALUE_CACHE *cache;
  for (i = 0; i < arg->cache->row_minimal->len; i++) {
    cache = (LIL_MATRIX_MIN_VALUE_CACHE *) g_ptr_array_index(arg->cache->row_minimal, i);
    if (cache) {
      lil_matrix_min_value_data_free(cache);
    }
  }
  g_ptr_array_free(arg->cache->row_minimal, TRUE);
  arg->cache->row_minimal = g_ptr_array_new();
}

void lil_snf_argument_cache_column_index_clear (GPtrArray *column_index)
{
  int i;
  GArray *cache;
  for (i = 0; i < column_index->len; i++) {
    cache = g_ptr_array_index(column_index, i);
    if(cache) {
      g_array_free(cache, TRUE);
    }
  }
  g_ptr_array_free(column_index, TRUE);
}

static void lil_snf_argument_cache_column_index_init (LIL_SNF_ARG *arg)
{
  lil_snf_argument_cache_column_index_clear(arg->cache->column_index);
  arg->cache->column_index = g_ptr_array_new();
}

/* Clear row_minimal, row_changed, and row_recreated. cumulative_row_changed is updated. */
void lil_snf_argument_cache_reset (LIL_SNF_ARG *arg, LIL_MATRIX_CACHE_TYPE type, LIL_MATRIX *mat)
{
  int i;
  lil_snf_argument_cache_row_minimal_clear(arg);
  for (i = 0; i < mat->row_size; i++) {
    g_ptr_array_add(arg->cache->row_minimal, NULL);
  }
  if (arg->cache->type == CACHE_BOTH) {
    lil_snf_argument_cache_column_index_init(arg);
    for (i = 0; i < mat->column_size; i++) {
      g_ptr_array_add(arg->cache->column_index, NULL);
    }
  }
  if (arg->cache->row_changed->len > 0) {
    GArray *tmp;
    tmp = array_union(arg->cache->cumulative_row_changed, arg->cache->row_changed);
    g_array_free(arg->cache->cumulative_row_changed, TRUE);
    arg->cache->cumulative_row_changed = tmp;
    g_array_remove_range(arg->cache->row_changed, 0, arg->cache->row_changed->len);
  }
  if (arg->cache->row_recreated->len > 0) {
    g_array_remove_range(arg->cache->row_recreated, 0, arg->cache->row_recreated->len);
  }
}

void lil_snf_argument_cache_init (LIL_SNF_ARG *arg, LIL_MATRIX_CACHE_TYPE type, LIL_MATRIX *mat)
{
  /* Set false only in this function and set true in the last of lil_matrix_minimal_non_zero_element */
  if (!arg->stat) {
    arg->stat = g_malloc(sizeof(CalcStat));
    arg->stat->step = 0;
    arg->stat->elementary_divisors = 0;
    arg->stat->empty_rows = 0;
    arg->stat->row_operations = 0;
    arg->stat->last_validating = 0;
    arg->stat->last_output = 0;
    arg->stat->last_reducing = 0;
    arg->stat->increasing = false;
  }
  if (!arg->cache) {
    arg->cache = (LIL_MATRIX_CACHE *) g_malloc(sizeof(LIL_MATRIX_CACHE));
    arg->cache->column_index = g_ptr_array_new();
    arg->cache->row_minimal = g_ptr_array_new();
    arg->cache->row_changed = g_array_new(false, false, sizeof(int));
    arg->cache->row_recreated = g_array_new(false, false, sizeof(int));
    arg->cache->cumulative_row_changed = g_array_new(false, false, sizeof(int));
    arg->cache->type = CACHE_NONE;
  }
  arg->cache->set_p = false;
  if (type != CACHE_NOT_CHANGED) {
    arg->cache->type = type;
  }
  if (arg->cache->type != CACHE_NONE) {
    lil_snf_argument_cache_reset(arg, type, mat);
  }
}

void lil_snf_argument_cache_clear (LIL_SNF_ARG *arg)
{
  if (arg->stat) {
    g_free(arg->stat);
  }
  if (arg->cache) {
    lil_snf_argument_cache_row_minimal_clear(arg);
    lil_snf_argument_cache_column_index_init(arg);
    g_ptr_array_free(arg->cache->column_index, TRUE);
    g_ptr_array_free(arg->cache->row_minimal, TRUE);
    g_array_free(arg->cache->row_changed, TRUE);
    g_array_free(arg->cache->row_recreated, TRUE);
    g_array_free(arg->cache->cumulative_row_changed, TRUE);
    g_free(arg->cache);
  }
}

static GArray *lil_snf_argument_cache_take_cumulative_row_changed (LIL_SNF_ARG *arg, bool create_new_array)
{
  GArray *ary;
  ary = arg->cache->cumulative_row_changed;
  if (create_new_array) {
    arg->cache->cumulative_row_changed = g_array_new(false, false, sizeof(int));
  }
  g_array_sort(ary, compare_integers);
  return ary;
}

LIL_MATRIX_PIVOT *lil_matrix_pivot_new (int row, int column, mpz_t abs)
{
  LIL_MATRIX_PIVOT *pivot;
  pivot = (LIL_MATRIX_PIVOT *) g_malloc(sizeof(LIL_MATRIX_PIVOT));
  pivot->row = row;
  pivot->column = column;
  pivot->increase = 0;
  mpz_init(pivot->abs);
  if (abs) {
    mpz_set(pivot->abs, abs);
  } else {
    mpz_set_si(pivot->abs, 0);
  }
  return pivot;
}

static void lil_matrix_pivot_free (LIL_MATRIX_PIVOT *pivot)
{
  mpz_clear(pivot->abs);
  g_free(pivot);
}

/* Not save to cache->row_changed to use in multiple threads. Later, we must modify row_changed */
static bool lil_snf_argument_remove_row_cache_without_save_changed (LIL_SNF_ARG *arg, int row_pivot) {
  LIL_MATRIX_MIN_VALUE_CACHE *cache;
  if (!arg->cache || arg->cache->type == CACHE_NONE) {
    fprintf(stderr, "Cache is NULL or CACHE_NONE type\n");
    abort_with_line_number();
  }
  cache = (LIL_MATRIX_MIN_VALUE_CACHE *) g_ptr_array_index(arg->cache->row_minimal, row_pivot);
  if (cache) {
    lil_matrix_min_value_data_free(cache);
    g_ptr_array_index(arg->cache->row_minimal, row_pivot) = NULL;
    return true;
  }
  return false;
}

void lil_snf_argument_remove_row_cache (LIL_SNF_ARG *arg, int row_pivot)
{
  if (lil_snf_argument_remove_row_cache_without_save_changed(arg, row_pivot)) {
    g_array_append_val(arg->cache->row_changed, row_pivot);
  }
}

static GPtrArray *lil_snf_argument_column_index_cache (LIL_SNF_ARG *arg)
{
  if (arg->cache && arg->cache->type == CACHE_BOTH) {
    return (GPtrArray *) arg->cache->column_index;
  } else {
    return NULL;
  }
}

static int lil_snf_argument_find_first_row_changed (GArray *row_changed, int row_num)
{
  return array_find_number(row_changed, row_num, NULL);
}

/*
  We must sort arg->cache->row_changed for binary search.
  Remove row numbers in row_changed from column_index_cache.
*/
static void lil_snf_argument_column_index_remove_changed_rows (LIL_SNF_ARG *arg, GArray *row_nums_to_be_deleted, int col_start, int col_end)
{
  int row, col, row_num_changed, ind_changed, i;
  GArray *old_row_nums;
  if (arg->cache->row_changed->len > 0) {
    for (col = col_start; col <= col_end; col++) {
      old_row_nums = g_ptr_array_index(arg->cache->column_index, col);
      if (old_row_nums) {
        i = 0;
        ind_changed = lil_snf_argument_find_first_row_changed(row_nums_to_be_deleted, g_array_index(old_row_nums, int, i));
        if (ind_changed >= 0) {
          while (i < old_row_nums->len) {
            row = g_array_index(old_row_nums, int, i);
            row_num_changed = g_array_index(arg->cache->row_changed, int, ind_changed);
            while (row > row_num_changed) {
              ind_changed += 1;
              if (arg->cache->row_changed->len <= ind_changed) {
                ind_changed = -1;
                break;
              }
              row_num_changed = g_array_index(arg->cache->row_changed, int, ind_changed);
            }
            if (ind_changed < 0) {
              break;
            } else if (row_num_changed == row) {
              g_array_remove_index(old_row_nums, i);
            } else {
              i += 1;
            }
          }
        }
      }
    }
  }
}

static void lil_matrix_column_index_merge_column_range (LIL_SNF_ARG *arg, GArray *row_nums_to_be_deleted, GPtrArray *caches, int col_start, int col_end)
{
  int i, col;
  GPtrArray *cache_new;
  GArray *row_nums_new, *row_nums;
  lil_snf_argument_column_index_remove_changed_rows(arg, row_nums_to_be_deleted, col_start, col_end);
  for (i = 0; i < caches->len; i++) {
    cache_new = (GPtrArray *) g_ptr_array_index(caches, i);
    for (col = col_start; col <= col_end; col++) {
      row_nums_new = (GArray *) g_ptr_array_index(cache_new, col);
      if (row_nums_new && row_nums_new->len > 0) {
        row_nums = lil_matrix_column_index_cache_get_create(arg->cache->column_index, col);
        g_array_append_vals(row_nums, row_nums_new->data, row_nums_new->len);
      }
    }
  }
  for (col = col_start; col <= col_end; col++) {
    row_nums = (GArray *) g_ptr_array_index(arg->cache->column_index, col);
    if (row_nums) {
      g_array_sort(row_nums, compare_integers);
    }
  }
}

static gpointer lil_matrix_column_index_merge_column_range_thread_func (ThreadArgs *thread_args)
{
  GPtrArray *additional_data;
  additional_data = (GPtrArray *) thread_args->data;
  lil_matrix_column_index_merge_column_range((LIL_SNF_ARG *) g_ptr_array_index(additional_data, 0),
                                             (GArray *) g_ptr_array_index(additional_data, 2),
                                             (GPtrArray *) g_ptr_array_index(additional_data, 1),
                                             thread_args->number_start, thread_args->number_end);
  return 0;
}

static void lil_snf_argument_cache_row_changed_recreated_sort (LIL_SNF_ARG *arg)
{
  g_array_sort(arg->cache->row_changed, compare_integers);
  g_array_sort(arg->cache->row_recreated, compare_integers);
}

static void lil_matrix_column_index_merge_clear (LIL_SNF_ARG *arg, GPtrArray *caches)
{
  int i;
  GPtrArray *additional_data;
  GArray *row_nums_changed, *cumulative_row_changed;
  ThreadExecArgs thread_exec_args;
  lil_snf_argument_cache_row_changed_recreated_sort(arg);
  row_nums_changed = array_union(arg->cache->row_changed, arg->cache->row_recreated);
  cumulative_row_changed = array_union(lil_snf_argument_cache_take_cumulative_row_changed(arg, false), row_nums_changed);
  g_array_free(arg->cache->cumulative_row_changed, TRUE);
  arg->cache->cumulative_row_changed = cumulative_row_changed;
  additional_data = g_ptr_array_new();
  g_ptr_array_add(additional_data, (gpointer) arg);
  g_ptr_array_add(additional_data, (gpointer) caches);
  g_ptr_array_add(additional_data, (gpointer) row_nums_changed);

  lil_matrix_thread_exec_args_set(&thread_exec_args, "column index merge", THREAD_NUMBER_DEFAULT,
                                  NULL, 0, arg->cache->column_index->len - 1, additional_data,
                                  NULL, NULL, lil_matrix_column_index_merge_column_range_thread_func);
  lil_matrix_thread_exec(&thread_exec_args);
  g_array_free(arg->cache->row_changed, TRUE);
  g_array_free(arg->cache->row_recreated, TRUE);
  arg->cache->row_changed = g_array_new(false, false, sizeof(int));
  arg->cache->row_recreated = g_array_new(false, false, sizeof(int));
  for (i = 0; i < caches->len; i++) {
    lil_snf_argument_cache_column_index_clear((GPtrArray *) g_ptr_array_index(caches, i));
  }
  g_ptr_array_free(additional_data, TRUE);
  g_array_free(row_nums_changed, TRUE);
}

static LIL_MATRIX_MIN_VALUE_CACHE *lil_snf_argument_cache_row_minimal_get_without_creation (LIL_SNF_ARG *arg, int row_num)
{
  if (arg->cache) {
    return (LIL_MATRIX_MIN_VALUE_CACHE *) g_ptr_array_index(arg->cache->row_minimal, row_num);
  } else {
    return NULL;
  }
}

/* save_recreation must be false to create cache by multiple threads */
static LIL_MATRIX_MIN_VALUE_CACHE *lil_snf_argument_cache_row_minimal_get (LIL_SNF_ARG *arg, LIL_MATRIX *mat, int row_num, GSList *row_node, bool save_recreation)
{
  if (row_node && arg->cache) {
    LIL_MATRIX_MIN_VALUE_CACHE *cache;
    cache = lil_snf_argument_cache_row_minimal_get_without_creation(arg, row_num);
    if (!cache) {
      cache = lil_matrix_min_value_data_new();
      lil_matrix_minimal_non_zero_element_at_row(mat, row_node, &(cache->column), cache->value, &(cache->size));
      g_ptr_array_index(arg->cache->row_minimal, row_num) = (gpointer) cache;
      if (save_recreation) {
        lil_snf_argument_cache_row_recreated_append(arg, row_num);
      }
    }
    return cache;
  } else {
    return NULL;
  }
}

/* static bool lil_snf_argument_cache_row_changed_find (LIL_SNF_ARG *arg, int row_num) */
/* { */
/*   return array_include_p(arg->cache->row_changed, row_num); */
/* } */

static void lil_snf_argument_cache_row_recreated_append (LIL_SNF_ARG *arg, int row_num)
{
  g_array_append_val(arg->cache->row_recreated, row_num);
}

static bool lil_snf_argument_cache_row_recreated_find (LIL_SNF_ARG *arg, int row_num)
{
  return array_include_p(arg->cache->row_recreated, row_num);
}

static void lil_snf_argument_cache_row_recreated_clear (LIL_SNF_ARG *arg)
{
  if (arg->cache->type == CACHE_BOTH && arg->cache->row_recreated->len > 0) {
    g_array_free(arg->cache->row_recreated, TRUE);
    arg->cache->row_recreated = g_array_new(false, false, sizeof(int));
  }
}

bool lil_snf_argument_cache_column_index_updated_p (LIL_SNF_ARG *arg)
{
  return arg->cache->row_recreated->len == 0 && arg->cache->row_changed->len == 0;
}

/*
  We must sort arg->cache->recreated for binary search.
  Create cache of minimal absolute value if no cache exist and
  create cache of column index for row numbers of row_recreated.
  If min_abs is not NULL, we set minimal absolute value of specified row range.
*/
static void lil_matrix_update_cache_changed_rows_in_row_range (LIL_MATRIX *mat, int row_num_start, int row_num_end, LIL_SNF_ARG *arg, GPtrArray *column_index_cache_changed, mpz_t min_abs)
{
  int row_num;
  GSList *row_list_node;
  bool column_index_create_p;
  LIL_MATRIX_MIN_VALUE_CACHE *cache;
  if (min_abs) {
    mpz_set_si(min_abs, 0);
  }
  row_list_node = g_slist_nth(mat->rows, row_num_start);
  row_num = row_num_start;
  while (row_list_node) {
    column_index_create_p = false;
    cache = lil_snf_argument_cache_row_minimal_get_without_creation(arg, row_num);
    if (cache) {
      column_index_create_p = lil_snf_argument_cache_row_recreated_find(arg, row_num);
    } else {
      cache = lil_snf_argument_cache_row_minimal_get(arg, mat, row_num, row_list_node->data, false);
      column_index_create_p = (cache && cache->column >= 0 && column_index_cache_changed);
    }
    if (column_index_create_p) {
      /* Recreate column_index_cache_changed because the row has been changed */
      lil_matrix_column_index_set_row(column_index_cache_changed, mat, row_list_node->data, row_num);
    }
    if (min_abs && cache && cache->column >= 0) {
      if (mpz_cmp_ui(min_abs, 0) == 0 || mpz_cmp(min_abs, cache->value) > 0) {
        mpz_set(min_abs, cache->value);
      }
    }
    row_list_node = g_slist_next(row_list_node);
    row_num += 1;
    if (row_num > row_num_end) {
      break;
    }
  }
}

static void lil_matrix_update_cache_args_alloc (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i;
  LIL_SNF_ARG *arg;
  GPtrArray *thread_arg_ary;
  __mpz_struct *min_val, *min_val_tmp;
  arg = (LIL_SNF_ARG *) g_ptr_array_index((GPtrArray *) additional_data, 0);
  min_val = (__mpz_struct *) g_ptr_array_index((GPtrArray *) additional_data, 1);
  for (i = 0; i < thread_num; i++) {
    thread_arg_ary = (GPtrArray *) g_ptr_array_index(thread_args, i);
    /* pointer of column index cache */
    if (arg->cache && arg->cache->type == CACHE_BOTH) {
      g_ptr_array_add(thread_arg_ary, (gpointer) lil_matrix_column_index_cache_new(arg->cache->column_index->len));
    } else {
      g_ptr_array_add(thread_arg_ary, NULL);
    }
    /* pointer of minimum absolute value */
    if (min_val) {
      min_val_tmp = (__mpz_struct *) g_malloc(sizeof(__mpz_struct));
      mpz_init(min_val_tmp);
      g_ptr_array_add(thread_arg_ary, (gpointer) min_val_tmp);
    } else {
      g_ptr_array_add(thread_arg_ary, NULL);
    }
  }
}

static gpointer lil_matrix_update_cache_args_free (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i;
  GPtrArray *ret_values, *column_cache_tmp, *tmp_caches, *thread_arg_ptr;
  __mpz_struct *min_val, *min_val_tmp;
  ret_values = (GPtrArray *) additional_data;
  /*
    ret_values is additional_data
    ret_values[0] pointer of LIL_SNF_ARG
    ret_values[1] pointer of minimum absolute value
    ret_values[2] pointer of temporary array of column indices
  */
  min_val = (__mpz_struct *) g_ptr_array_index(ret_values, 1);
  if (min_val) {
    mpz_set_si(min_val, 0);
  }
  tmp_caches = (GPtrArray *) g_ptr_array_index(ret_values, 2);

  for (i = 0; i < thread_num; i++) {
    thread_arg_ptr = (GPtrArray *) g_ptr_array_index(thread_args, i);
    column_cache_tmp = (GPtrArray *) g_ptr_array_index(thread_arg_ptr, 0);
    min_val_tmp = (__mpz_struct *) g_ptr_array_index(thread_arg_ptr, 1);
    if (tmp_caches) {
      g_ptr_array_add(tmp_caches, column_cache_tmp);
    }
    if (min_val_tmp) {
      if (min_val && mpz_cmp_si(min_val_tmp, 0) > 0 && (mpz_cmp_si(min_val, 0) == 0 || mpz_cmp(min_val, min_val_tmp) > 0)) {
        mpz_set(min_val, min_val_tmp);
      }
      mpz_clear(min_val_tmp);
      g_free(min_val_tmp);
    }
  }
  return NULL;
}

static gpointer lil_matrix_update_cache_thread_func (ThreadArgs *thread_args)
{
  GPtrArray *arguments, *additional_data, *column_index_cache_tmp;
  LIL_SNF_ARG *arg;
  __mpz_struct *min_val;
  additional_data = (GPtrArray *) thread_args->data;
  arguments = (GPtrArray *) thread_args->arguments;
  arg = (LIL_SNF_ARG *) g_ptr_array_index(additional_data, 0);
  column_index_cache_tmp = (GPtrArray *) g_ptr_array_index(arguments, 0);
  min_val = (__mpz_struct *) g_ptr_array_index(arguments, 1);
  lil_matrix_update_cache_changed_rows_in_row_range(thread_args->matrix, thread_args->number_start, thread_args->number_end, arg, column_index_cache_tmp, min_val);
  return NULL;
}

void lil_matrix_update_cache_row_range (LIL_MATRIX *mat, LIL_SNF_ARG *arg, int row_num_start, int row_num_end, mpz_t min_val)
{
  ThreadExecArgs thread_exec_args;
  GPtrArray *additional_data, *tmp_caches;
  additional_data = g_ptr_array_sized_new(3);
  /*
    additional_data[0] pointer of LIL_SNF_ARG
    additional_data[1] pointer of minimum absolute value
    additional_data[2] pointer of temporary array of column indices
  */
  g_ptr_array_add(additional_data, (gpointer) arg);
  g_ptr_array_add(additional_data, (gpointer) min_val);
  tmp_caches = (lil_snf_argument_column_index_cache(arg) ? g_ptr_array_new() : NULL);
  g_ptr_array_add(additional_data, tmp_caches);

  lil_snf_argument_cache_row_changed_recreated_sort(arg);
  lil_matrix_thread_exec_args_set(&thread_exec_args, "update cache", THREAD_NUMBER_DEFAULT,
                                  mat, row_num_start, row_num_end, additional_data,
                                  lil_matrix_update_cache_args_alloc,
                                  lil_matrix_update_cache_args_free,
                                  lil_matrix_update_cache_thread_func);
  lil_matrix_thread_exec(&thread_exec_args);
  if (tmp_caches) {
    lil_matrix_column_index_merge_clear(arg, tmp_caches);
    g_ptr_array_free(tmp_caches, TRUE);
  }
  lil_snf_argument_cache_row_recreated_clear(arg);
  arg->cache->set_p = true;

  g_ptr_array_free(additional_data, TRUE);
}

void lil_matrix_update_cache (LIL_MATRIX *mat, LIL_SNF_ARG *arg, mpz_t min_val)
{
  lil_matrix_update_cache_row_range(mat, arg, 0, mat->row_size - 1, min_val);
}

static void lil_matrix_minimal_non_zero_element_args_alloc (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i;
  GPtrArray *thread_arg_ary;
  for (i = 0; i < thread_num; i++) {
    thread_arg_ary = (GPtrArray *) g_ptr_array_index(thread_args, i);
    g_ptr_array_add(thread_arg_ary, NULL); /* pivot array (returned value) */
    g_ptr_array_add(thread_arg_ary, (gpointer) g_malloc(sizeof(int))); /* number of elements at pivot row */
    g_ptr_array_add(thread_arg_ary, (gpointer) g_malloc(sizeof(int))); /* counter of empty rows */
  }
}



gpointer lil_matrix_snf_pivot_array_free (GPtrArray *pivot_array, int index_start)
{
  int i;
  LIL_MATRIX_PIVOT *pivot;
  for (i = (index_start < 0 ? 0 : index_start); i < pivot_array->len; i++) {
    pivot = g_ptr_array_index(pivot_array, i);
    if (pivot) {
      lil_matrix_pivot_free(pivot);
    }
  }
  if (index_start < 0) {
    g_ptr_array_free(pivot_array, TRUE);
    pivot_array = NULL;
  } else if (index_start >= 0 && index_start < pivot_array->len) {
    g_ptr_array_remove_range(pivot_array, index_start, pivot_array->len - index_start);
  }
  return pivot_array;
}

static int lil_matrix_snf_pivot_increase_compare_func (gconstpointer p1, gconstpointer p2)
{
  int n;
  n = (*((LIL_MATRIX_PIVOT **) p1))->increase - (*((LIL_MATRIX_PIVOT **) p2))->increase;
  if (n == 0) {
    n = (*((LIL_MATRIX_PIVOT **) p1))->row - (*((LIL_MATRIX_PIVOT **) p2))->row;
  }
  return n;
}

static int lil_matrix_snf_pivot_row_compare_func (gconstpointer p1, gconstpointer p2)
{
  return (*((LIL_MATRIX_PIVOT **) p1))->row - (*((LIL_MATRIX_PIVOT **) p2))->row;
}

static gpointer lil_matrix_minimal_non_zero_element_args_free (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i, j, *num_elements, *count_empty_rows, *ret_num_elements, *ret_count_empty_rows;
  GPtrArray *ret_values, *pivot_array, *pivot_array_thread, *thread_arg_ptr;
  LIL_MATRIX_PIVOT *pivot, *pivot_thread;
  pivot_array = NULL;
  ret_values = (GPtrArray *) additional_data;
  /*
    ret_values is additional_data
    ret_values[0] number of elements at pivot row
    ret_values[1] counter of empty rows
    ret_values[2] pointer of LIL_SNF_ARG
  */
  ret_num_elements = (int *) g_ptr_array_index(ret_values, 0);
  ret_count_empty_rows = (int *) g_ptr_array_index(ret_values, 1);
  *ret_count_empty_rows = 0;

  for (i = 0; i < thread_num; i++) {
    thread_arg_ptr = (GPtrArray *) g_ptr_array_index(thread_args, i);
    pivot_array_thread = (GPtrArray *) g_ptr_array_index(thread_arg_ptr, 0);
    num_elements = (int *) g_ptr_array_index(thread_arg_ptr, 1);
    count_empty_rows = (int *) g_ptr_array_index(thread_arg_ptr, 2);
    *ret_count_empty_rows += *count_empty_rows;
    if (pivot_array_thread) {
      if (!pivot_array) {
        pivot_array = pivot_array_thread;
        *ret_num_elements = *num_elements;
      } else {
        pivot = (LIL_MATRIX_PIVOT *) g_ptr_array_index(pivot_array, 0);
        pivot_thread = (LIL_MATRIX_PIVOT *) g_ptr_array_index(pivot_array_thread, 0);
        if (*ret_num_elements <= 2 || *num_elements <= 2) {
          if (mpz_cmp(pivot_thread->abs, pivot->abs) < 0 || (mpz_cmp(pivot_thread->abs, pivot->abs) == 0 && *num_elements < *ret_num_elements)) {
            *ret_num_elements = *num_elements;
            lil_matrix_snf_pivot_array_free(pivot_array, -1);
            pivot_array = pivot_array_thread;
          } else if (mpz_cmp(pivot_thread->abs, pivot->abs) == 0 && *num_elements == *ret_num_elements) {
            for (j = 0; j < pivot_array_thread->len; j++) {
              g_ptr_array_add(pivot_array, g_ptr_array_index(pivot_array_thread, j));
            }
            g_ptr_array_free(pivot_array_thread, TRUE);
          } else {
            lil_matrix_snf_pivot_array_free(pivot_array_thread, -1);
          }
        } else {
          for (j = 0; j < pivot_array_thread->len; j++) {
            g_ptr_array_add(pivot_array, g_ptr_array_index(pivot_array_thread, j));
          }
          g_ptr_array_free(pivot_array_thread, TRUE);
        }
      }
    }
    g_free(num_elements);
    g_free(count_empty_rows);
  }
  return pivot_array;
}

static int lil_matrix_column_abs (mpz_t val_abs, LIL_MATRIX *mat, GSList *row_node)
{
  if (mat->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el_mpz;
    el_mpz = (LIL_MATRIX_ELEMENT_MPZ *) row_node->data;
    mpz_abs(val_abs, el_mpz->value);
    return el_mpz->column;
  } else {
    LIL_MATRIX_ELEMENT *el_int;
    el_int = (LIL_MATRIX_ELEMENT *) row_node->data;
    mpz_set_si(val_abs, abs(el_int->value));
    return el_int->column;
  }
}

static int lil_matrix_pivot_element_increase_number (GArray *row_nums, GArray *nums)
{
  bool find_p;
  int i, j, increase, row1, row2;
  increase = 0;
  j = 0;
  for (i = 0; i < row_nums->len; i++) {
    find_p = false;
    row1 = g_array_index(row_nums, int, i);
    while (j < nums->len) {
      row2 = g_array_index(nums, int, j);
      if (row1 <= row2) {
        if (row1 == row2) {
          find_p = true;
          j += 1;
        }
        break;
      }
      j += 1;
    }
    if (!find_p) {
      increase += 1;
    }
  }
  return increase;
}

static int lil_matrix_pivot_increase (int col_pivot, GArray *column_nums, GPtrArray *column_index_cache)
{
  GArray *pivot_row_nums, *nums;
  int increase, i;
  increase = 0;
  pivot_row_nums = (GArray *) g_ptr_array_index(column_index_cache, col_pivot);
  for (i = 0; i < column_nums->len; i++) {
    nums = (GArray *) g_ptr_array_index(column_index_cache, g_array_index(column_nums, int, i));
    increase += lil_matrix_pivot_element_increase_number(pivot_row_nums, nums);
  }
  return increase;
}

static LIL_MATRIX_PIVOT *lil_matrix_pivot_select_from_row (LIL_MATRIX *mat, GSList *row_list_node, int row_num, LIL_MATRIX_MIN_VALUE_CACHE *cache, GPtrArray *column_index_cache, bool precise_estimate)
{
  LIL_MATRIX_PIVOT *pivot;
  GSList *row_node;
  GPtrArray *pivot_candidates;
  GArray *column_nums, *row_nums;
  int col, i, len;
  mpz_t val_abs;
  column_nums = g_array_new(false, false, sizeof(int));
  mpz_init(val_abs);
  pivot = NULL;
  pivot_candidates = g_ptr_array_new();
  row_node = (GSList *) row_list_node->data;
  while (row_node) {
    col = lil_matrix_column_abs(val_abs, mat, row_node);
    g_array_append_val(column_nums, col);
    if (mpz_cmp(cache->value, val_abs) == 0) {
      pivot = lil_matrix_pivot_new(row_num, col, cache->value);
      row_nums = g_ptr_array_index(column_index_cache, col);
      if (!row_nums) {
        fprintf(stderr, "Invalid column index: cache does not exist at the candidate of pivot (%d, %d)\n", pivot->row, pivot->column);
        abort_with_line_number();
      }
      len = row_nums->len;
      pivot->increase = (len - 1) * (cache->size - 2);
      g_ptr_array_add(pivot_candidates, pivot);
    }
    row_node = g_slist_next(row_node);
  }
  mpz_clear(val_abs);
  g_ptr_array_sort(pivot_candidates, lil_matrix_snf_pivot_increase_compare_func);
  if (precise_estimate) {
    lil_matrix_snf_pivot_array_free(pivot_candidates, PIVOT_CANDIDATE_NUM);
    for (i = 0; i < pivot_candidates->len; i++) {
      pivot = (LIL_MATRIX_PIVOT *) g_ptr_array_index(pivot_candidates, i);
      pivot->increase = lil_matrix_pivot_increase(pivot->column, column_nums, column_index_cache);
    }
    g_ptr_array_sort(pivot_candidates, lil_matrix_snf_pivot_increase_compare_func);
  }
  pivot = (LIL_MATRIX_PIVOT *) g_ptr_array_index(pivot_candidates, 0);
  lil_matrix_snf_pivot_array_free(pivot_candidates, 1);
  g_ptr_array_free(pivot_candidates, TRUE);
  g_array_free(column_nums, TRUE);
  return pivot;
}

/* We must update cache before calling this function. */
static GPtrArray *lil_matrix_minimal_non_zero_element_base (int *el_row_num, int *count_empty_rows, LIL_MATRIX *mat, int row_num_start, int row_num_end, LIL_SNF_ARG *arg, mpz_t min_abs_val, GPtrArray *column_index_cache, bool precise_estimate)
{
  GPtrArray *pivot_array;
  LIL_MATRIX_PIVOT *pivot;
  int row_num;
  GSList *row_list_node;
  LIL_MATRIX_MIN_VALUE_CACHE *cache;
  bool find_row_less_than_two = false;
  pivot_array = g_ptr_array_new();
  pivot = NULL;
  *count_empty_rows = 0;
  *el_row_num = mat->column_size;
  row_list_node = g_slist_nth(mat->rows, row_num_start);
  row_num = row_num_start;
  while (row_list_node) {
    /* cache must be updated */
    cache = lil_snf_argument_cache_row_minimal_get_without_creation(arg, row_num);
    if (!cache || cache->column < 0) {
      *count_empty_rows += 1;
    } else {
      if (mpz_cmp(min_abs_val, cache->value) == 0) {
        if (find_row_less_than_two) {
          if (cache->size <= *el_row_num) {
            pivot = lil_matrix_pivot_new(row_num, cache->column, cache->value);
            if (cache->size < *el_row_num) {
              pivot_array = lil_matrix_snf_pivot_array_free(pivot_array, 0);
              *el_row_num = cache->size;
            }
            g_ptr_array_add(pivot_array, pivot);
          }
        } else {
          if (cache->size < *el_row_num) {
            *el_row_num = cache->size;
            if (*el_row_num <= 2) {
              pivot_array = lil_matrix_snf_pivot_array_free(pivot_array, 0);
              find_row_less_than_two = true;
            }
          }
          if (find_row_less_than_two) {
            pivot = lil_matrix_pivot_new(row_num, cache->column, cache->value);
            g_ptr_array_add(pivot_array, pivot);
          } else {
            pivot = lil_matrix_pivot_select_from_row(mat, row_list_node, row_num, cache, column_index_cache, precise_estimate);
            /* pivot must not be NULL */
            g_ptr_array_add(pivot_array, pivot);
          }
        }
      }
    }
    row_list_node = g_slist_next(row_list_node);
    row_num += 1;
    if (row_num > row_num_end) {
      break;
    }
  }
  if (pivot_array->len == 0) {
    g_ptr_array_free(pivot_array, TRUE);
    pivot_array = NULL;
    *el_row_num = 0;
  }
  return pivot_array;
}

static gpointer lil_matrix_minimal_non_zero_element_thread_func (ThreadArgs *thread_args)
{
  GPtrArray *arguments, *additional_data, *pivot_array, *column_index_cache;
  LIL_SNF_ARG *arg;
  int *el_row_num, *count_empty_rows;
  __mpz_struct *min_abs_val;
  additional_data = (GPtrArray *) thread_args->data;
  arguments = (GPtrArray *) thread_args->arguments;
  arg = (LIL_SNF_ARG *) g_ptr_array_index(additional_data, 2);
  min_abs_val = (__mpz_struct *) g_ptr_array_index(additional_data, 3);
  column_index_cache = lil_snf_argument_column_index_cache(arg);

  el_row_num = (int *) g_ptr_array_index(arguments, 1); /* number of elements at pivot row */
  count_empty_rows = (int *) g_ptr_array_index(arguments, 2); /* counter of empty rows */

  pivot_array = lil_matrix_minimal_non_zero_element_base(el_row_num, count_empty_rows, thread_args->matrix, thread_args->number_start, thread_args->number_end, arg, min_abs_val, column_index_cache, arg->precise_estimate);
  g_ptr_array_index(arguments, 0) = (gpointer) pivot_array;
  return 0;
}

/* If pivot_array->len == 0 then this function return unchanged pivot_array. */
GPtrArray *lil_matrix_snf_pivot_rows_select (GPtrArray *pivot_array, LIL_SNF_ARG *arg, int num_elements)
{
  int i;
  LIL_MATRIX_PIVOT *pivot;
  GPtrArray *column_index_cache;
  column_index_cache = lil_snf_argument_column_index_cache(arg);
  if (pivot_array && pivot_array->len > 0) {
    if (column_index_cache && mpz_cmp_ui(((LIL_MATRIX_PIVOT *) g_ptr_array_index(pivot_array, 0))->abs, 1) == 0) {
      GArray *row_nums, *row_nums_new, *row_nums_tmp;
      GPtrArray *pivot_array_new;
      int sum_increase;
      sum_increase = 0;
      pivot_array_new = g_ptr_array_new();
      row_nums = g_array_new(false, false, sizeof(int));
      for (i = 0; i < pivot_array->len; i++) {
        pivot = (LIL_MATRIX_PIVOT *) g_ptr_array_index(pivot_array, i);
        row_nums_tmp = (GArray *) g_ptr_array_index(column_index_cache, pivot->column);
        if (!row_nums_tmp) {
          fprintf(stderr, "Invalid column index: cache does not exist at the pivot (%d, %d)\n", pivot->row, pivot->column);
          abort_with_line_number();
        }
        if (!array_intersect_p(row_nums, row_nums_tmp)) {
          row_nums_new = array_union(row_nums, row_nums_tmp);
          g_array_free(row_nums, TRUE);
          row_nums = row_nums_new;
          g_ptr_array_add(pivot_array_new, pivot);
          g_ptr_array_index(pivot_array, i) = NULL;
          sum_increase += pivot->increase;
          if (num_elements > 2 && pivot_array_new->len >= arg->min_pivot_number && sum_increase > arg->max_estimate_increase) {
            break;
          }
        }
      }
      g_array_free(row_nums, TRUE);
      lil_matrix_snf_pivot_array_free(pivot_array, -1);
      pivot_array = pivot_array_new;
      g_ptr_array_sort(pivot_array, lil_matrix_snf_pivot_row_compare_func);
    } else {
      pivot_array = lil_matrix_snf_pivot_array_free(pivot_array, 1);
    }
  }
  return pivot_array;
}

GPtrArray *lil_matrix_minimal_non_zero_element_candidates (int *el_row_num, int *count_empty_rows, LIL_MATRIX *mat, int row_num_start, int row_num_end, mpz_t min_abs_val, LIL_SNF_ARG *arg)
{
  ThreadExecArgs thread_exec_args;
  GPtrArray *additional_data, *pivot_array;
  additional_data = g_ptr_array_sized_new(4);
  /*
    additional_data[0] number of elements at pivot row
    additional_data[1] counter of empty rows
    additional_data[2] pointer of LIL_SNF_ARG
    additional_data[3] precise_estimate
  */
  g_ptr_array_add(additional_data, (gpointer) el_row_num);
  g_ptr_array_add(additional_data, (gpointer) count_empty_rows);
  g_ptr_array_add(additional_data, (gpointer) arg);
  g_ptr_array_add(additional_data, (gpointer) min_abs_val);

  lil_matrix_thread_exec_args_set(&thread_exec_args, "minimal_non_zero", THREAD_NUMBER_ADJUST,
                                  mat, row_num_start, row_num_end, additional_data,
                                  lil_matrix_minimal_non_zero_element_args_alloc,
                                  lil_matrix_minimal_non_zero_element_args_free,
                                  lil_matrix_minimal_non_zero_element_thread_func);
  pivot_array = (GPtrArray *) lil_matrix_thread_exec(&thread_exec_args);
  g_ptr_array_free(additional_data, TRUE);
  return pivot_array;
}

int lil_matrix_pivots_sum_increase (GPtrArray *pivot_array)
{
  int i, sum;
  sum = 0;
  if (pivot_array) {
    for (i = 0; i < pivot_array->len; i++) {
      sum += ((LIL_MATRIX_PIVOT *) g_ptr_array_index(pivot_array, i))->increase;
    }
  }
  return sum;
}

void lil_matrix_pivot_array_sort (GPtrArray *pivot_array)
{
  if (pivot_array) {
    g_ptr_array_sort(pivot_array, lil_matrix_snf_pivot_increase_compare_func);
  }
}

GPtrArray *lil_matrix_minimal_non_zero_element (int *el_row_num, int *count_empty_rows, LIL_MATRIX *mat, int row_num_start, int row_num_end, mpz_t min_abs_val, LIL_SNF_ARG *arg, int *sum_increase)
{
  GPtrArray *pivot_array;
  pivot_array = lil_matrix_minimal_non_zero_element_candidates(el_row_num, count_empty_rows, mat, row_num_start, row_num_end, min_abs_val, arg);
  lil_matrix_pivot_array_sort(pivot_array);
  pivot_array = lil_matrix_snf_pivot_rows_select(pivot_array, arg, *el_row_num);
  if (sum_increase) {
    *sum_increase = lil_matrix_pivots_sum_increase(pivot_array);
  }
  return pivot_array;
}

static bool lil_matrix_snf_reduce_pivot_element_int (LIL_MATRIX *mat, GSList *row_list_node_pivot, int value_pivot, int column_pivot, GSList *row_list_node, bool *all_zero)
{
  int factor;
  LIL_MATRIX_ELEMENT *el;
  GSList *row_node;
  row_node = (GSList *) row_list_node->data;
  el = (LIL_MATRIX_ELEMENT *) lil_matrix_find_element_in_row(mat, row_node, column_pivot);
  if (el) {
    factor = -el->value / value_pivot;
    if (*all_zero) {
      /* We may reduce the below multiplication. */
      *all_zero = ((lil_matrix_multiply(factor, value_pivot) + el->value) == 0);
    }
    lil_matrix_add_rows_for_int(mat, row_list_node, row_list_node_pivot, factor);
    return true;
  }
  return false;
}

static bool lil_matrix_snf_partially_row_range_reduce_for_int (LIL_MATRIX *mat, int row_pivot, int column_pivot, int row_number_start, int row_number_end, GArray *rows_changed, GPtrArray *column_index_cache)
{
  GSList *row_list_node, *row_list_node_pivot;
  LIL_MATRIX_ELEMENT *el;
  int value_pivot, row_num;
  bool all_zero;
  all_zero = true;
  row_list_node_pivot = g_slist_nth(mat->rows, row_pivot);
  el = (LIL_MATRIX_ELEMENT *) lil_matrix_find_element_in_row(mat, (GSList *) row_list_node_pivot->data, column_pivot);
  if (!el) {
    fprintf(stderr, "Invalid pivot (%d, %d)\n", row_pivot, column_pivot);
    abort_with_line_number();
  }
  value_pivot = el->value;
  row_list_node = g_slist_nth(mat->rows, row_number_start);
  row_num = row_number_start;
  if (column_index_cache) {
    int i, row_num_next;
    GArray *row_nums;
    row_nums = lil_matrix_column_index_cache_get(column_index_cache, column_pivot);
    if (!row_nums) {
      fprintf(stderr, "Invalid column index: cache of target pivot does not exist; column=%d\n", column_pivot);
      abort_with_line_number();
    }
    for (i = 0; i < row_nums->len; i++) {
      row_num_next = g_array_index(row_nums, int, i);
      if (row_num_next < row_number_start || row_num_next == row_pivot) {
        continue;
      } else if (row_num_next > row_number_end) {
        break;
      }
      for (; row_num < row_num_next; row_num++) {
        row_list_node = g_slist_next(row_list_node);
      }
      if (row_list_node->data) {
        if (lil_matrix_snf_reduce_pivot_element_int(mat, row_list_node_pivot, value_pivot, column_pivot, row_list_node, &all_zero)) {
          g_array_append_val(rows_changed, row_num);
        }
      }
    }
  } else {
    while (row_list_node) {
      if ((row_pivot != row_num) && row_list_node->data) {
        if (lil_matrix_snf_reduce_pivot_element_int(mat, row_list_node_pivot, value_pivot, column_pivot, row_list_node, &all_zero)) {
          g_array_append_val(rows_changed, row_num);
        }
      }
      row_num += 1;
      if (row_num > row_number_end) {
        break;
      }
      row_list_node = g_slist_next(row_list_node);
    }
  }
  return all_zero;
}

static bool lil_matrix_snf_reduce_pivot_element_mpz (LIL_MATRIX *mat, GSList *row_list_node_pivot, mpz_t value_pivot, int column_pivot, GSList *row_list_node, bool *all_zero, mpz_t tmp_factor, mpz_t tmp_r)
{
  LIL_MATRIX_ELEMENT_MPZ *el;
  GSList *row_node;
  row_node = (GSList *) row_list_node->data;
  el = (LIL_MATRIX_ELEMENT_MPZ *) lil_matrix_find_element_in_row(mat, row_node, column_pivot);
  if (el) {
    mpz_tdiv_qr(tmp_factor, tmp_r, el->value, value_pivot);
    mpz_neg(tmp_factor, tmp_factor);
    if (*all_zero) {
      *all_zero = (mpz_cmp_ui(tmp_r, 0) == 0);
    }
    lil_matrix_add_rows_for_mpz(mat, row_list_node, row_list_node_pivot, tmp_factor);
    return true;
  }
  return false;
}

static bool lil_matrix_snf_partially_row_range_reduce_for_mpz (LIL_MATRIX *mat, int row_pivot, int column_pivot, int row_number_start, int row_number_end, GArray *rows_changed, GPtrArray *column_index_cache)
{
  GSList *row_list_node, *row_list_node_pivot;
  int row_num;
  mpz_t value_pivot, factor, r;
  bool all_zero;
  all_zero = true;
  row_list_node_pivot = g_slist_nth(mat->rows, row_pivot);
  mpz_init(value_pivot);
  mpz_init(factor);
  mpz_init(r);
  if (!lil_matrix_find_get_element_in_row(value_pivot, mat, (GSList *) row_list_node_pivot->data, column_pivot)) {
    fprintf(stderr, "Invalid pivot (%d, %d)\n", row_pivot, column_pivot);
    abort_with_line_number();
  }
  row_list_node = g_slist_nth(mat->rows, row_number_start);
  row_num = row_number_start;
  if (column_index_cache) {
    int i, row_num_next;
    GArray *row_nums;
    row_nums = lil_matrix_column_index_cache_get(column_index_cache, column_pivot);
    if (!row_nums) {
      fprintf(stderr, "Invalid column index: cache of target pivot does not exist; column=%d\n", column_pivot);
      abort_with_line_number();
    }
    for (i = 0; i < row_nums->len; i++) {
      row_num_next = g_array_index(row_nums, int, i);
      if (row_num_next < row_number_start || row_num_next == row_pivot) {
        continue;
      } else if (row_num_next > row_number_end) {
        break;
      }
      for (; row_num < row_num_next; row_num++) {
        row_list_node = g_slist_next(row_list_node);
      }
      if (row_list_node->data) {
        if (lil_matrix_snf_reduce_pivot_element_mpz(mat, row_list_node_pivot, value_pivot, column_pivot, row_list_node, &all_zero, factor, r)) {
          g_array_append_val(rows_changed, row_num);
        }
      }
    }
  } else {
    while (row_list_node) {
      if (row_pivot != row_num) {
        if (row_list_node->data) {
          if (lil_matrix_snf_reduce_pivot_element_mpz(mat, row_list_node_pivot, value_pivot, column_pivot, row_list_node, &all_zero, factor, r)) {
            g_array_append_val(rows_changed, row_num);
          }
        }
      }
      row_num += 1;
      if (row_num > row_number_end) {
        break;
      }
      row_list_node = g_slist_next(row_list_node);
    }
  }
  mpz_clear(value_pivot);
  mpz_clear(factor);
  mpz_clear(r);
  return all_zero;
}

static bool lil_matrix_snf_partially_row_range_reduce (LIL_MATRIX *mat, int row_pivot, int column_pivot, int row_number_start, int row_number_end, GArray *rows_changed, GPtrArray *column_index_cache)
{
  if (mat->use_gmp_p) {
    return lil_matrix_snf_partially_row_range_reduce_for_mpz(mat, row_pivot, column_pivot, row_number_start, row_number_end, rows_changed, column_index_cache);
  } else {
    return lil_matrix_snf_partially_row_range_reduce_for_int(mat, row_pivot, column_pivot, row_number_start, row_number_end, rows_changed, column_index_cache);
  }
}

static void lil_matrix_thread_pool_exec (gpointer thread_args_ptr, gpointer thread_signal_ptr)
{
  ThreadArgs *thread_args;
  GPtrArray *thread_signal;
  ThreadSignal *sig;
  thread_args = (ThreadArgs *) thread_args_ptr;
  ((gpointer (*)(ThreadArgs *))thread_args->func)(thread_args);
  thread_signal = (GPtrArray *) thread_signal_ptr;
  sig = (ThreadSignal *) g_ptr_array_index(thread_signal, thread_args->task_number);
  sig->finished = true;
  g_mutex_lock(&sig->mutex);
  g_cond_signal(&sig->cond);
  g_mutex_unlock(&sig->mutex);
}

static ThreadPool *lil_matrix_thread_pool () {
  if (!thread_pool) {
    lil_matrix_thread_pool_init(DEFAULT_MAX_THREAD_NUM);
  }
  return thread_pool;
}

void lil_matrix_thread_pool_init (int thread_num) {
  int i;
  ThreadSignal *sig;
  if (thread_pool) {
    fprintf(stderr, "Thread pool has been already initialized.\n");
    abort_with_line_number();
  }
  thread_pool = (ThreadPool *) g_malloc(sizeof(ThreadPool));
  thread_pool->max_thread_number = thread_num;
  thread_pool->max_task_number = thread_pool->max_thread_number * 3;
  thread_pool->signal = g_ptr_array_sized_new(thread_pool->max_thread_number);
  for (i= 0; i < thread_pool->max_task_number; i++) {
    sig = (ThreadSignal *) g_malloc(sizeof(ThreadSignal));
    g_ptr_array_add(thread_pool->signal, sig);
    g_cond_init(&sig->cond);
    g_mutex_init(&sig->mutex);
  }
  thread_pool->pool = g_thread_pool_new(lil_matrix_thread_pool_exec, thread_pool->signal, thread_pool->max_thread_number, false, NULL);
}

void lil_matrix_thread_pool_free ()
{
  if (thread_pool) {
    int i;
    ThreadSignal *sig;
    for (i = 0; i < thread_pool->max_task_number; i++) {
      sig = g_ptr_array_index(thread_pool->signal, i);
      g_cond_clear(&sig->cond);
      g_mutex_clear(&sig->mutex);
      g_free(sig);
    }
    g_ptr_array_free(thread_pool->signal, TRUE);
    g_thread_pool_free(thread_pool->pool, TRUE, TRUE);
    g_free(thread_pool);
    thread_pool = NULL;
  }
}

static void lil_matrix_thread_exec_args_set_thread_num (ThreadExecArgs *thread_exec_args, int thread_num)
{
  ThreadPool *tp;
  tp = lil_matrix_thread_pool();
  thread_exec_args->thread_num = thread_num;
  if (thread_exec_args->thread_num == THREAD_NUMBER_DEFAULT) {
    thread_exec_args->thread_num = tp->max_thread_number;
  } else if (thread_exec_args->thread_num == THREAD_NUMBER_ADJUST) {
    thread_exec_args->thread_num = (thread_exec_args->num_end - thread_exec_args->num_start) / MIN_ROW_NUM;
  }
  if (thread_exec_args->thread_num > tp->max_task_number) {
    thread_exec_args->thread_num = tp->max_task_number;
  } else if (thread_exec_args->thread_num <= 1) {
    thread_exec_args->thread_num = 1;
  }
  thread_exec_args->row_num_step = (thread_exec_args->num_end - thread_exec_args->num_start + 1) / thread_exec_args->thread_num;
  if (thread_exec_args->row_num_step <= 0) {
    thread_exec_args->thread_num = 1;
    thread_exec_args->row_num_step = thread_exec_args->num_end - thread_exec_args->num_start + 1;
  }
}

static void lil_matrix_thread_exec_args_set (ThreadExecArgs *thread_exec_args,
                                             const char *thread_name, int thread_num, LIL_MATRIX *mat,
                                             int num_start, int num_end, gpointer additional_data,
                                             void (*args_alloc)(int thread_num, GPtrArray *args, gpointer additional_data),
                                             gpointer (*args_free)(int thread_num, GPtrArray *args, gpointer additional_data),
                                             gpointer (*thread_func)(ThreadArgs *thread_args))
{
  thread_exec_args->thread_name = thread_name;
  thread_exec_args->mat = mat;
  thread_exec_args->num_start = num_start;
  thread_exec_args->num_end = num_end;
  thread_exec_args->additional_data = additional_data;
  thread_exec_args->args_alloc = args_alloc;
  thread_exec_args->args_free = args_free;
  thread_exec_args->thread_func = thread_func;
  lil_matrix_thread_exec_args_set_thread_num(thread_exec_args, thread_num);
}

static GPtrArray *lil_matrix_thread_arguments_alloc (ThreadExecArgs *thread_exec_args)
{
  int i;
  GPtrArray *args;
  if (thread_exec_args->args_alloc) {
    args = g_ptr_array_sized_new(thread_exec_args->thread_num);
    for (i = 0; i < thread_exec_args->thread_num; i++) {
      g_ptr_array_add(args, g_ptr_array_new());
    }
    thread_exec_args->args_alloc(thread_exec_args->thread_num, args, thread_exec_args->additional_data);
  } else {
    args = NULL;
  }
  return args;
}

static gpointer lil_matrix_thread_arguments_free (ThreadExecArgs *thread_exec_args, GPtrArray *args)
{
  int i;
  gpointer ret_ptr;
  ret_ptr = (thread_exec_args->args_free ? thread_exec_args->args_free(thread_exec_args->thread_num, args, thread_exec_args->additional_data) : NULL);
  if (args) {
    for (i = 0; i < thread_exec_args->thread_num; i++) {
      g_ptr_array_free((GPtrArray *) g_ptr_array_index(args, i), TRUE);
    }
    g_ptr_array_free(args, TRUE);
  }
  return ret_ptr;
}

/* We must free returned value. */
static ThreadArgs *lil_matrix_thread_args_array_alloc (ThreadExecArgs *thread_exec_args, GPtrArray *args)
{
  int i;
  ThreadArgs *thread_args;
  thread_args = (ThreadArgs *) g_malloc(sizeof(ThreadArgs) * thread_exec_args->thread_num);
  for (i = 0; i < thread_exec_args->thread_num; i++) {
    thread_args[i].matrix = thread_exec_args->mat;
    thread_args[i].thread_total_number = thread_exec_args->thread_num;
    thread_args[i].thread_index_number = i;
    if (i == 0) {
      thread_args[i].number_start = thread_exec_args->num_start;
    } else {
      thread_args[i].number_start = thread_args[i - 1].number_end + 1;
    }
    thread_args[i].number_end = thread_args[i].number_start + thread_exec_args->row_num_step - 1;
    if (i == (thread_exec_args->thread_num - 1)) {
      thread_args[i].number_end = thread_exec_args->num_end;
    }
    thread_args[i].arguments = (args ? g_ptr_array_index(args, i) : NULL);
    thread_args[i].data = thread_exec_args->additional_data;
    thread_args[i].func = (gpointer) thread_exec_args->thread_func;
  }
  return thread_args;
}

/* Return number of pushed tasks */
static int lil_matrix_thread_push_tasks (ThreadExecArgs *thread_exec_args, ThreadArgs *thread_args, int pool_index_start)
{
  int i, ind_max;
  ThreadPool *tp;
  ThreadSignal *sig;
  tp = lil_matrix_thread_pool();
  ind_max = pool_index_start + thread_exec_args->thread_num;
  for (i = pool_index_start; i < ind_max; i++) {
    sig = g_ptr_array_index(tp->signal, i);
    sig->finished = false;
  }
  for (i = 0; i < thread_exec_args->thread_num; i++) {
    thread_args[i].task_number = pool_index_start + i;
    g_thread_pool_push(tp->pool, (gpointer) &thread_args[i], NULL);
  }
  return ind_max;
}

static void lil_matrix_thread_wait_tasks (ThreadExecArgs *thread_exec_args, int num_tasks)
{
  int i;
  ThreadPool *tp;
  ThreadSignal *sig;
  tp = lil_matrix_thread_pool();
  for (i = 0; i < num_tasks; i++) {
    sig = g_ptr_array_index(tp->signal, i);
    g_mutex_lock(&sig->mutex);
    while (!sig->finished) {
      g_cond_wait(&sig->cond, &sig->mutex);
    }
    g_mutex_unlock(&sig->mutex);
  }
}

/*
  Function args_alloc should return GPtrArray of which size is thread_num.
  Function args_free free memory of an array allocated by args_alloc
  and returned value is that of lil_matrix_thread_exec.
*/
static gpointer lil_matrix_thread_exec (ThreadExecArgs *thread_exec_args)
{
  int num_tasks;
  GPtrArray *args;
  ThreadArgs *thread_args;
  args = lil_matrix_thread_arguments_alloc(thread_exec_args);
  thread_args = lil_matrix_thread_args_array_alloc(thread_exec_args, args);
  if (thread_exec_args->thread_num > 1) {
    num_tasks = lil_matrix_thread_push_tasks(thread_exec_args, thread_args, 0);
    lil_matrix_thread_wait_tasks(thread_exec_args, num_tasks);
  } else {
    thread_exec_args->thread_func(&thread_args[0]);
  }
  g_free(thread_args);
  return lil_matrix_thread_arguments_free(thread_exec_args, args);
}

static void lil_matrix_thread_args_adjust_task_number (GPtrArray *thread_exec_args_array)
{
  int i, sum_task_num, sub, thread_num_new;
  ThreadExecArgs *args;
  ThreadPool *tp;
  tp = lil_matrix_thread_pool();
  while (true) {
    sum_task_num = 0;
    for (i = 0; i < thread_exec_args_array->len; i++) {
      args = (ThreadExecArgs *) g_ptr_array_index(thread_exec_args_array, i);
      sum_task_num += args->thread_num;
    }
    if (sum_task_num > tp->max_task_number) {
      sub = (sum_task_num - tp->max_task_number) / thread_exec_args_array->len;
      if (sub <= 0) {
        sub = 1;
      }
      for (i = 0; i < thread_exec_args_array->len; i++) {
        args = (ThreadExecArgs *) g_ptr_array_index(thread_exec_args_array, i);
        thread_num_new = args->thread_num - sub;
        if (thread_num_new <= 0) {
          thread_num_new = 1;
        }
        lil_matrix_thread_exec_args_set_thread_num(args, thread_num_new);
      }
    } else {
      break;
    }
  }
}

static GPtrArray *lil_matrix_thread_exec_in_parallel (GPtrArray *thread_exec_args_array)
{
  int num_tasks, i;
  GPtrArray *args, *args_array, *ret;
  ThreadExecArgs *thread_exec_args;
  ThreadArgs *thread_args;
  lil_matrix_thread_args_adjust_task_number(thread_exec_args_array);
  args_array = g_ptr_array_sized_new(thread_exec_args_array->len * 2);
  ret = g_ptr_array_sized_new(thread_exec_args_array->len);
  num_tasks = 0;
  for (i = 0; i < thread_exec_args_array->len; i++) {
    thread_exec_args = (ThreadExecArgs *) g_ptr_array_index(thread_exec_args_array, i);
    args = lil_matrix_thread_arguments_alloc(thread_exec_args);
    thread_args = lil_matrix_thread_args_array_alloc(thread_exec_args, args);
    g_ptr_array_add(args_array, args);
    g_ptr_array_add(args_array, thread_args);
    num_tasks = lil_matrix_thread_push_tasks(thread_exec_args, thread_args, num_tasks);
  }
  lil_matrix_thread_wait_tasks(thread_exec_args, num_tasks);
  for (i = 0; i < thread_exec_args_array->len; i++) {
    thread_exec_args = (ThreadExecArgs *) g_ptr_array_index(thread_exec_args_array, i);
    args = (GPtrArray *) g_ptr_array_index(args_array, i * 2);
    thread_args = (ThreadArgs *) g_ptr_array_index(args_array, i * 2 + 1);
    g_ptr_array_add(ret, lil_matrix_thread_arguments_free(thread_exec_args, args));
    g_free(thread_args);
  }
  g_ptr_array_free(args_array, TRUE);
  return ret;
}

/* additional_data is GPtrArray: first element is pointer of LIL_SNF_ARG and pointer of column_pivot. */
static void lil_matrix_snf_partially_row_reduce_args_alloc (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i;
  GPtrArray *thread_arg_ptr;
  for (i = 0; i < thread_num; i++) {
    thread_arg_ptr = (GPtrArray *) g_ptr_array_index(thread_args, i);
    g_ptr_array_add(thread_arg_ptr, (gpointer) g_malloc(sizeof(bool)));
    g_ptr_array_add(thread_arg_ptr, (gpointer) g_array_new(false, false, sizeof(int)));
  }
}

static gpointer lil_matrix_snf_partially_row_reduce_args_free (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i;
  bool all_zero, *all_zero_of_thread;
  GArray *rows_changed;
  GPtrArray *thread_arg_ptr;
  LIL_SNF_ARG *arg;
  arg = (LIL_SNF_ARG *) g_ptr_array_index((GPtrArray *) additional_data, 0);
  all_zero = true;
  for (i = 0; i < thread_num; i++) {
    thread_arg_ptr = (GPtrArray *) g_ptr_array_index(thread_args, i);
    all_zero_of_thread = (bool *) g_ptr_array_index(thread_arg_ptr, 0);
    rows_changed = (GArray *) g_ptr_array_index(thread_arg_ptr, 1);
    if (!(*all_zero_of_thread)) {
      all_zero = false;
    }
    arg->stat->row_operations += rows_changed->len;
    if (rows_changed->len > 0) {
      g_array_append_vals(arg->cache->row_changed, rows_changed->data, rows_changed->len);
    }
    g_free(all_zero_of_thread);
    g_array_free(rows_changed, TRUE);
  }
  return (all_zero ? g_free : NULL); /* g_free does not mean function, but just true. */
}

/* If returned value is not NULL, the array has more than one items. */
static GArray *lil_matrix_rows_including (LIL_MATRIX *mat, GSList *row_node, int row_num, GPtrArray *column_index_cache)
{
  int i, col_ind, r;
  GArray *rows_including, *rows_including_new, *row_nums;
  GSList *el_node;
  rows_including = NULL;
  if (row_node) {
    el_node = g_slist_nth(row_node, 0);
    while (el_node) {
      col_ind = lil_matrix_element_column_number(mat, el_node->data);
      row_nums = g_ptr_array_index(column_index_cache, col_ind);
      if (!row_nums) {
        fprintf(stderr, "Invalid column_index_cache that must include col_ind=%d\n", col_ind);
      } else if (row_nums->len <= 1) {
        if (rows_including) {
          g_array_free(rows_including, TRUE);
          rows_including = NULL;
        }
        break;
      }
      if (!rows_including) {
        rows_including = g_array_sized_new(FALSE, FALSE, sizeof(int), row_nums->len - 1);
        for (i = 0; i < row_nums->len; i++) {
          r = g_array_index(row_nums, int, i);
          if (r != row_num) {
            g_array_append_val(rows_including, r);
          }
        }
      } else {
        rows_including_new = array_intersection(rows_including, row_nums);
        g_array_free(rows_including, TRUE);
        rows_including = rows_including_new;
        rows_including_new = NULL;
        if (!rows_including) {
          break;
        }
      }
      el_node = g_slist_next(el_node);
    }
  }
  return rows_including;
}

static void lil_matrix_snf_create_inclusion_index (LIL_MATRIX *mat, LIL_SNF_ARG *arg, int row_num_start, int row_num_end, GPtrArray *inclusion_index, GArray *target_rows)
{
  GPtrArray *column_index_cache;
  GArray *row_nums, *row_nums_new;
  int row_num;
  GSList *row_list_node, *row_node;
  column_index_cache = lil_snf_argument_column_index_cache(arg);

  row_num = row_num_start;
  row_list_node = g_slist_nth(mat->rows, row_num);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    row_nums = lil_matrix_rows_including(mat, row_node, row_num, column_index_cache);
    /* row_nums must be NULL for empty rows */
    if (row_nums) {
      if (target_rows && !array_include_p(target_rows, row_num)) {
        int i, row_num_tmp;
        GArray *row_nums2;
        row_nums2 = g_array_sized_new(false, false, sizeof(int), row_nums->len);
        for (i = 0; i < row_nums->len; i++) {
          row_num_tmp = g_array_index(row_nums, int, i);
          if (array_include_p(target_rows, row_num_tmp)) {
            g_array_append_val(row_nums2, row_num_tmp);
          }
        }
        g_array_free(row_nums, TRUE);
        row_nums = row_nums2;
      }
      if (row_nums->len > 0) {
        row_nums_new = g_array_sized_new(false, false, sizeof(int), row_nums->len + 1);
        g_array_append_val(row_nums_new, row_num);
        g_array_append_vals(row_nums_new, row_nums->data, row_nums->len);
        g_ptr_array_add(inclusion_index, row_nums_new);
      }
      g_array_free(row_nums, TRUE);
      row_nums = NULL;
      row_nums_new = NULL;
    }
    row_list_node = g_slist_next(row_list_node);
    row_num += 1;
    if (row_num > row_num_end) {
      break;
    }
  }
}

static void lil_matrix_snf_create_inclusion_index_multithread_args_alloc (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i;
  for (i = 0; i < thread_num; i++) {
    g_ptr_array_add((GPtrArray *) g_ptr_array_index(thread_args, i), (gpointer) g_ptr_array_new()); /* inclusion_index_tmp */
  }
}

static gpointer lil_matrix_snf_create_inclusion_index_multithread_args_free (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i, j;
  GPtrArray *inclusion_index, *inclusion_index_tmp;
  inclusion_index = g_ptr_array_index((GPtrArray *) additional_data, 0);
  for (i = 0; i < thread_num; i++) {
    inclusion_index_tmp = (GPtrArray *) g_ptr_array_index((GPtrArray *) g_ptr_array_index(thread_args, i), 0);
    for (j = 0; j < inclusion_index_tmp->len; j++) {
      g_ptr_array_add(inclusion_index, g_ptr_array_index(inclusion_index_tmp, j));
    }
    g_ptr_array_free(inclusion_index_tmp, TRUE);
  }
  return NULL;
}

static gpointer lil_matrix_snf_create_inclusion_index_multithread_thread_exec (ThreadArgs *thread_args)
{
  GPtrArray *inclusion_index_tmp, *additional_data;
  GArray *target_rows;
  LIL_SNF_ARG *arg;
  additional_data = (GPtrArray *) thread_args->data;
  inclusion_index_tmp = (GPtrArray *) g_ptr_array_index((GPtrArray *) thread_args->arguments, 0);
  arg = (LIL_SNF_ARG *) g_ptr_array_index(additional_data, 1);
  target_rows = (GArray *) g_ptr_array_index(additional_data, 2);
  lil_matrix_snf_create_inclusion_index(thread_args->matrix, arg, thread_args->number_start, thread_args->number_end, inclusion_index_tmp, target_rows);
  return NULL;
}

void lil_matrix_snf_create_inclusion_index_multithread_for_range (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *inclusion_index, GArray *target_rows, int row_num_start, int row_num_end)
{
  ThreadExecArgs thread_exec_args;
  GPtrArray *additional_data;
  additional_data = g_ptr_array_new();
  g_ptr_array_add(additional_data, (gpointer) inclusion_index);
  g_ptr_array_add(additional_data, (gpointer) arg);
  g_ptr_array_add(additional_data, (gpointer) target_rows);
  lil_matrix_thread_exec_args_set(&thread_exec_args, "Create inclusion index", THREAD_NUMBER_ADJUST,
                                  mat, row_num_start, row_num_end, additional_data,
                                  lil_matrix_snf_create_inclusion_index_multithread_args_alloc,
                                  lil_matrix_snf_create_inclusion_index_multithread_args_free,
                                  lil_matrix_snf_create_inclusion_index_multithread_thread_exec);
  lil_matrix_thread_exec(&thread_exec_args);
  g_ptr_array_free(additional_data, TRUE);
}

static void lil_matrix_snf_create_inclusion_index_multithread (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *inclusion_index, GArray *target_rows)
{
  lil_matrix_snf_create_inclusion_index_multithread_for_range(mat, arg, inclusion_index, target_rows, 0, mat->row_size -1);
}

static gpointer lil_matrix_snf_partially_row_reduce_thread_func (ThreadArgs *thread_args)
{
  GPtrArray *arguments, *additional_data, *pivot_array;
  GArray *rows_changed;
  int i;
  bool *all_zero, ret;
  LIL_SNF_ARG *arg;
  LIL_MATRIX_PIVOT *pivot;
  additional_data = (GPtrArray *) thread_args->data;
  arg = (LIL_SNF_ARG *) g_ptr_array_index(additional_data, 0);
  pivot_array = (GPtrArray *) g_ptr_array_index(additional_data, 1);

  arguments = (GPtrArray *) thread_args->arguments;
  all_zero = (bool *) g_ptr_array_index(arguments, 0);
  rows_changed = (GArray *) g_ptr_array_index(arguments, 1);

  *all_zero = true;
  for (i = 0; i < pivot_array->len; i++) {
    pivot = (LIL_MATRIX_PIVOT *) g_ptr_array_index(pivot_array, i);
    ret = lil_matrix_snf_partially_row_range_reduce(thread_args->matrix, pivot->row, pivot->column, thread_args->number_start, thread_args->number_end, rows_changed, lil_snf_argument_column_index_cache(arg));
    if (!ret) {
      *all_zero = false;
    }
  }
  for (i = 0; i < rows_changed->len; i++) {
    lil_snf_argument_remove_row_cache_without_save_changed(arg, g_array_index(rows_changed, int, i));
  }
  g_array_sort(rows_changed, compare_integers);
  return 0;
}

/* If all elements of other rows at the specified column are zero, we get true. Otherwise, false. */
bool lil_matrix_snf_partially_row_reduce (LIL_MATRIX *mat, GPtrArray *pivot_array, LIL_SNF_ARG *arg, int row_start_num, int row_end_num)
{
  ThreadExecArgs thread_exec_args;
  GPtrArray *additional_data;
  bool all_zero;
  additional_data = g_ptr_array_sized_new(2);
  g_ptr_array_index(additional_data, 0) = (gpointer) arg;
  g_ptr_array_index(additional_data, 1) = (gpointer) pivot_array;

  lil_matrix_thread_exec_args_set(&thread_exec_args, "row_reduce", THREAD_NUMBER_ADJUST,
                                  mat, row_start_num, row_end_num, additional_data,
                                  lil_matrix_snf_partially_row_reduce_args_alloc,
                                  lil_matrix_snf_partially_row_reduce_args_free,
                                  lil_matrix_snf_partially_row_reduce_thread_func);
  if (lil_matrix_thread_exec(&thread_exec_args) != NULL) {
    all_zero = true;
  } else {
    all_zero = false;
  }
  g_ptr_array_free(additional_data, TRUE);
  return all_zero;
}

static bool lil_matrix_snf_partially_column_reduce_after_row_reduction_for_int (LIL_MATRIX *mat, int row_pivot, int column_pivot, LIL_MATRIX *column_ops)
{
  int value_pivot, factor;
  bool all_zero;
  GSList *row_list_node, *row_node, *el_node;
  LIL_MATRIX_ELEMENT *el;
  all_zero = true;
  row_list_node = g_slist_nth(mat->rows, row_pivot);
  value_pivot = lil_matrix_element(mat, row_pivot, column_pivot);
  row_node = (GSList *) row_list_node->data;
  el_node = g_slist_nth(row_node, 0);
  while (el_node) {
    el = (LIL_MATRIX_ELEMENT *) el_node->data;
    if (el->column != column_pivot) {
      factor = -el->value / value_pivot;
      if (column_ops) {
        lil_matrix_add_rows(column_ops, el->column, column_pivot, factor);
      }
      el->value = el->value + value_pivot * factor;
      if (el->value == 0) {
        row_node = lil_matrix_next_element_node_with_link_deletion(mat, row_node, &el_node);
      } else {
        all_zero = false;
        el_node = g_slist_next(el_node);
      }
    } else {
      el_node = g_slist_next(el_node);
    }
  }
  row_list_node->data = row_node;
  return all_zero;
}

static bool lil_matrix_snf_partially_column_reduce_after_row_reduction_for_mpz (LIL_MATRIX *mat, int row_pivot, int column_pivot, LIL_MATRIX *column_ops)
{
  mpz_t value_pivot, factor;
  bool all_zero;
  GSList *row_list_node, *row_node, *el_node;
  LIL_MATRIX_ELEMENT_MPZ *el;
  all_zero = true;
  mpz_init(value_pivot);
  mpz_init(factor);
  row_list_node = g_slist_nth(mat->rows, row_pivot);
  lil_matrix_element_mpz(value_pivot, mat, row_pivot, column_pivot);
  row_node = (GSList *) row_list_node->data;
  el_node = g_slist_nth(row_node, 0);
  while (el_node) {
    el = (LIL_MATRIX_ELEMENT_MPZ *) el_node->data;
    if (el->column != column_pivot) {
      mpz_tdiv_q(factor, el->value, value_pivot);
      mpz_neg(factor, factor);
      if (column_ops) {
        lil_matrix_add_rows_mpz(column_ops, el->column, column_pivot, factor);
      }
      mpz_mul(factor, factor, value_pivot);
      mpz_add(el->value, el->value, factor);
      if (mpz_cmp_ui(el->value, 0) == 0) {
        row_node = lil_matrix_next_element_node_with_link_deletion(mat, row_node, &el_node);
      } else {
        all_zero = false;
        el_node = g_slist_next(el_node);
      }
    } else {
      el_node = g_slist_next(el_node);
    }
  }
  row_list_node->data = row_node;
  mpz_clear(value_pivot);
  mpz_clear(factor);
  return all_zero;
}

/*
  Value at pivot must have non zero.
  The pointer of GSList at rows of pivots is changed.
*/
bool lil_matrix_snf_partially_column_reduce_after_row_reduction (LIL_MATRIX *mat, GPtrArray *pivot_array, LIL_MATRIX *column_ops)
{
  int i;
  bool ret, ret2;
  LIL_MATRIX_PIVOT *pivot;
  ret = true;
  for (i = 0; i < pivot_array->len; i++) {
    pivot = g_ptr_array_index(pivot_array, i);
    if (mat->use_gmp_p) {
      ret2 = lil_matrix_snf_partially_column_reduce_after_row_reduction_for_mpz(mat, pivot->row, pivot->column, column_ops);
    } else {
      ret2 = lil_matrix_snf_partially_column_reduce_after_row_reduction_for_int(mat, pivot->row, pivot->column, column_ops);
    }
    if (!ret2) {
      ret = false;
    }
  }
  if (!ret && pivot_array->len > 1) {
    fprintf(stderr, "All values of pivots are zero when pivot_array has multiple elements\n");
    abort_with_line_number();
  }
  return ret;
}

static bool lil_matrix_snf_divisible_for_int (LIL_MATRIX *mat, bool *divisible, int *row, int *col, mpz_t q, int val)
{
  GSList *row_list_node, *row_node, *el_node;
  LIL_MATRIX_ELEMENT *el;
  row_list_node = g_slist_nth(mat->rows, 0);
  *row = 0;
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      el_node = g_slist_nth(row_node, 0);
      while (el_node) {
        el = (LIL_MATRIX_ELEMENT *) el_node->data;
        if ((el->value % val) != 0) {
          *col = el->column;
          mpz_set_si(q, el->value / val);
          *divisible = false;
          return true;
        }
        el_node = g_slist_next(el_node);
      }
    }
    row_list_node = g_slist_next(row_list_node);
    *row += 1;
  }
  return true;
}

static bool lil_matrix_snf_divisible_for_mpz (LIL_MATRIX *mat, bool *divisible, int *row, int *col, mpz_t q, mpz_t val)
{
  GSList *row_list_node, *row_node, *el_node;
  LIL_MATRIX_ELEMENT_MPZ *el;
  row_list_node = g_slist_nth(mat->rows, 0);
  *row = 0;
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      el_node = g_slist_nth(row_node, 0);
      while (el_node) {
        el = (LIL_MATRIX_ELEMENT_MPZ *) el_node->data;
        if (!mpz_divisible_p(el->value, val)) {
          *col = el->column;
          mpz_tdiv_q(q, el->value, val);
          *divisible = false;
          return true;
        }
        el_node = g_slist_next(el_node);
      }
    }
    row_list_node = g_slist_next(row_list_node);
    *row += 1;
  }
  return true;
}

/*
 * (row_pivot, column_pivot) element must be not zero.
 * *divisible always is set by this function.
 */
bool lil_matrix_snf_divisible_p (LIL_MATRIX *mat, bool *divisible, int *row, int *col, mpz_t q, int row_pivot, int column_pivot)
{
  bool ret;
  mpz_t val;
  *divisible = true;
  mpz_init(val);
  lil_matrix_element_mpz(val, mat, row_pivot, column_pivot);
  mpz_abs(val, val);
  if (mpz_cmp_ui(val, 0) == 0) {
    ret = false;
  } else if (mpz_cmp_ui(val, 1) == 0) {
    ret = true;
  } else {
    if (mat->use_gmp_p) {
      ret = lil_matrix_snf_divisible_for_mpz(mat, divisible, row, col, q, val);
    } else {
      ret = lil_matrix_snf_divisible_for_int(mat, divisible, row, col, q, mpz_get_si(val));
    }
  }
  mpz_clear(val);
  return ret;
}

static void lil_matrix_snf_delete_empty_rows_and_cache (LIL_MATRIX *mat, LIL_SNF_ARG *arg)
{
  int row_num, ind, cache_row_num, deleted_num;
  GArray *cumulative_row_changed;
  GSList *row_list_node, *row_list_node_next;
  lil_snf_argument_cache_reset(arg, CACHE_NOT_CHANGED, mat);
  cumulative_row_changed = lil_snf_argument_cache_take_cumulative_row_changed(arg, false);
  row_num = 0;
  ind = 0;
  deleted_num = 0;
  row_list_node = g_slist_nth(mat->rows, row_num);
  while (row_list_node) {
    if (!row_list_node->data || g_slist_length((GSList *) row_list_node->data) == 0) {
      row_list_node_next = g_slist_next(row_list_node);
      lil_matrix_row_list_node_delete(mat, row_list_node);
      row_list_node = row_list_node_next;
      if (cumulative_row_changed->len > ind) {
        cache_row_num = g_array_index(cumulative_row_changed, int, ind);
        while (cache_row_num < row_num) {
          g_array_index(cumulative_row_changed, int, ind) = cache_row_num - deleted_num;
          ind += 1;
          if (cumulative_row_changed->len <= ind) {
            break;
          }
          cache_row_num = g_array_index(cumulative_row_changed, int, ind);
        }
        if (cache_row_num == row_num) {
          g_array_remove_index(cumulative_row_changed, ind);
        }
      }
      deleted_num += 1;
    } else {
      row_list_node = g_slist_next(row_list_node);
    }
    row_num += 1;
  }
  while (ind < cumulative_row_changed->len) {
    cache_row_num = g_array_index(cumulative_row_changed, int, ind);
    g_array_index(cumulative_row_changed, int, ind) = cache_row_num - deleted_num;
    ind += 1;
  }
}

static void lil_matrix_row_element_first_positive (LIL_MATRIX *mat)
{
  GSList *row_list_node, *row_node, *el_node;
  row_list_node = g_slist_nth(mat->rows, 0);
  if (mat->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el;
    while (row_list_node) {
      row_node = (GSList *) row_list_node->data;
      if (row_node) {
        el_node = g_slist_nth(row_node, 0);
        el = (LIL_MATRIX_ELEMENT_MPZ *) el_node->data;
        if (mpz_cmp_si(el->value, 0) < 0) {
          while (el_node) {
            el = (LIL_MATRIX_ELEMENT_MPZ *) el_node->data;
            mpz_neg(el->value, el->value);
            el_node = g_slist_next(el_node);
          }
        }
      }
      row_list_node = g_slist_next(row_list_node);
    }
  } else {
    LIL_MATRIX_ELEMENT *el;
    while (row_list_node) {
      row_node = (GSList *) row_list_node->data;
      if (row_node) {
        el_node = g_slist_nth(row_node, 0);
        el = (LIL_MATRIX_ELEMENT *) el_node->data;
        if (el->value < 0) {
          while (el_node) {
            el = (LIL_MATRIX_ELEMENT *) el_node->data;
            el->value = -el->value;
            el_node = g_slist_next(el_node);
          }
        }
      }
      row_list_node = g_slist_next(row_list_node);
    }
  }
}

/* Return true if row_list_node1 includes row_list_node2. */
static bool lil_matrix_snf_test_inclusion (LIL_MATRIX *mat, GSList *row_list_node1, GSList *row_list_node2)
{
  bool find_p;
  GSList *row_node1, *row_node2;
  row_node1 = (GSList *) row_list_node1->data;
  row_node2 = (GSList *) row_list_node2->data;
  if (mat->use_gmp_p) {
    LIL_MATRIX_ELEMENT_MPZ *el1, *el2;
    while (row_node2) {
      el2 = (LIL_MATRIX_ELEMENT_MPZ *) row_node2->data;
      find_p = false;
      while (row_node1) {
        el1 = (LIL_MATRIX_ELEMENT_MPZ *) row_node1->data;
        if (el1->column >= el2->column) {
          if (el1->column == el2->column) {
            find_p = true;
          }
          break;
        }
        row_node1 = g_slist_next(row_node1);
      }
      if (!find_p) {
        return false;
      }
      row_node2 = g_slist_next(row_node2);
    }
  } else {
    LIL_MATRIX_ELEMENT *el1, *el2;
    while (row_node2) {
      el2 = (LIL_MATRIX_ELEMENT *) row_node2->data;
      while (row_node1) {
        el1 = (LIL_MATRIX_ELEMENT *) row_node1->data;
        if (el1->column >= el2->column) {
          if (el1->column == el2->column) {
            find_p = true;
          }
          break;
        }
        row_node1 = g_slist_next(row_node1);
      }
      if (!find_p) {
        return false;
      }
      row_node2 = g_slist_next(row_node2);
    }
  }
  return true;
}

static bool lil_matrix_snf_reduce_elements_of_row (LIL_MATRIX *mat, GSList *row_list_node_pivot, mpz_t value_pivot, int column_pivot, GSList *row_list_node_other, bool test_inclusion)
{
  bool ret;
  gpointer el_ptr;
  if (test_inclusion && !lil_matrix_snf_test_inclusion(mat, row_list_node_other, row_list_node_pivot)) {
    return false;
  }
  ret = false;
  el_ptr = lil_matrix_find_element_in_row(mat, row_list_node_other->data, column_pivot);
  if (el_ptr) {
    mpz_t el_val;
    mpz_init(el_val);
    lil_matrix_element_value(el_val, mat, el_ptr);
    if (mpz_cmp_si(el_val, 0) != 0) {
      mpz_t factor, r;
      mpz_init(factor);
      mpz_init(r);
      mpz_tdiv_qr(factor, r, el_val, value_pivot);
      if (mpz_cmp_si(factor, 0) != 0) {
        mpz_neg(factor, factor);
        lil_matrix_add_row_nodes_mpz(mat, row_list_node_other, row_list_node_pivot, factor);
        ret = true;
      }
      mpz_clear(factor);
      mpz_clear(r);
    }
    mpz_clear(el_val);
  }
  return ret;
}

/* Convert { pivot row, row including pivot, ... } to { row, pivot, ... } */
void lil_matrix_snf_split_inclusion_index (LIL_MATRIX *mat, GPtrArray *inclusion_index, GArray **out_pivot_row_nums, GPtrArray **out_pivot_splitted, GPtrArray **out_row_nums_splitted)
{
  int i, j;
  GArray *row_nums, *pivot_row_nums, *nums;
  GPtrArray *row_nums_splitted, *pivot_splitted, *splitted;
  int num, row_num;
  pivot_row_nums = g_array_sized_new(false, false, sizeof(int), inclusion_index->len);
  for (i = 0; i < inclusion_index->len; i++) {
    row_nums = g_ptr_array_index(inclusion_index, i);
    row_num = g_array_index(row_nums, int, 0);
    g_array_append_val(pivot_row_nums, row_num);
  }
  splitted = g_ptr_array_sized_new(mat->row_size);
  for (i = 0; i < mat->row_size; i++) {
    g_ptr_array_add(splitted, NULL);
  }
  for (i = 0; i < inclusion_index->len; i++) {
    /* row_nums: the first element is pivot row number and the others are row numbers including the pivot */
    row_nums = g_ptr_array_index(inclusion_index, i);
    row_num = g_array_index(row_nums, int, 0);
    for (j = 1; j < row_nums->len; j++) {
      num = g_array_index(row_nums, int, j);
      nums = (GArray*) g_ptr_array_index(splitted, num);
      if (!nums) {
        nums = g_array_new(false, false, sizeof(int));
        g_ptr_array_index(splitted, num) = nums;
      }
      g_array_append_val(nums, row_num);
    }
  }
  row_nums_splitted = g_ptr_array_new();
  pivot_splitted = g_ptr_array_new();
  for (num = 0; num < splitted->len; num++) {
    nums = (GArray*) g_ptr_array_index(splitted, num);
    if (nums) {
      g_array_sort(nums, compare_integers);
      g_array_prepend_val(nums, num);
      if (array_include_p(pivot_row_nums, num)) {
        g_ptr_array_add(pivot_splitted, nums);
      } else {
        g_ptr_array_add(row_nums_splitted, nums);
      }
    }
  }
  g_ptr_array_free(splitted, TRUE);
  *out_pivot_row_nums = pivot_row_nums;
  *out_row_nums_splitted = row_nums_splitted;
  *out_pivot_splitted = pivot_splitted;
}

/* If row_changed is not NULL, this function is executed in multithread. */
void lil_matrix_snf_reduction_by_inclusion_at_each_row (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *row_nums_target, int row_num_start, int row_num_end, GArray *row_changed, bool test_inclusion)
{
  int i, j, k, step_next;
  GArray *row_nums;
  int row_num, row_num_last, row_num_pivot;
  GSList *row_list_node, *row_list_node_pivot;
  LIL_MATRIX_MIN_VALUE_CACHE *row_cache;
  gpointer el_ptr;
  mpz_t value_pivot;
  bool row_changed_p;
  mpz_init(value_pivot);
  row_list_node = NULL;
  for (i = 0; i < row_nums_target->len; i++) {
    row_nums = g_ptr_array_index(row_nums_target, i);
    row_num = g_array_index(row_nums, int, 0);
    row_changed_p = false;
    if (row_num < row_num_start) {
      continue;
    } else if (row_num > row_num_end) {
      break;
    }
    if (row_list_node) {
      step_next = row_num - row_num_last;
      if (step_next <= 0) {
        fprintf(stderr, "Invalid row_nums_target\n");
        abort_with_line_number();
      }
      for (j = 0; j < step_next; j++) {
        row_list_node = g_slist_next(row_list_node);
      }
    } else {
      row_list_node = g_slist_nth(mat->rows, row_num);
    }
    row_list_node_pivot = NULL;
    for (j = 1; j < row_nums->len; j++) {
      if (!row_list_node->data) {
        break;
      }
      row_num_pivot = g_array_index(row_nums, int, j);
      if (row_list_node_pivot) {
        step_next = row_num_pivot - g_array_index(row_nums, int, j - 1);
        for (k = 0; k < step_next; k++) {
          row_list_node_pivot = g_slist_next(row_list_node_pivot);
        }
      } else {
        row_list_node_pivot = g_slist_nth(mat->rows, row_num_pivot);
      }
      if (row_list_node_pivot && row_list_node_pivot->data) {
        row_cache = lil_snf_argument_cache_row_minimal_get(arg, mat, row_num_pivot, row_list_node_pivot->data, (row_changed ? false : true));
        if (row_cache) {
          el_ptr = lil_matrix_find_element_in_row(mat, row_list_node_pivot->data, row_cache->column);
          if (el_ptr) {
            lil_matrix_element_value(value_pivot, mat, el_ptr);
            if (lil_matrix_snf_reduce_elements_of_row(mat, row_list_node_pivot, value_pivot, row_cache->column, row_list_node, test_inclusion)) {
              if (row_changed) {
                if (!row_changed_p) {
                  row_changed_p = true;
                  g_array_append_val(row_changed, row_num);
                  lil_snf_argument_remove_row_cache_without_save_changed(arg, row_num);
                }
              } else {
                arg->stat->row_operations += 1;
                lil_snf_argument_remove_row_cache(arg, row_num);
              }
            }
          }
        }
      }
    }
    row_num_last = row_num;
  }
  mpz_clear(value_pivot);
}

static void lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread_args_alloc (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i;
  for (i = 0; i < thread_num; i++) {
    g_ptr_array_add((GPtrArray *) g_ptr_array_index(thread_args, i), (gpointer) g_array_new(false, false, sizeof(int))); /* rows_changed */
  }
}

static gpointer lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread_args_free (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i;
  GArray *rows_changed;
  LIL_SNF_ARG *arg;
  arg = (LIL_SNF_ARG *) g_ptr_array_index((GPtrArray *) additional_data, 0);
  for (i = 0; i < thread_num; i++) {
    rows_changed = (GArray *) g_ptr_array_index((GPtrArray *) g_ptr_array_index(thread_args, i), 0);
    arg->stat->row_operations += rows_changed->len;
    if (rows_changed->len > 0) {
      g_array_append_vals(arg->cache->row_changed, rows_changed->data, rows_changed->len);
    }
    g_array_free(rows_changed, TRUE);
  }
  return NULL;
}

static gpointer lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread_thread_func (ThreadArgs *thread_args)
{
  GPtrArray *row_nums_target, *additional_data, *arguments;
  GArray *row_changed;
  LIL_SNF_ARG *arg;
  additional_data = thread_args->data;
  arguments = (GPtrArray *) thread_args->arguments;
  row_changed = (GArray *) g_ptr_array_index(arguments, 0);
  arg = (LIL_SNF_ARG *) g_ptr_array_index(additional_data, 0);
  row_nums_target = (GPtrArray *) g_ptr_array_index(additional_data, 1);
  lil_matrix_snf_reduction_by_inclusion_at_each_row(thread_args->matrix, arg, row_nums_target, thread_args->number_start, thread_args->number_end, row_changed, false);
  return NULL;
}

static gpointer lil_matrix_snf_reduction_by_inclusion_split_numbers_thread_func (ThreadArgs *thread_args)
{
  int i, j, row_num, val;
  GPtrArray *additional_data, *row_nums_to_split, *row_nums_independent, *row_nums_dependent;
  GArray *row_nums, *row_nums_to_be_changed, *nums_dep, *nums_indep;
  additional_data = thread_args->data;
  row_nums_to_split = (GPtrArray *) g_ptr_array_index(additional_data, 0);
  row_nums_independent = (GPtrArray *) g_ptr_array_index(additional_data, 1);
  row_nums_dependent = (GPtrArray *) g_ptr_array_index(additional_data, 2);
  row_nums_to_be_changed = g_array_new(false, false, sizeof(int));
  for (i = 0; i < row_nums_to_split->len; i++) {
    row_nums = (GArray *) g_ptr_array_index(row_nums_to_split, i);
    row_num = g_array_index(row_nums, int, 0);
    g_array_append_val(row_nums_to_be_changed, row_num);
  }
  g_array_sort(row_nums_to_be_changed, compare_integers);
  for (i = 0; i < row_nums_to_split->len; i++) {
    row_nums = (GArray *) g_ptr_array_index(row_nums_to_split, i);
    row_num = g_array_index(row_nums, int, 0);
    nums_dep = g_array_sized_new(false, false, sizeof(int), row_nums->len);
    nums_indep = g_array_sized_new(false, false, sizeof(int), row_nums->len);
    g_array_append_val(nums_dep, row_num);
    g_array_append_val(nums_indep, row_num);
    for (j = 1; j < row_nums->len; j++) {
      val = g_array_index(row_nums, int, j);
      g_array_append_val((array_include_p(row_nums_to_be_changed, val) ? nums_dep : nums_indep), val);
    }
    if (nums_dep->len > 1) {
      g_ptr_array_add(row_nums_dependent, nums_dep);
    } else {
      g_array_free(nums_dep, TRUE);
    }
    if (nums_indep->len > 1) {
      g_ptr_array_add(row_nums_independent, nums_indep);
    } else {
      g_array_free(nums_indep, TRUE);
    }
  }
  g_array_free(row_nums_to_be_changed, TRUE);
  return NULL;
}

GPtrArray *lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *row_nums_target, GPtrArray *row_nums_to_split)
{
  ThreadExecArgs thread_exec_args;
  GPtrArray *additional_data;
  gpointer ret;
  additional_data = g_ptr_array_new();
  g_ptr_array_add(additional_data, arg);
  g_ptr_array_add(additional_data, row_nums_target);
  lil_matrix_thread_exec_args_set(&thread_exec_args, "Reduce elements", THREAD_NUMBER_DEFAULT,
                                  mat, 0, row_nums_target->len, additional_data,
                                  lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread_args_alloc,
                                  lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread_args_free,
                                  lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread_thread_func);
  if (row_nums_to_split) {
    ThreadExecArgs thread_exec_args_split;
    GPtrArray *thread_exec_args_array, *ret_vals, *additional_data2;
    additional_data2 = g_ptr_array_sized_new(2);
    g_ptr_array_add(additional_data2, row_nums_to_split);
    g_ptr_array_add(additional_data2, g_ptr_array_new());
    g_ptr_array_add(additional_data2, g_ptr_array_new());
    lil_matrix_thread_exec_args_set(&thread_exec_args_split, "Reduce row numbers", 1, NULL, 0, 0, additional_data2,
                                    NULL, NULL, lil_matrix_snf_reduction_by_inclusion_split_numbers_thread_func);
    thread_exec_args_array = g_ptr_array_sized_new(2);
    g_ptr_array_add(thread_exec_args_array, (gpointer) &thread_exec_args_split);
    g_ptr_array_add(thread_exec_args_array, (gpointer) &thread_exec_args);
    ret_vals = lil_matrix_thread_exec_in_parallel(thread_exec_args_array);
    ret = additional_data2;
    g_ptr_array_free(ret_vals, TRUE);
    g_ptr_array_free(thread_exec_args_array, TRUE);
  } else {
    ret = NULL;
    lil_matrix_thread_exec(&thread_exec_args);
  }
  g_ptr_array_free(additional_data, TRUE);
  return ret;
}

static void lil_matrix_cache_count_non_zeros (mpz_t count, GPtrArray *row_minimal)
{
  int i;
  LIL_MATRIX_MIN_VALUE_CACHE *cache;
  mpz_set_si(count, 0);
  for (i = 0; i < row_minimal->len; i++) {
    cache = (LIL_MATRIX_MIN_VALUE_CACHE *) g_ptr_array_index(row_minimal, i);
    if (cache) {
      mpz_add_ui(count, count, cache->size);
    }
  }
}

static void lil_matrix_snf_reduction_by_inclusion_create_cache (LIL_MATRIX *mat, LIL_SNF_ARG *arg, mpz_t count)
{
  lil_matrix_update_cache(mat, arg, NULL);
  if (count) {
    lil_matrix_cache_count_non_zeros(count, arg->cache->row_minimal);
  }
}

/* Make inclusion_index empty, but do not free it. */
void lil_matrix_snf_inclusion_index_clear (GPtrArray *inclusion_index, bool free_index)
{
  int i;
  if (inclusion_index) {
    for (i = 0; i < inclusion_index->len; i++) {
      g_array_free((GArray *) g_ptr_array_index(inclusion_index, i), TRUE);
    }
    g_ptr_array_set_size(inclusion_index, 0);
    if (free_index) {
      g_ptr_array_free(inclusion_index, TRUE);
    }
  }
}

void lil_matrix_snf_reduction_by_inclusion_targets_free (GPtrArray *targets)
{
  int i;
  GArray *row_nums;
  for (i = 0; i < targets->len; i++) {
    row_nums = (GArray *) g_ptr_array_index(targets, i);
    g_array_free(row_nums, TRUE);
  }
  g_ptr_array_free(targets, TRUE);
}

/*
  We need to recreate cache before executing lil_matrix_snf_reduction_by_inclusion_step.
  That is, we should execute lil_matrix_snf_reduction_by_inclusion_create_cache.
*/
static void lil_matrix_snf_reduction_by_inclusion_step (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *inclusion_index)
{
  GArray *pivot_row_nums;
  GPtrArray *row_nums_splitted, *pivot_splitted, *splitted_next;
  lil_matrix_snf_split_inclusion_index(mat, inclusion_index, &pivot_row_nums, &pivot_splitted, &row_nums_splitted);
  if (pivot_splitted->len <= 100) {
    lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread(mat, arg, row_nums_splitted, NULL);
  } else {
    splitted_next = lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread(mat, arg, row_nums_splitted, pivot_splitted);
    lil_matrix_snf_reduction_by_inclusion_targets_free(pivot_splitted);
    lil_matrix_snf_reduction_by_inclusion_targets_free(row_nums_splitted);
    row_nums_splitted = (GPtrArray *) g_ptr_array_index(splitted_next, 1);
    pivot_splitted = (GPtrArray *) g_ptr_array_index(splitted_next, 2);
    lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread(mat, arg, row_nums_splitted, NULL);
    g_ptr_array_free(splitted_next, TRUE);
  }

  lil_matrix_snf_reduction_by_inclusion_at_each_row(mat, arg, pivot_splitted, 0, mat->row_size - 1, NULL, false);
  /* lil_matrix_snf_reduction_by_inclusion_at_each_row(mat, arg, pivot_splitted, 0, mat->row_size - 1, NULL, true); /\* Too late *\/ */
  g_array_free(pivot_row_nums, TRUE);
  lil_matrix_snf_reduction_by_inclusion_targets_free(row_nums_splitted);
  lil_matrix_snf_reduction_by_inclusion_targets_free(pivot_splitted);
}

static void lil_matrix_snf_reduction_by_inclusion_repeatedly (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray *init_target_rows)
{
  mpz_t count, count_last, count_deleted;
  GPtrArray *inclusion_index;
  inclusion_index = g_ptr_array_new();
  lil_matrix_snf_reduction_by_inclusion_create_cache(mat, arg, NULL);
  lil_matrix_snf_create_inclusion_index_multithread(mat, arg, inclusion_index, init_target_rows);
  mpz_init(count);
  mpz_init(count_last);
  mpz_init(count_deleted);
  lil_matrix_snf_reduction_by_inclusion_create_cache(mat, arg, count);
  mpz_set(count_last, count);

  while (true) {
    lil_matrix_snf_reduction_by_inclusion_step(mat, arg, inclusion_index);
    mpz_set(count_last, count);
    lil_matrix_snf_reduction_by_inclusion_create_cache(mat, arg, count);
    mpz_sub(count_deleted, count_last, count);
    if (mpz_cmp_ui(count_deleted, 0) > 0) {
      GArray *target_rows;
      lil_matrix_snf_inclusion_index_clear(inclusion_index, false);
      lil_matrix_update_cache(mat, arg, NULL);
      target_rows = lil_snf_argument_cache_take_cumulative_row_changed(arg, true);
      lil_matrix_snf_create_inclusion_index_multithread(mat, arg, inclusion_index, target_rows);
      g_array_free(target_rows, TRUE);
    } else {
      lil_matrix_snf_inclusion_index_clear(inclusion_index, true);
      break;
    }
  }

  mpz_clear(count);
  mpz_clear(count_last);
  mpz_clear(count_deleted);
}

static void lil_matrix_snf_reduction_by_inclusion (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray *target_rows)
{
  GPtrArray *inclusion_index;
  lil_matrix_snf_reduction_by_inclusion_create_cache(mat, arg, NULL);
  inclusion_index = g_ptr_array_new();
  lil_matrix_snf_create_inclusion_index_multithread(mat, arg, inclusion_index, target_rows);
  lil_matrix_snf_reduction_by_inclusion_step(mat, arg, inclusion_index);
  lil_matrix_snf_inclusion_index_clear(inclusion_index, true);
}

static bool lil_matrix_snf_validate_snf_values_row_range (LIL_MATRIX *mat, GPtrArray *pivot_array, int row_num_start, int row_num_end)
{
  int i, row_num, col_num;
  GSList *row_list_node, *row_node;
  LIL_MATRIX_PIVOT *pivot;
  GSList *el_node;
  mpz_t val;
  bool find_invalid_element;
  find_invalid_element = false;
  mpz_init(val);
  for (i = 0; i < pivot_array->len; i++) {
    pivot = (LIL_MATRIX_PIVOT *) g_ptr_array_index(pivot_array, i);
    row_num = row_num_start;
    row_list_node = (GSList *) g_slist_nth(mat->rows, row_num);
    while (row_list_node) {
      row_node = (GSList *) row_list_node->data;
      if (row_node) {
        el_node = (GSList *) g_slist_nth(row_node, 0);
        while (el_node) {
          col_num = lil_matrix_element_column_number(mat, el_node->data);
          if (col_num == pivot->column && row_num != pivot->row) {
            lil_matrix_element_value(val, mat, el_node->data);
            gmp_fprintf(stderr, "Element %Zd at (%d %d) exists!\n", val, pivot->row, pivot->column);
            find_invalid_element = true;
          }
          el_node = g_slist_next(el_node);
        }
      }
      row_list_node = g_slist_next(row_list_node);
      row_num += 1;
      if (row_num > row_num_end) {
        break;
      }
    }
  }
  mpz_clear(val);
  return !find_invalid_element;
}

static void lil_matrix_snf_validate_snf_values_args_alloc (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i;
  for (i = 0; i < thread_num; i++) {
    g_ptr_array_add((GPtrArray *) g_ptr_array_index(thread_args, i), (gpointer) g_malloc(sizeof(bool)));
  }
}

static gpointer lil_matrix_snf_validate_snf_values_args_free (int thread_num, GPtrArray *thread_args, gpointer additional_data)
{
  int i;
  bool ret, *ret_thread;
  ret = true;
  for (i = 0; i < thread_num; i++) {
    ret_thread = (bool *) g_ptr_array_index((GPtrArray *) g_ptr_array_index(thread_args, i), 0);
    if (!ret_thread) {
      ret = false;
    }
    g_free(ret_thread);
  }
  return (ret ? g_free : NULL);
}

static gpointer lil_matrix_snf_validate_snf_values_thread_func (ThreadArgs *thread_args)
{
  bool *ret;
  GPtrArray *pivot_array;
  pivot_array = (GPtrArray *) thread_args->data;
  ret = (bool *) g_ptr_array_index((GPtrArray *)thread_args->arguments, 0);
  *ret = lil_matrix_snf_validate_snf_values_row_range(thread_args->matrix, pivot_array, thread_args->number_start, thread_args->number_end);
  return NULL;
}

void lil_matrix_snf_validate_snf_values (LIL_MATRIX *mat, GPtrArray *pivot_array)
{
  ThreadExecArgs thread_exec_args;
  lil_matrix_thread_exec_args_set(&thread_exec_args,  "Validate SNF values", THREAD_NUMBER_DEFAULT,
                                  mat, 0, mat->row_size - 1, (gpointer) pivot_array,
                                  lil_matrix_snf_validate_snf_values_args_alloc,
                                  lil_matrix_snf_validate_snf_values_args_free,
                                  lil_matrix_snf_validate_snf_values_thread_func);
  if (lil_matrix_thread_exec(&thread_exec_args)) {
    printf("Valid SNF values\n");
  } else {
    abort_with_line_number();
  }
}

void lil_matrix_snf_save_column_numbers (GArray *column_pivots_seq, GPtrArray *pivot_array)
{
  int i;
  LIL_MATRIX_PIVOT *pivot;
  for (i = 0; i < pivot_array->len; i++) {
    pivot = g_ptr_array_index(pivot_array, i);
    g_array_append_val(column_pivots_seq, pivot->column);
  }
}

GPtrArray *lil_matrix_snf_take_elementary_divisors (LIL_MATRIX *mat, GPtrArray *pivot_array)
{
  int i;
  LIL_MATRIX_PIVOT *pivot;
  __mpz_struct *val;
  GPtrArray *snf_vals;
  snf_vals = g_ptr_array_new();
  for (i = 0; i < pivot_array->len; i++) {
    pivot = g_ptr_array_index(pivot_array, i);
    val = (__mpz_struct *) g_malloc(sizeof(__mpz_struct));
    mpz_init(val);
    lil_matrix_element_mpz(val, mat, pivot->row, pivot->column);
    mpz_abs(val, val);
    g_ptr_array_add(snf_vals, val);
  }
  return snf_vals;
}

void lil_matrix_snf_make_pivot_rows_empty (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *pivot_array)
{
  int i;
  LIL_MATRIX_PIVOT *pivot;
  GSList *row_list_node;
  for (i = 0; i < pivot_array->len; i++) {
    pivot = g_ptr_array_index(pivot_array, i);
    row_list_node = g_slist_nth(mat->rows, pivot->row);
    lil_snf_argument_remove_row_cache(arg, pivot->row);
    if (row_list_node->data) {
      lil_matrix_row_data_clear(row_list_node->data, mat->use_gmp_p);
      row_list_node->data = NULL;
    }
  }
}

/*
  Note that
  - Matrix must be non zero
  - Columns must not be deleted because we need to record column operations
  - Rows must not be deleted because column index cache becomes incompatible
*/
static GPtrArray *lil_matrix_snf_partially (LIL_MATRIX *mat, LIL_SNF_ARG *arg, LIL_MATRIX *column_ops, GArray *column_pivots_seq, int *count_empty_rows, int *sum_increase)
{
  int i, el_row_num, loop_count, row, col;
  bool divisible;
  mpz_t q, min_abs_val;
  GPtrArray *pivot_array, *snf_vals;
  LIL_MATRIX_PIVOT *pivot;
  mpz_init(min_abs_val);
  pivot_array = NULL;
  pivot = NULL;
  mpz_init(q);
  for (loop_count = 0; ; loop_count++) {
    if (pivot_array) {
      pivot_array = lil_matrix_snf_pivot_array_free(pivot_array, -1);
    }
    lil_matrix_update_cache(mat, arg, min_abs_val);
    pivot_array = lil_matrix_minimal_non_zero_element(&el_row_num, count_empty_rows, mat, 0, mat->row_size - 1, min_abs_val, arg, sum_increase);
    if (!pivot_array) {
      if (arg->debug) {
        fprintf(stderr, "[Loop %d] Pivots: empty\n", loop_count);
      }
      break;
    }
    arg->stat->increasing = (el_row_num > 2);
    if (arg->debug) {
      fprintf(stderr, "[Loop %d] Pivots: %d\n", loop_count, pivot_array->len);
      for (i = 0; i < pivot_array->len; i++) {
        pivot = g_ptr_array_index(pivot_array, i);
        gmp_fprintf(stderr, "  Pivot: %d %d, Value: %Zd, Column elements: %d\n", pivot->row, pivot->column, pivot->abs, el_row_num);
      }
    }
    if (arg->cache->type == CACHE_BOTH && !lil_snf_argument_cache_column_index_updated_p(arg)) {
      fprintf(stderr, "Not updated column index cache; row_changed=%d, row_recreated=%d\n",
              arg->cache->row_changed->len, arg->cache->row_recreated->len);
      abort_with_line_number();
    }
    for (i = 0; i < pivot_array->len; i++) {
      pivot = g_ptr_array_index(pivot_array, i);
      lil_snf_argument_remove_row_cache(arg, pivot->row);
    }
    if (!lil_matrix_snf_partially_row_reduce(mat, pivot_array, arg, 0, mat->row_size - 1)) {
      continue;
    }
    if (!lil_matrix_snf_partially_column_reduce_after_row_reduction(mat, pivot_array, column_ops)) {
      continue;
    }
    /* Check only remainder of first element of pivot_array because all pivots have same absolute values */
    pivot = g_ptr_array_index(pivot_array, 0);
    if (!lil_matrix_snf_divisible_p(mat, &divisible, &row, &col, q, pivot->row, pivot->column)) {
      fprintf(stderr, "Invalid matrix. (%d, %d) element is zero.\n", pivot->row, pivot->column);
      abort_with_line_number();
    }
    if (!divisible) {
      lil_matrix_add_rows(mat, pivot->row, row, 1);
      mpz_neg(q, q);
      lil_matrix_add_columns_mpz(mat, col, pivot->column, q);
      if (column_ops) {
        lil_matrix_add_rows_mpz(column_ops, col, pivot->column, q);
      }
      lil_snf_argument_cache_reset(arg, CACHE_NOT_CHANGED, mat);
    } else {
      break;
    }
  }
  mpz_clear(q);
  mpz_clear(min_abs_val);
  if (pivot_array) {
    if (column_pivots_seq) {
      lil_matrix_snf_save_column_numbers(column_pivots_seq, pivot_array);
    }
    snf_vals = lil_matrix_snf_take_elementary_divisors(mat, pivot_array);
    lil_matrix_snf_make_pivot_rows_empty(mat, arg, pivot_array);
    if (arg->cache->type == CACHE_BOTH && arg->validate > 0) {
      lil_matrix_snf_validate_snf_values(mat, pivot_array);
    }
    pivot_array = lil_matrix_snf_pivot_array_free(pivot_array, -1);
  } else {
    snf_vals = NULL;
  }
  return snf_vals;
}

void lil_matrix_snf_print (GArray *snf)
{
  int i;
  char *fmt;
  for (i = 0; i < snf->len; i++) {
    if (i % 2 == 0) {
      if (i == 0) {
        fmt = "%Zd: ";
      } else {
        fmt = ", %Zd: ";
      }
    } else {
      fmt = "%Zd";
    }
    gmp_printf(fmt, g_array_index(snf, mpz_t, i));
  }
  printf("\n");
}

static GString *lil_matrix_strftime (const char *format)
{
  GString *str;
  time_t t;
  struct tm *cur_tm;
  int len;
  t = time(NULL);
  cur_tm = localtime(&t);
  if (cur_tm == NULL) {
    fprintf(stderr, "Can not get time\n");
    abort_with_line_number();
  }
  str = g_string_sized_new(TIME_STRING_SIZE);
  len = strftime(str->str, str->allocated_len, format, cur_tm);
  if (len == 0) {
    fprintf(stderr, "strftime returns 0\n");
  }
  str->len = len;
  return str;
}

void lil_matrix_snf_output_info (char *str, CalcStat *stat, LIL_MATRIX *mat, GArray *snf)
{
  GString *t;
  t = lil_matrix_strftime("%F %T");
  if (stat) {
    printf("[%s: %d %d] %s\n", str, stat->step, stat->elementary_divisors, t->str);
    printf("  Total row operations: %d\n", stat->row_operations);
  } else {
    printf("[%s] %s\n", str, t->str);
  }
  g_string_free(t, TRUE);
  if (snf) {
    printf("  ");
    lil_matrix_snf_print(snf);
  }
  if (mat) {
    printf("  ");
    if (stat && snf) {
      lil_matrix_debug_print_info_with_stat(mat, stat);
    } else {
      lil_matrix_debug_print_info(mat);
    }
  }
  fflush(stdout);
}

static bool lil_matrix_snf_row_cache_valid_p (LIL_MATRIX *mat, LIL_SNF_ARG *arg)
{
  GSList *row_list_node, *row_node;
  LIL_MATRIX_MIN_VALUE_CACHE *cache;
  int row_num, col, size;
  mpz_t val;
  bool ret;
  ret = true;
  mpz_init(val);
  row_num = 0;
  row_list_node = g_slist_nth(mat->rows, row_num);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    cache = lil_snf_argument_cache_row_minimal_get_without_creation(arg, row_num);
    if (cache) {
      lil_matrix_minimal_non_zero_element_at_row(mat, row_node, &col, val, &size);
      if (col != cache->column) {
        fprintf(stderr, "Cache of column is invalid: cache->column=%d, col=%d\n", cache->column, col);
        ret = false;
      }
      if (size != cache->size) {
        fprintf(stderr, "Cache of size is invalid: cache->size=%d, size=%d\n", cache->size, size);
        ret = false;
      }
      if (mpz_cmp(val, cache->value) != 0) {
        gmp_fprintf(stderr, "Cache of value is invalid: cache->value=%Zd, val=%Zd\n", cache->value, val);
        ret = false;
      }
    }
    row_list_node = g_slist_next(row_list_node);
    row_num += 1;
  }
  mpz_clear(val);
  return ret;
}

void lil_matrix_snf_output_validate (LIL_MATRIX *mat, LIL_SNF_ARG *arg, char *str)
{
  int count;
  if (arg) {
    count = arg->stat->step;
  } else {
    count = 0;
  }
  if (str) {
    printf("[Validation %d: %s]  ", count, str);
  } else {
    printf("[Validation %d]  ", count);
  }
  if (lil_matrix_valid_p(mat)) {
    printf("Matrix: Valid\n");
  } else {
    printf("Matrix: NOT valid\n");
    /* We want to dump matrix data to file. */
    abort_with_line_number();
  }
}

static bool lil_matrix_snf_column_index_valid_p (LIL_MATRIX *mat, LIL_SNF_ARG *arg)
{
  int i, j, num, num_last;
  GPtrArray *column_index;
  GArray *row_nums;
  bool ret;
  ret = true;
  column_index = arg->cache->column_index;
  if (column_index->len != mat->column_size) {
    printf("Column size does not coincide with cache size\n");
    return false;
  }
  for (i = 0; i < column_index->len; i++) {
    row_nums = g_ptr_array_index(column_index, i);
    if (row_nums && row_nums->len) {
      num_last = -1;
      for (j = 0; j < row_nums->len; j++) {
        num = g_array_index(row_nums, int, j);
        if (num_last > num) {
          printf("Column index is not sorted: column %d, row %d %d\n", i, num_last, num);
          ret = false;
        } else if (num_last == num) {
          printf("Column index has duplicated numbers: column %d, row %d\n", i, num);
          ret = false;
        }
        num_last = num;
      }
    }
  }
  return ret;
}

void lil_matrix_snf_cache_validate (LIL_MATRIX *mat, LIL_SNF_ARG *arg)
{
  if (lil_matrix_snf_row_cache_valid_p(mat, arg)) {
    printf("  Row cache: %d cache, Valid\n", arg->cache->row_minimal->len);
  } else {
    printf("  Row cache: %d cache, Not valid\n", arg->cache->row_minimal->len);
    abort_with_line_number();
  }
  if (lil_matrix_snf_column_index_valid_p(mat, arg)) {
    printf("  Column index is valid\n");
  } else {
    printf("  Column index is not valid\n");
    abort_with_line_number();
  }
}

static LIL_MATRIX *lil_matrix_snf_column_operations_matrix_init (int matrix_size, int modulo)
{
  int i, val;
  LIL_MATRIX *column_ops;
  GSList *row_list_node, *row_node;
  val = 1;
  column_ops = lil_matrix_new(matrix_size, matrix_size, modulo);
  row_list_node = g_slist_nth(column_ops->rows, 0);
  for (i = 0; i < matrix_size; i++) {
    row_node = (GSList *) row_list_node->data;
    row_node = g_slist_append(row_node, lil_matrix_element_suitable_new(column_ops, i, val));
    row_list_node->data = row_node;
    row_list_node = g_slist_next(row_list_node);
  }
  return column_ops;;
}

/* Exchange rows of column operations by the excluded order and dump the matrix to file. */
void lil_matrix_snf_column_operations_matrix_dump_clear (LIL_MATRIX *mat, GArray *column_pivots_seq, char *path)
{
  FILE *fp;
  int i;
  GSList *rows_new, *row_list_node_new, *row_list_node;
  rows_new = lil_matrix_rows_new (mat->row_size);
  row_list_node_new = g_slist_nth(rows_new, 0);
  for (i = 0; i < column_pivots_seq->len; i++) {
    row_list_node = g_slist_nth(mat->rows, g_array_index(column_pivots_seq, int, i));
    row_list_node_new->data = (GSList *) row_list_node->data;
    row_list_node->data = NULL;
    row_list_node_new = g_slist_next(row_list_node_new);
  }
  g_slist_free(mat->rows);
  mat->rows = rows_new;
  g_array_free(column_pivots_seq, TRUE);

  fp = fopen(path, "w");
  if (fp == NULL) {
    fprintf(stderr, "Can not open file %s\n", path);
    abort_with_line_number();
  }
  lil_matrix_fprint(fp, mat);
  fclose(fp);
  lil_matrix_clear(mat);
}

void lil_matrix_snf_array_free (GArray *snf)
{
  int i;
  for (i = 0; i < snf->len; i++) {
    mpz_clear(g_array_index(snf, mpz_t, i));
  }
  g_array_free(snf, TRUE);
}

__mpz_struct *lil_matrix_snf_array_new_value_init (GArray *snf)
{
  mpz_t val_new;
  mpz_init(val_new);
  g_array_append_val(snf, val_new);
  return g_array_index(snf, mpz_t, snf->len - 1);
}

void lil_matrix_snf_array_append (GArray *snf, mpz_t val)
{
  __mpz_struct *val_new;
  val_new = lil_matrix_snf_array_new_value_init(snf);
  mpz_set(val_new, val);
}

void lil_matrix_snf_validation (LIL_MATRIX *mat, LIL_SNF_ARG *arg)
{
  if (arg->validate > 0 && arg->stat->elementary_divisors - arg->stat->last_validating > arg->validate) {
    lil_matrix_snf_output_validate(mat, arg, NULL);
    if (arg->cache) {
      lil_matrix_snf_cache_validate(mat, arg);
    }
    arg->stat->last_validating = arg->stat->elementary_divisors;
  }
}

static void catch_sigterm (int sig) {
  fprintf(stderr, "[SIGNAL] Catch sigterm\n");
  signal_sigterm = true;
}

GString *lil_matrix_snf_intermediate_result_dump (LIL_SNF_ARG *arg, GArray *snf, LIL_MATRIX *column_ops, GArray *column_pivots_seq)
{
  int i;
  FILE *fp;
  GString *dir, *path;
  dir = lil_matrix_strftime("dump_%Y%m%d_%H%M%S");
  if (g_mkdir_with_parents(dir->str, 0755) == -1) {
    fprintf(stderr, "Can not make directory\n");
    abort_with_line_number();
  }

  path = g_string_sized_new(dir->len * 2);
  g_string_printf(path, "%s/snf.txt", dir->str);
  fp = fopen(path->str, "w");
  if (fp == NULL) {
    fprintf(stderr, "Can not open file %s\n", path->str);
    abort_with_line_number();
  }
  for (i = 0; i < snf->len; i++) {
    gmp_fprintf(fp, (i == 0 ? "%Zd" : " %Zd"), g_array_index(snf, mpz_t, i));
  }
  fclose(fp);

  if (column_ops) {
    g_string_printf(path, "%s/column_ops.txt", dir->str);
    lil_matrix_dump_to_file(column_ops, path->str, LIL_MATRIX_DATA_TEXT);

    g_string_printf(path, "%s/column_pivots_seq.txt", dir->str);
    fp = fopen(path->str, "w");
    if (fp == NULL) {
      fprintf(stderr, "Can not open file %s\n", path->str);
      abort_with_line_number();
    }
    for (i = 0; i < column_pivots_seq->len; i++) {
      fprintf(fp, (i == 0 ? "%d" : " %d"), g_array_index(column_pivots_seq, int, i));
    }
    fclose(fp);
  }
  g_string_free(path, TRUE);
  return dir;
}

static void lil_matrix_snf_intermediate_dump (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray *snf, LIL_MATRIX *column_ops, GArray *column_pivots_seq)
{
  GString *dir, *path;
  dir = lil_matrix_snf_intermediate_result_dump(arg, snf, column_ops, column_pivots_seq);
  path = g_string_sized_new(dir->len * 2);
  g_string_printf(path, "%s/mat.txt", dir->str);
  lil_matrix_dump_to_file(mat, path->str, LIL_MATRIX_DATA_TEXT);
  g_string_free(dir, TRUE);
  g_string_free(path, TRUE);
}

void lil_matrix_snf_load_directory (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray **snf, LIL_MATRIX **column_ops, GArray **column_pivots_seq)
{
  *column_ops = NULL;
  *column_pivots_seq = NULL;
  *snf = g_array_new(FALSE, FALSE, sizeof(mpz_t));
  if (arg->column_ops_output) {
    if (!arg->load_directory) {
      *column_ops = lil_matrix_snf_column_operations_matrix_init(mat->column_size, mat->modulo);
    }
    *column_pivots_seq = g_array_new(FALSE, FALSE, sizeof(int));
  }
  if (arg->load_directory) {
    int max_line_size, line_size, i;
    FILE *fp;
    char path[256], *line;
    gchar **tokens;
    g_sprintf(path, "%s/snf.txt", arg->load_directory);
    fp = fopen(path, "r");
    lil_matrix_init_buffer_to_read(&max_line_size, &line, sizeof(char));
    while (fileread_gets_line(&line, &line_size, &max_line_size, fp)) {
      tokens = g_strsplit(line, " ", -1);
      i = 0;
      while (tokens[i]) {
        if (strlen(tokens[i]) > 0) {
          mpz_t val;
          mpz_init(val);
          mpz_set_str(val, tokens[i], 10);
          g_array_append_val(*snf, val);
          if (i % 2 == 1 && mpz_cmp_ui(g_array_index(*snf, mpz_t, i - 1), 0) != 0) {
            arg->stat->elementary_divisors += mpz_get_si(val);
          }
        }
        i += 1;
      }
      g_strfreev(tokens);
    }
    g_free(line);
    fclose(fp);
    if (arg->column_ops_output) {
      g_sprintf(path, "%s/column_ops.txt", arg->load_directory);
      *column_ops = lil_matrix_load_file(path, LIL_MATRIX_DATA_TEXT); 

      g_sprintf(path, "%s/column_pivots_seq.txt", arg->load_directory);
      fp = fopen(path, "r");
      lil_matrix_init_buffer_to_read(&max_line_size, &line, sizeof(char));
      while (fileread_gets_line(&line, &line_size, &max_line_size, fp)) {
        tokens = g_strsplit(line, " ", -1);
        i = 0;
        while (tokens[i]) {
          if (strlen(tokens[i]) > 0) {
            int val_int;
            val_int = atoi(tokens[i]);
            g_array_append_val(*column_pivots_seq, val_int);
          }
          i += 1;
        }
        g_strfreev(tokens);
      }
      g_free(line);
      fclose(fp);
    }
  } else {
    __mpz_struct *free_part_number;
    free_part_number = lil_matrix_snf_array_new_value_init(*snf);
    mpz_set_si(free_part_number, 0);
    free_part_number = lil_matrix_snf_array_new_value_init(*snf);
    mpz_set_si(free_part_number, (mat->row_size > mat->column_size ? mat->column_size : mat->row_size));
  }
}

LIL_MATRIX *lil_matrix_snf_load_directory_matrix (LIL_SNF_ARG *arg)
{
  LIL_MATRIX *mat;
  char *input;
  input = (char *) g_malloc(sizeof(char) * (strlen(arg->load_directory) + 32));
  g_sprintf(input, "%s/mat.txt", arg->load_directory);
  mat = lil_matrix_load_file(input, LIL_MATRIX_DATA_TEXT);
  g_free(input);
  return mat;
}

void lil_matrix_snf_signal_set ()
{
  signal_sigterm = false;
  if (SIG_ERR == signal(SIGTERM, catch_sigterm)) {
    fprintf(stderr, "Failed to set signal handler.\n");
    exit(1);
  }
  if (SIG_ERR == signal(SIGINT, catch_sigterm)) {
    fprintf(stderr, "Failed to set signal handler.\n");
    exit(1);
  }
}

/* mat is devided by values of SNF */
void lil_matrix_snf_divide_snf_values (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray *snf, GPtrArray *snf_vals)
{
  __mpz_struct *val;
  int i;
  for (i = 0; i < snf_vals->len; i++) {
    val = (__mpz_struct *) g_ptr_array_index(snf_vals, i);
    if (mpz_cmp_ui(val, 1) != 0) {
      lil_matrix_divide_elements_mpz(mat, val);
      lil_snf_argument_cache_reset(arg, CACHE_NOT_CHANGED, mat);
    }
  }
}

void lil_matrix_snf_concat_clear_results (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray *snf, GPtrArray **snf_vals_ptr)
{
  GPtrArray *snf_vals;
  snf_vals = *snf_vals_ptr;
  int i, ind;
  mpz_ptr val_last;
  __mpz_struct *val, *val_count;
  lil_matrix_snf_divide_snf_values(mat, arg, snf, snf_vals);
  for (i = 0; i < snf_vals->len; i++) {
    val = (__mpz_struct *) g_ptr_array_index(snf_vals, i);
    /* snf->len > 0 because free part data is set. */
    val_last = g_array_index(snf, mpz_t, snf->len - 2);
    if (mpz_cmp_ui(val_last, 0) == 0 || mpz_cmp_ui(val, 1) > 0) {
      if (mpz_cmp_ui(val_last, 0) != 0) {
        mpz_mul(val, val, val_last);
      }
      lil_matrix_snf_array_append (snf, val);
      val_count = lil_matrix_snf_array_new_value_init(snf);
      mpz_set_ui(val_count, 1);
    } else {
      ind = snf->len - 1;
      mpz_add_ui(g_array_index(snf, mpz_t, ind), g_array_index(snf, mpz_t, ind), 1);
    }
    mpz_clear(val);
    g_free(val);
  }
  g_ptr_array_free(snf_vals, TRUE);
  snf_vals = NULL;
}

void lil_matrix_snf_set_free_part_number (GArray *snf)
{
  int i;
  __mpz_struct *free_part_number;
  free_part_number = g_array_index(snf, mpz_t, 1);
  for (i = 2; i < snf->len; i += 2) {
    mpz_sub(free_part_number, free_part_number, g_array_index(snf, mpz_t, i + 1));
  }
  if (mpz_cmp_ui(free_part_number, 0) <= 0) {
    free_part_number = g_array_index(snf, mpz_t, 0);
    mpz_clear(free_part_number);
    free_part_number = g_array_index(snf, mpz_t, 1);
    mpz_clear(free_part_number);
    g_array_remove_range(snf, 0, 2);
  }
}

GArray *lil_matrix_snf (LIL_MATRIX *mat, LIL_SNF_ARG *arg)
{
  int sum_increase, cumulative_sum_increase;
  GArray *snf, *column_pivots_seq;
  LIL_MATRIX *column_ops;
  bool delete_at_min, reduce_by_inclusion;
  GPtrArray *snf_vals;
  if (arg->signal_use_p) {
    lil_matrix_snf_signal_set();
  }
  cumulative_sum_increase = 0;
  delete_at_min = arg->reduce_by_divisors > 0 || arg->auto_delete_duplicates;
  lil_snf_argument_cache_init(arg, CACHE_BOTH, mat);
  lil_matrix_snf_load_directory(mat, arg, &snf, &column_ops, &column_pivots_seq);
  if (arg->info > 0) {
    lil_matrix_snf_output_info("Matrix", NULL, mat, NULL);
  }
  lil_matrix_snf_validation(mat, arg);
  while (mat->column_size > 0 && mat->row_size > 0) {
    arg->stat->step += 1;
    reduce_by_inclusion = arg->stat->increasing &&
      (delete_at_min || (arg->reduce_by_divisors > 0 &&
                         (arg->stat->elementary_divisors - arg->stat->last_reducing) >= arg->reduce_by_divisors));
    snf_vals = lil_matrix_snf_partially(mat, arg, column_ops, column_pivots_seq, &arg->stat->empty_rows, &sum_increase);
    cumulative_sum_increase += sum_increase;
    if (!snf_vals) {
      break;
    }
    arg->stat->elementary_divisors += snf_vals->len;
    lil_matrix_snf_concat_clear_results(mat, arg, snf, &snf_vals);

    if (arg->info > 0 && arg->stat->elementary_divisors - arg->stat->last_output >= arg->info) {
      lil_matrix_snf_output_info("Calculation", arg->stat, mat, snf);
      arg->stat->last_output = arg->stat->elementary_divisors;
    }
    if (arg->auto_delete_duplicates && sum_increase > arg->max_estimate_increase) {
      GArray *target_rows;
      if (arg->info > 0) {
        lil_matrix_snf_output_info("Before reducing elements only for updated rows", arg->stat, mat, snf);
      }
      lil_matrix_update_cache(mat, arg, NULL);
      target_rows = lil_snf_argument_cache_take_cumulative_row_changed(arg, true);
      if (arg->reduce_repeatedly) {
        lil_matrix_snf_reduction_by_inclusion_repeatedly(mat, arg, target_rows);
      } else {
        lil_matrix_snf_reduction_by_inclusion(mat, arg, target_rows);
      }
      g_array_free(target_rows, TRUE);
      if (arg->info > 0) {
        lil_matrix_snf_output_info("Finish reducing only for updated rows", arg->stat, mat, snf);
      }
    }
    if (reduce_by_inclusion || (arg->auto_delete_duplicates && cumulative_sum_increase > arg->reduce_by_increase)) {
      if (arg->info > 0) {
        lil_matrix_snf_output_info("Before reducing elements", arg->stat, mat, snf);
      }
      arg->stat->last_reducing = arg->stat->elementary_divisors;
      lil_matrix_snf_reduction_by_inclusion(mat, arg, NULL);
      if (arg->info > 0) {
        lil_matrix_snf_output_info("Finish reducing", arg->stat, mat, snf);
      }
      if (delete_at_min) {
        delete_at_min = false;
      }
      cumulative_sum_increase = 0;
    }
    if (arg->stat->empty_rows > MAX_EMPTY_ROWS) {
      lil_matrix_snf_delete_empty_rows_and_cache(mat, arg);
      arg->stat->empty_rows = 0;
      if (arg->info > 0) {
        lil_matrix_snf_output_info("Deleted empty rows", arg->stat, mat, snf);
      }
    }
    lil_matrix_snf_validation(mat, arg);
    if (signal_sigterm) {
      lil_matrix_snf_intermediate_dump(mat, arg, snf, column_ops, column_pivots_seq);
      exit(0);
    }
  }
  if (column_ops) {
    lil_matrix_snf_column_operations_matrix_dump_clear(column_ops, column_pivots_seq, arg->column_ops_output);
  }
  lil_matrix_snf_set_free_part_number(snf);
  lil_snf_argument_cache_clear(arg);
  return snf;
}

/* For test */
GArray *lil_matrix_snf2 (LIL_MATRIX *mat)
{
  int i, j, count, val;
  GArray *snf, *snf_new, *free_part, *array_ptr;
  LIL_SNF_ARG *arg;
  arg = lil_snf_argument_new();
  snf = lil_matrix_snf(mat, arg);
  snf_new = g_array_new(FALSE, FALSE, sizeof(int));
  free_part = NULL;
  for (i = 0; i < snf->len; i++) {
    if (i % 2 == 0) {
      val = mpz_get_si(g_array_index(snf, mpz_t, i));
    } else {
      count = mpz_get_si(g_array_index(snf, mpz_t, i));
      if (val == 0) {
        free_part = g_array_new(FALSE, FALSE, sizeof(int));
        array_ptr = free_part;
      } else {
        array_ptr = snf_new;
      }
      for (j = 0; j < count; j++) {
        g_array_append_val(array_ptr, val);
      }
    }
  }
  if (free_part) {
    g_array_append_vals(snf_new, free_part->data, free_part->len);
    g_array_free(free_part, TRUE);
  }
  lil_snf_argument_free(arg);
  lil_matrix_snf_array_free(snf);
  return snf_new;
}

static void lil_matrix_debug_print_to (LIL_MATRIX *mat, FILE *fp)
{
  GSList *row_list_node, *row_node, *el_node;
  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      el_node = g_slist_nth(row_node, 0);
      if (el_node->data) {
        while (el_node) {
          if (mat->use_gmp_p) {
            LIL_MATRIX_ELEMENT_MPZ *el_mpz;
            el_mpz = (LIL_MATRIX_ELEMENT_MPZ *) el_node->data;
            gmp_fprintf(fp, "%d %Zd; ", el_mpz->column, el_mpz->value);
          } else {
            LIL_MATRIX_ELEMENT *el_int;
            el_int = (LIL_MATRIX_ELEMENT *) el_node->data;
            fprintf(fp, "%d %d; ", el_int->column, el_int->value);
          }
          el_node = g_slist_next(el_node);
        }
      }
      fprintf(fp, "\n");
    }
    row_list_node = g_slist_next(row_list_node);
  }
}

/* Returned array must be freed later by lil_matrix_column_element_count_free. */
static GArray *lil_matrix_column_element_count (LIL_MATRIX *mat)
{
  GArray *count;
  GSList *row_list_node, *row_node, *el_node;
  int i, col;
  mpz_t abs_val;
  mpz_ptr val, abs_old_var;
  mpz_init(abs_val);
  count = g_array_sized_new(FALSE, FALSE, sizeof(mpz_t), mat->column_size * 2);
  count->len = mat->column_size * 2;
  for (i = 0; i < count->len; i++) {
    val = g_array_index(count, mpz_t, i);
    mpz_init(val);
    mpz_set_ui(val, 0);
  }
  row_list_node = g_slist_nth(mat->rows, 0);
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      el_node = g_slist_nth(row_node, 0);
      while (el_node) {
        col = lil_matrix_element_value(abs_val, mat, el_node->data);
        mpz_abs(abs_val, abs_val);
        mpz_add_ui(g_array_index(count, mpz_t, col * 2), g_array_index(count, mpz_t, col * 2), 1);
        abs_old_var = g_array_index(count, mpz_t, col * 2 + 1);
        if (mpz_cmp_ui(abs_old_var, 0) || mpz_cmp(abs_val, abs_old_var) < 0) {
          mpz_set(g_array_index(count, mpz_t, col * 2 + 1), abs_val);
        }
        el_node = g_slist_next(el_node);
      }
    }
    row_list_node = g_slist_next(row_list_node);
  }
  mpz_clear(abs_val);
  return count;
}

static void lil_matrix_column_element_count_free (GArray *count)
{
  int i;
  for (i = 0; i < count->len; i++) {
    mpz_clear(g_array_index(count, mpz_t, i));
  }
  g_array_free(count, FALSE);
}

static int lil_matrix_pivot_column_number (GArray *count)
{
  int i, column_size, ret;
  mpz_t min_count, min_val, min_col;
  mpz_ptr col_count, abs_val;
  column_size = count->len / 2;
  mpz_init(min_col);
  mpz_init(min_count);
  mpz_init(min_val);
  mpz_set_ui(min_col, 0);
  mpz_set_ui(min_count, 0);
  mpz_set_ui(min_val, 0);
  for (i = 0; i < column_size; i++) {
    col_count = g_array_index(count, mpz_t, i * 2);
    abs_val = g_array_index(count, mpz_t, i * 2 + 1);
    if (mpz_cmp_ui(abs_val, 0) > 0 && (mpz_cmp_ui(min_count, 0) == 0 || mpz_cmp(abs_val, min_val) < 0 || (mpz_cmp(abs_val, min_val) == 0 && mpz_cmp(col_count, min_count) < 0))) {
      mpz_set_si(min_col, i);
      mpz_set(min_count, col_count);
      mpz_set(min_val, abs_val);
    }
  }
  ret = mpz_get_si(min_col);
  mpz_clear(min_col);
  mpz_clear(min_count);
  mpz_clear(min_val);
  return ret;
}

void lil_matrix_debug_print_count_column (LIL_MATRIX *mat)
{
  GArray *count;
  int i, print_count, min_col;
  mpz_ptr abs_val, col_count;
  count = lil_matrix_column_element_count(mat);
  print_count = 0;
  for (i = 0; i < mat->column_size; i++) {
    col_count = g_array_index(count, mpz_t, i * 2);
    abs_val = g_array_index(count, mpz_t, i * 2 + 1);
    if (mpz_cmp_ui(abs_val, 0) > 0) {
      gmp_printf("%d %Zd %Zd;", i, col_count, abs_val);
      print_count += 1;
      printf((print_count % PRINT_COLUMN_COUNT_LF == 0 ? "\n" : " "));
    }
  }
  if (print_count % PRINT_COLUMN_COUNT_LF != 0) {
    printf("\n");
  }
  min_col = lil_matrix_pivot_column_number (count);
  gmp_printf("Minimal non zero elements: %Zd %Zd %Zd\n",
             min_col, g_array_index(count, mpz_t, min_col * 2), g_array_index(count, mpz_t, min_col * 2 + 1));
  lil_matrix_column_element_count_free(count);
}

static bool lil_matrix_check_element (LIL_MATRIX *mat, gpointer el_data, int el_num, int row_num, int col_last, int *col_new)
{
  bool valid_p;
  mpz_t val;
  valid_p = true;
  mpz_init(val);
  *col_new = lil_matrix_element_value(val, mat, el_data);
  if (mpz_cmp_ui(val, 0) == 0) {
    fprintf(stderr, "%dth data of row %d for %dx%d matrix is zero\n",
            el_num, row_num, mat->row_size, mat->column_size);
    valid_p = false;
  }
  if (*col_new < 0 || *col_new >= mat->column_size) {
    gmp_fprintf(stderr, "%dth data of row %d for %dx%d matrix is invalid: column=%d, value=%Zd\n",
                el_num, row_num, mat->row_size, mat->column_size, *col_new, val);
    valid_p = false;
  }
  if (*col_new <= col_last) {
    gmp_fprintf(stderr, "%dth data of row %d for %dx%d matrix is invalid: column=%d, last_column=%d, value=%Zd\n",
                el_num, row_num, mat->row_size, mat->column_size, *col_new, col_last, val);
    valid_p = false;
  }
  mpz_clear(val);
  return valid_p;
}

bool lil_matrix_valid_p (LIL_MATRIX *mat)
{
  bool valid_p;
  int row_num, el_num, col_last;
  GSList *row_list_node, *row_node, *el_node;
  valid_p = true;
  row_list_node = g_slist_nth(mat->rows, 0);
  for (row_num = 0; ; row_num++) {
    if (!row_list_node) break;
    if (row_num >= mat->row_size) {
      fprintf(stderr, "Too many rows %d for %dx%d matrix\n", row_num, mat->row_size, mat->column_size);
      valid_p = false;
    }
    row_node = (GSList *) row_list_node->data;
    if (row_node) {
      el_node = g_slist_nth(row_node, 0);
      col_last = -1;
      for (el_num = 0; ; el_num++) {
        if (!el_node) break;
        if (!lil_matrix_check_element(mat, el_node->data, el_num, row_num, col_last, &col_last)) {
          valid_p = false;
        }
        el_node = g_slist_next(el_node);
      }
    }
    row_list_node = g_slist_next(row_list_node);
  }
  if (!valid_p) {
    lil_matrix_debug_print_to(mat, stderr);
  }
  return valid_p;
}

static void lil_matrix_fprint_row_node (FILE *fp, LIL_MATRIX *mat, GSList *row_node)
{
  int j, col;
  GSList *el_node;
  mpz_t val;
  mpz_init(val);
  el_node = g_slist_nth((GSList *)row_node->data, 0);
  col = -1;
  for (j = 0; j < mat->column_size; j++) {
    if (j > col && el_node) {
      col = lil_matrix_element_value(val, mat, el_node->data);
    }
    if (col >= 0 && col == j) {
      el_node = g_slist_next(el_node);
      gmp_fprintf(fp, (j == 0 ? "%Zd" : " %Zd"), val);
    } else {
      fprintf(fp, (j == 0 ? "0" : " 0"));
    }
  }
  fprintf(fp, "\n");
  mpz_clear(val);
}

void lil_matrix_fprint (FILE *fp, LIL_MATRIX *mat)
{
  int i;
  GSList *row_node;
  for (i = 0; i < mat->row_size; i++) {
    row_node = g_slist_nth(mat->rows, i);
    lil_matrix_fprint_row_node(fp, mat, row_node);
  }
}

void lil_matrix_print (LIL_MATRIX *mat)
{
  lil_matrix_fprint(stdout, mat);
}

void lil_matrix_print_row (LIL_MATRIX *mat, int row_num)
{
  GSList *row_node;
  check_row_number_argument(row_num, mat);
  row_node = g_slist_nth(mat->rows, row_num);
  lil_matrix_fprint_row_node(stdout, mat, row_node);
}

void lil_matrix_debug_print (LIL_MATRIX *mat)
{
  lil_matrix_debug_print_to(mat, stdout);
}

void lil_matrix_debug_print_range (LIL_MATRIX *mat, int row_start, int row_end)
{
  FILE *fp;
  GSList *row_list_node, *row_node, *el_node;
  int row_num, col;
  mpz_t val;
  mpz_init(val);
  fp = stdout;
  fprintf(fp, "# %d %d modulo %d\n", mat->row_size, mat->column_size, mat->modulo);
  row_list_node = g_slist_nth(mat->rows, row_start);
  row_num = row_start;
  while (row_list_node && row_num <= row_end) {
    if ((row_node = (GSList *) row_list_node->data) != NULL) {
      fprintf(fp, "%d: %d elements >", row_num, g_slist_length(row_node));
      el_node = g_slist_nth(row_node, 0);
      while (el_node) {
        col = lil_matrix_element_value(val, mat, el_node->data);
        gmp_fprintf(fp, " %d %Zd", col, val);
        el_node = g_slist_next(el_node);
      }
      fprintf(fp, "\n");
    } else {
      fprintf(fp, "%d: empty row\n", row_num);
    }
    row_list_node = g_slist_next(row_list_node);
    row_num += 1;
  }
  mpz_clear(val);
}

void lil_matrix_debug_print2 (LIL_MATRIX *mat)
{
  lil_matrix_debug_print_range(mat, 0, mat->row_size - 1);
}

void lil_matrix_debug_fprint_matrix_size (FILE *fp, LIL_MATRIX *mat, CalcStat *stat)
{
  if (stat) {
    fprintf(fp, "%d x %d", mat->row_size - stat->empty_rows, mat->column_size - stat->elementary_divisors);
  } else {
    fprintf(fp, "%d x %d", mat->row_size, mat->column_size);
  }
}

static void lil_matrix_debug_print_info_with_stat (LIL_MATRIX *mat, CalcStat *stat)
{
  mpz_t count, total;
  mpf_t per;
  FILE *fp;
  mpz_init(count);
  mpz_init(total);
  mpf_init2(per, MPF_PREC);
  fp = stdout;
  lil_matrix_debug_fprint_matrix_size(fp, mat, stat);
  lil_matrix_mpi_total_number_of_elements(mat, stat, total);
  lil_matrix_number_of_non_zero_elements(mat, count);
  lil_matrix_percentage_of_non_zero_elements(mat, stat, total, count, per);
  gmp_fprintf(fp, " mod %d, %Zd / %Zd, ", mat->modulo, count, total);
  gmp_fprintf(fp, (mpf_cmp_d(per, 0.01) >= 0 ? "%4.2Ff%%\n" : "%4.2lFe%%\n"), per);
  mpz_clear(count);
  mpz_clear(total);
  mpf_clear(per);
}

void lil_matrix_debug_print_info (LIL_MATRIX *mat)
{
  lil_matrix_debug_print_info_with_stat(mat, NULL);
}

void lil_matrix_debug_print_valid (LIL_MATRIX *mat)
{
  if (lil_matrix_valid_p(mat)) {
    printf("valid\n");
  } else {
    printf("Not valid\n");
  }
}

/*
 * Each line on input file should be the following format:
 * ROW COL1 VAL1 COL2 VAL2 ...
 */
void lil_matrix_set_from_input_file(LIL_MATRIX *mat, FILE *fp)
{
  int max_line_size, line_size, i, row_num, num;
  char *line;
  gchar **tokens;
  GArray *ary;
  lil_matrix_init_buffer_to_read(&max_line_size, &line, sizeof(char));
  while (fileread_gets_line(&line, &line_size, &max_line_size, fp)) {
    if (line[0] != '#') {
      ary = g_array_new(FALSE, FALSE, sizeof(int));
      row_num = -1;
      tokens = g_strsplit(line, " ", -1);
      i = 0;
      while (tokens[i]) {
        if (strlen(tokens[i]) > 0) {
          num = atoi(tokens[i]);
          if (row_num < 0) {
            row_num = num;
          } else {
            g_array_append_val(ary, num);
          }
        }
        i += 1;
      }
      g_strfreev(tokens);
      if (row_num >= 0) {
        lil_matrix_row_set(mat, row_num, ary->len / 2, (int *) ary->data);
      }
      g_array_free(ary, FALSE);
    }
  }
  free(line);
}

static int lil_matrix_reduce_following_rows_by_row_operations (LIL_MATRIX *mat, LIL_SNF_ARG *arg, int row_num_start)
{
  int i;
  GPtrArray *pivot_array;
  int el_row_num, row_num_next, count_empty_rows;
  LIL_MATRIX_PIVOT *pivot;
  mpz_t min_abs_val;
  mpz_init(min_abs_val);
  lil_matrix_update_cache(mat, arg, min_abs_val);
  pivot_array = lil_matrix_minimal_non_zero_element(&el_row_num, &count_empty_rows, mat, row_num_start, mat->row_size - 1, min_abs_val, arg, NULL);
  mpz_clear(min_abs_val);
  if (!pivot_array) {
    row_num_next = -1;
  } else {
    pivot = (LIL_MATRIX_PIVOT *) g_ptr_array_index(pivot_array, 0);
    if (el_row_num > 2 || mpz_cmp_ui(pivot->abs, 1) != 0) {
      row_num_next = -1;
    } else {
      if (arg->debug) {
        fprintf(stderr, "Pivots: %d\n", pivot_array->len);
        for (i = 0; i < pivot_array->len; i++) {
          pivot = g_ptr_array_index(pivot_array, i);
          gmp_fprintf(stderr, "  Pivot: %d %d, Value: %Zd, Column elements: %d\n", pivot->row, pivot->column, pivot->abs, el_row_num);
        }
      }
      if (arg->cache->type == CACHE_BOTH && !lil_snf_argument_cache_column_index_updated_p(arg)) {
        fprintf(stderr, "Not updated column index cache; row_changed=%d, row_recreated=%d\n",
                arg->cache->row_changed->len, arg->cache->row_recreated->len);
        abort_with_line_number();
      }
      for (i = 0; i < pivot_array->len; i++) {
        pivot = g_ptr_array_index(pivot_array, i);
        lil_snf_argument_remove_row_cache(arg, pivot->row);
      }
      if (!lil_matrix_snf_partially_row_reduce(mat, pivot_array, arg, row_num_start, mat->row_size - 1)) {
        fprintf(stderr, "Invalid case: number at the pivot must have absolute value 1.");
        abort_with_line_number();
      }
      arg->stat->elementary_divisors += pivot_array->len;
      for (i = 0; i < pivot_array->len; i++) {
        pivot = g_ptr_array_index(pivot_array, i);
        if (pivot->row != row_num_start) {
          lil_matrix_row_exchange(mat, row_num_start, pivot->row);
        }
        row_num_start += 1;
      }
      row_num_next = row_num_start;
    }
    lil_matrix_snf_pivot_array_free(pivot_array, -1);
  }
  return row_num_next;
}

static void lil_matrix_reduce_by_row_operations_pivot_two_elements (LIL_MATRIX *mat, LIL_SNF_ARG *arg)
{
  int row_num;
  row_num = 0;
  while (mat->column_size > 0 && mat->row_size > 0) {
    arg->stat->step += 1;
    row_num = lil_matrix_reduce_following_rows_by_row_operations(mat, arg, row_num);
    if (row_num < 0) {
      break;
    }
    if (arg->info > 0 && arg->stat->elementary_divisors - arg->stat->last_output >= arg->info) {
      printf("row_num=%d\n", row_num);
      lil_matrix_snf_output_info("Calculation", arg->stat, mat, NULL);
      arg->stat->last_output = arg->stat->elementary_divisors;
    }
    lil_matrix_snf_validation(mat, arg);
  }
}

static void lil_matrix_reduce_by_row_operations_of_inclusion (LIL_MATRIX *mat, LIL_SNF_ARG *arg)
{
  lil_matrix_snf_reduction_by_inclusion_repeatedly(mat, arg, NULL);
  lil_matrix_snf_delete_empty_rows_and_cache(mat, arg);
}

void lil_matrix_reduce_by_row_operations(LIL_MATRIX *mat, LIL_SNF_ARG *arg)
{
  mpz_t count, count_last;
  mpz_init(count);
  mpz_init(count_last);
  lil_snf_argument_cache_init(arg, CACHE_BOTH, mat);
  arg->reduce_by_divisors = 0;
  lil_matrix_number_of_non_zero_elements(mat, count);
  if (arg->info > 0) {
    lil_matrix_snf_output_info("Matrix", NULL, mat, NULL);
  }

  do {
    lil_matrix_snf_validation(mat, arg);
    lil_matrix_reduce_by_row_operations_pivot_two_elements(mat, arg);
    if (arg->info > 0) {
      lil_matrix_snf_output_info("Finish reducing by row operations", arg->stat, mat, NULL);
    }
    lil_matrix_reduce_by_row_operations_of_inclusion(mat, arg);
    if (arg->info > 0) {
      lil_matrix_snf_output_info("Deleted elements of a row including other", arg->stat, mat, NULL);
    }
    mpz_set(count_last, count);
    lil_matrix_number_of_non_zero_elements(mat, count);
  } while (mpz_cmp(count, count_last) < 0);

  lil_matrix_row_element_first_positive(mat);
  mpz_clear(count);
  mpz_clear(count_last);
  lil_snf_argument_cache_clear(arg);
}

void lil_matrix_print_stat(LIL_MATRIX *mat)
{
  lil_matrix_snf_output_info("Statistics", NULL, mat, NULL);
}
