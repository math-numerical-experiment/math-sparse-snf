#include <argtable2.h>
#include "lilmatrix_mpi.h"

int main(int argc, char *argv[])
{
  MPI_NODE_INFO mpi_node_info[1];
  LIL_MATRIX *mat;
  LIL_SNF_ARG *snf_arg;
  const char *progname = "sparse_snf_mpi";

  struct arg_lit *help = arg_lit0("h", "help", "Print this help and exit");
  struct arg_int *info_number = arg_int0("i", "info", "<num>", "Set the frequency of infomation output");
  struct arg_lit *precise_estimate = arg_lit0("c", "precise-estimate", "Estimate increase of elements precisely");
  struct arg_int *reduce_by_divisors = arg_int0("d", "reduce-by-divisors", "<num>", "Reduce elements by obtaining divisors");
  struct arg_int *reduce_at_increase = arg_int0("I", "reduce-by-increase", "<num>", "Reduce elements by increase of elements");
  struct arg_lit *auto_delete_duplicates = arg_lit0("D", "auto-reduction", "Reduce elements automatically");
  struct arg_lit *reduce_repeatedly = arg_lit0("r", "reduce-repeatedly", "Reduce elements repeatedly");
  struct arg_int *pivot_number = arg_int0("P", "min-pivot-number", "<num>", "Minimum pivot number of calculation");
  struct arg_int *estimate_increase = arg_int0("E", "max-elements-increase", "<num>", "Maximum number of estimation of elements");
  struct arg_lit *load_directory = arg_lit0("L", "load-directory", "Load directory");
  struct arg_file *input_file = arg_file0(NULL, NULL, "<file>", "Input file. Input directory if load option is on");
  struct arg_file *column_ops_save = arg_file0("C", "column", "<output>", "Save column operations to the file");
  struct arg_str *reduce_by_row_operations = arg_str0("R", "reduce-by-row-operations", "<output>",
                                                      "Reduce elements of matrix by only row operations");
  struct arg_dbl *memory_reserve = arg_dbl0("M", "memory-reserve", "<GB>", "Memory size of reserve");
  struct arg_int *validate = arg_int0("V", "validate", "<num>", "Validate matrix");
  struct arg_lit *test = arg_lit0(NULL, "test", "Test MPI version");
  struct arg_lit *debug = arg_lit0(NULL, "debug", "Debug mode");

  struct arg_end *end = arg_end(20);
  void* argtable[] = {
    info_number, precise_estimate, reduce_by_divisors, reduce_at_increase, auto_delete_duplicates, reduce_repeatedly,
    pivot_number, estimate_increase, load_directory, input_file,
    column_ops_save, reduce_by_row_operations, memory_reserve, validate, test, debug, help, end
  };
  int nerrors;
  GArray *snf;
  GString *path;
  bool test_mpi;
  path = NULL;
  test_mpi = false;

  MPI_Init(&argc, &argv);
  mpi_node_info_set(mpi_node_info);

  nerrors = arg_parse(argc, argv, argtable);

  if (memory_reserve->count > 0) {
    mpi_node_info->memory_reserve = memory_reserve->dval[0] * 1024.0;
  }
  if (test->count > 0) {
    test_mpi = true;
  }

  if (help->count > 0) {
    printf("Usage: %s", progname);
    arg_print_syntax(stdout, argtable,"\n");
    printf("Display points in files.\n\n");
    arg_print_glossary(stdout, argtable," %-30s %s\n");
    printf("\nReport bugs to <d@ytak.info>.\n");
    exit(0);
  }

  if (nerrors > 0){
    /* Display the error details contained in the arg_end struct.*/
    arg_print_errors(stdout, end, progname);
    printf("Try '%s --help' for more information.\n", progname);
    exit(1);
  }

  lil_matrix_thread_pool_init(mpi_node_info->cpu * 2);

  snf_arg = lil_snf_argument_new();
  snf_arg->signal_use_p = true;
  if (estimate_increase->count > 0) {
    snf_arg->max_estimate_increase = estimate_increase->ival[0];
  }
  if (pivot_number->count > 0) {
    snf_arg->min_pivot_number = pivot_number->ival[0];
  }
  if (column_ops_save->count > 0) {
    snf_arg->column_ops_output = (char *) column_ops_save->filename[0];
  }
  if (info_number->count > 0) {
    snf_arg->info = info_number->ival[0];
  }
  if (validate->count > 0) {
    snf_arg->validate = validate->ival[0];
  }
  if (reduce_by_divisors->count > 0) {
    snf_arg->reduce_by_divisors = reduce_by_divisors->ival[0];
  }
  if (reduce_at_increase->count > 0) {
    snf_arg->reduce_by_increase = reduce_at_increase->ival[0];
  }
  if (auto_delete_duplicates->count > 0) {
    snf_arg->auto_delete_duplicates = true;
  }
  if (reduce_repeatedly->count > 0) {
    snf_arg->reduce_repeatedly = true;
  }
  if (debug->count > 0) {
    snf_arg->debug = true;
  }
  if (precise_estimate->count > 0) {
    snf_arg->precise_estimate = true;
  }

  if (load_directory->count > 0) {
    snf_arg->load_directory = g_strdup(input_file->filename[0]);
    path = g_string_sized_new(100);
    g_string_printf(path, "%s/mat.txt", snf_arg->load_directory);
    mat = lil_matrix_mpi_split_load_file(mpi_node_info, path->str);
  } else if (input_file->count > 0) {
    path = g_string_new(input_file->filename[0]);
    mat = lil_matrix_mpi_split_load_file(mpi_node_info, path->str);
  } else {
    printf("The matrix is not specified.\n");
    return EXIT_SUCCESS;
  }

  arg_freetable(argtable,sizeof(argtable) / sizeof(argtable[0]));

  if (test_mpi) {
    snf = lil_matrix_mpi_snf_test(mat, path->str, snf_arg, mpi_node_info);
  } else {
    snf = lil_matrix_mpi_snf(mat, snf_arg, mpi_node_info);
  }
  if (snf) {
    lil_matrix_snf_print(snf);
    lil_matrix_snf_array_free(snf);
  }
  lil_snf_argument_free(snf_arg);
  lil_matrix_clear(mat);
  if (path) {
    g_string_free(path, TRUE);
  }
  mpi_node_info_clear(mpi_node_info);
  MPI_Finalize();
  return EXIT_SUCCESS;
}
