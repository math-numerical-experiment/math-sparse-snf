#include "lilmatrix_mpi.h"
#include "../intern.h"

#define MAX_DUMP_DATA_SIZE 1024 * 1024 * 10

static void lil_matrix_mpi_bcast_bool_from_root (bool *val);
static void lil_matrix_mpi_bcast_string_from_root (MPI_NODE_INFO *mpi_node_info, GString **string);

static double memory_size_get ()
{
  struct sysinfo info;
  sysinfo(&info);
  return info.totalram / 1024.0 / 1024.0;
}

static int cpu_number_get ()
{
  int num;
  char *out;
  g_spawn_command_line_sync(COMMAND_CPU_NUMBER, &out, NULL, NULL, NULL);
  num = atoi(out);
  g_free(out);
  return num;
}

static void hostname_get (char **out)
{
  g_spawn_command_line_sync(COMMAND_HOSTNAME, out, NULL, NULL, NULL);
}

void mpi_node_info_set (MPI_NODE_INFO *mpi_node_info)
{
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_node_info->rank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_node_info->number_nodes);
  mpi_node_info->cpu = cpu_number_get();
  mpi_node_info->memory = memory_size_get();
  mpi_node_info->memory_reserve = -1.0;
  hostname_get(&mpi_node_info->hostname);
  mpi_node_info->row_start = -1;
  mpi_node_info->row_end = -1;
  mpi_node_info->row_end_numbers = NULL;
}

void mpi_node_info_clear (MPI_NODE_INFO *mpi_node_info)
{
  g_free(mpi_node_info->hostname);
  if (mpi_node_info->row_end_numbers) {
    g_array_free(mpi_node_info->row_end_numbers, TRUE);
  }
}

static GArray *mpi_nodes_memory_get (MPI_NODE_INFO *mpi_node_info, double *total_memory)
{
  GArray *memory_size;
  double *memory_size_data;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    memory_size = (GArray *) g_array_sized_new(false, false, sizeof(double), mpi_node_info->number_nodes);
    memory_size->len = mpi_node_info->number_nodes;
    memory_size_data = (double *) memory_size->data;
  } else {
    memory_size = NULL;
    memory_size_data = NULL;
    *total_memory = 0.0;
  }
  MPI_Gather(&mpi_node_info->memory, 1, MPI_DOUBLE, memory_size_data, 1, MPI_DOUBLE, MPI_ROOT_RANK, MPI_COMM_WORLD);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    int i;
    *total_memory = 0.0;
    for (i = 0; i < memory_size->len; i++) {
      *total_memory += g_array_index(memory_size, double, i);
    }
  }
  return memory_size;
}

static void mpi_nodes_number_split (GArray *nums_splitted, int node_num, int num, GArray *memory_size, int total_memory, double memory_reserve) {
  int i, last_ind, n;
  double mem, total_mem;
  if (memory_reserve <= 0.0) {
    memory_reserve = DEFAULT_MEMORY_RESERVE;
  }
  total_mem = total_memory - memory_reserve * node_num;
  last_ind = 0;
  for (i = 0; i < memory_size->len; i++) {
    if (i == (memory_size->len - 1)) {
      last_ind = num;
    } else {
      mem = g_array_index(memory_size, double, i) - memory_reserve;
      if (mem <= 0) {
        fprintf(stderr, "Too small memory: %lf\n", g_array_index(memory_size, double, i));
        abort_with_line_number();
      }
      n = floor(mem / total_mem * num);
      if (n == 0) {
        n += 1;
      }
      last_ind += n;
    }
    if (i > 0) {
      if (g_array_index(nums_splitted, int, i - 1) == last_ind) {
        last_ind += 1;
      }
    }
    if (last_ind > num) {
      last_ind = num;
    }
    g_array_index(nums_splitted, int, i) = last_ind;
  }
}

LIL_MATRIX *lil_matrix_mpi_split_load_file (MPI_NODE_INFO *mpi_node_info, const char *path)
{
  int mat_args[3];
  GArray *memory_size, *nums_splitted;
  double total_memory;
  if (!lil_matrix_text_file_get_matrix_arguments(mat_args, path)) {
    fprintf(stderr, "Invalid matrix data file\n");
    abort_with_line_number();
  }
  memory_size = mpi_nodes_memory_get(mpi_node_info, &total_memory);
  nums_splitted = g_array_sized_new(false, false, sizeof(int), mpi_node_info->number_nodes);
  nums_splitted->len = mpi_node_info->number_nodes;
  if (memory_size) {
    mpi_nodes_number_split(nums_splitted, mpi_node_info->number_nodes, mat_args[0] - 1, memory_size, total_memory, mpi_node_info->memory_reserve);
    g_array_free(memory_size, TRUE);
  }
  MPI_Bcast(nums_splitted->data, nums_splitted->len, MPI_INT, MPI_ROOT_RANK, MPI_COMM_WORLD);
  if (mpi_node_info->rank == 0) {
    mpi_node_info->row_start = 0;
  } else {
    mpi_node_info->row_start = g_array_index(nums_splitted, int, mpi_node_info->rank - 1) + 1;
  }
  mpi_node_info->row_end = g_array_index(nums_splitted, int, mpi_node_info->rank);
  mpi_node_info->row_end_numbers = nums_splitted;
  return lil_matrix_load_file_row_range(path, mpi_node_info->row_start, mpi_node_info->row_end);
}

static void lil_matrix_mpi_bcast_mpz (MPI_NODE_INFO *mpi_node_info, mpz_t val, int rank_from)
{
  char *dump = NULL;
  int len;
  if (mpi_node_info->rank == rank_from) {
    size_t size_exported;
    dump = mpz_export(NULL, &size_exported, 1, 1, 1, 0, val);
    if (mpz_cmp_si(val, 0) < 0) {
      len = -size_exported;
    } else {
      len = size_exported;
    }
  }
  MPI_Bcast(&len, 1, MPI_INT, rank_from, MPI_COMM_WORLD);
  if (len == 0) {
    if (mpi_node_info->rank != rank_from) {
      mpz_set_si(val, 0);
    }
  } else {
    if (mpi_node_info->rank != rank_from) {
      dump = (char *) malloc(sizeof(char) * abs(len));
    }
    MPI_Bcast(dump, abs(len), MPI_CHAR, rank_from, MPI_COMM_WORLD);
    if (mpi_node_info->rank != rank_from) {
      mpz_import(val, abs(len), 1, 1, 1, 0, dump);
      if (len < 0) {
        mpz_neg(val, val);
      }
    }
    free(dump);
  }
}

static void lil_matrix_mpi_bcast_mpz_from_root (MPI_NODE_INFO *mpi_node_info, mpz_t val)
{
  lil_matrix_mpi_bcast_mpz(mpi_node_info, val, MPI_ROOT_RANK);
}

/*
  Returned array should be freed by lil_matrix_mpz_array_free
*/
static GPtrArray *lil_matrix_mpi_gather_mpz_to_root (MPI_NODE_INFO *mpi_node_info, mpz_t val)
{
  char *exported, *exported_all = NULL, *dump = NULL;
  size_t size_exported, *size_all;
  GPtrArray *ary_mpz;
  int i, max_size;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    ary_mpz = g_ptr_array_new();
    size_all = (size_t *) malloc(sizeof(size_t) * mpi_node_info->number_nodes);
  } else {
    ary_mpz = NULL;
  }
  dump = mpz_export(NULL, &size_exported, 1, 1, 1, 0, val);
  MPI_Gather(&size_exported, sizeof(size_t), MPI_CHAR, size_all, sizeof(size_t), MPI_CHAR, MPI_ROOT_RANK, MPI_COMM_WORLD);
  if (MPI_ROOT_RANK == mpi_node_info->rank) {
    max_size = 0;
    for (i = 0; i < mpi_node_info->number_nodes; i++) {
      if (max_size < size_all[i]) {
        max_size = size_all[i];
      }
    }
    exported_all = (char *) malloc(max_size * mpi_node_info->number_nodes * sizeof(char));
  }
  MPI_Bcast(&max_size, 1, MPI_INT, MPI_ROOT_RANK, MPI_COMM_WORLD);
  exported = (char *) malloc(max_size);
  memcpy(exported, dump, size_exported);
  if (size_exported < max_size) {
    memset(exported + size_exported, 0, sizeof(char) * (max_size - size_exported));
  }
  free(dump);
  MPI_Gather(exported, sizeof(char) * max_size, MPI_CHAR, exported_all, sizeof(char) * max_size, MPI_CHAR, MPI_ROOT_RANK, MPI_COMM_WORLD);
  if (MPI_ROOT_RANK == mpi_node_info->rank) {
    for (i = 0; i < mpi_node_info->number_nodes; i++) {
      __mpz_struct *val;
      val = (__mpz_struct *) malloc(sizeof(__mpz_struct));
      mpz_init(val);
      mpz_import(val, size_all[i], 1, 1, 1, 0, (exported_all + sizeof(char) * max_size * i));
      g_ptr_array_add(ary_mpz, val);
    }
    free(size_all);
    free(exported_all);
  }
  free(exported);
  return ary_mpz;
}

static void lil_matrix_mpz_array_free (GPtrArray *ary)
{
  int i;
  __mpz_struct *val;
  for (i = 0; i < ary->len; i++) {
    val = g_ptr_array_index(ary, i);
    mpz_clear(val);
    free(val);
  }
  g_ptr_array_free(ary, TRUE);
}

/* Only root node set sum_count. Other nodes do not access sum_count. */
static GPtrArray *lil_matrix_mpi_number_of_non_zero_elements (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, mpz_t sum_count)
{
  GPtrArray *count_array;
  mpz_t count;
  mpz_init(count);
  lil_matrix_number_of_non_zero_elements(mat, count);
  count_array = lil_matrix_mpi_gather_mpz_to_root(mpi_node_info, count);
  if (count_array) {            /* root node */
    int i;
    __mpz_struct *val;
    mpz_set_si(sum_count, 0);
    for (i = 0; i < mpi_node_info->number_nodes; i++) {
      val = g_ptr_array_index(count_array, i);
      mpz_add(sum_count, sum_count, val);
    }
  }
  mpz_clear(count);
  return count_array;
}

static void lil_matrix_mpi_debug_print_info (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, CalcStat *stat)
{
  mpz_t count, total;
  mpf_t per;
  GPtrArray *count_array;
  if (MPI_ROOT_RANK == mpi_node_info->rank) {
    mpz_init(count);
    mpz_init(total);
    mpf_init2(per, MPF_PREC);
    lil_matrix_mpi_total_number_of_elements(mat, stat, total);
  }
  count_array = lil_matrix_mpi_number_of_non_zero_elements(mpi_node_info, mat, count);
  if (MPI_ROOT_RANK == mpi_node_info->rank) {
    FILE *fp;
    int i;
    __mpz_struct *val;
    lil_matrix_percentage_of_non_zero_elements(mat, stat, total, count, per);
    fp = stdout;
    lil_matrix_debug_fprint_matrix_size(fp, mat, stat);
    gmp_fprintf(fp, " mod %d, %Zd / %Zd, ", mat->modulo, count, total);
    gmp_fprintf(fp, (mpf_cmp_d(per, 0.01) >= 0 ? "%4.2Ff%%\n" : "%4.2lFe%%\n"), per);
    for (i = 0; i < count_array->len; i++) {
      val = g_ptr_array_index(count_array, i);
      gmp_printf("    rank:%d %Zd\n", i, val);
    }
    lil_matrix_mpz_array_free(count_array);
    mpz_clear(count);
    mpz_clear(total);
    mpf_clear(per);
  }
}

static void lil_matrix_mpi_snf_output_info (MPI_NODE_INFO *mpi_node_info, char *str, CalcStat *stat, LIL_MATRIX *mat, GArray *snf)
{
  bool gather_data_p;
  GArray *buf = NULL, *buf_gathered = NULL;
  char *buf_received;
  CalcStat stat_tmp[1];
  if (MPI_ROOT_RANK == mpi_node_info->rank) {
    gather_data_p = (stat ? true : false);
  }
  lil_matrix_mpi_bcast_bool_from_root(&gather_data_p);
  if (gather_data_p) {
    buf = g_array_sized_new(false, false, sizeof(int), 2);
    buf->len = 2;
    g_array_index(buf, int, 0) = stat->empty_rows;
    g_array_index(buf, int, 1) = stat->row_operations;
    if (MPI_ROOT_RANK == mpi_node_info->rank) {
      int len;
      len = 2 * mpi_node_info->number_nodes;
      buf_gathered = g_array_sized_new(false, false, sizeof(int), len);
      buf_gathered->len = len;
      buf_received = buf_gathered->data;
    } else {
      buf_received = NULL;
    }
    MPI_Gather(buf->data, 2, MPI_INT, buf_received, 2, MPI_INT, MPI_ROOT_RANK, MPI_COMM_WORLD);
    g_array_free(buf, TRUE);
  }
  if (MPI_ROOT_RANK == mpi_node_info->rank) {
    if (buf_gathered) {
      int i;
      stat_tmp->step = stat->step;
      stat_tmp->elementary_divisors = stat->elementary_divisors;
      stat_tmp->empty_rows = 0;
      stat_tmp->row_operations = 0;
      for (i = 0; i < mpi_node_info->number_nodes; i++) {
        stat_tmp->empty_rows += g_array_index(buf_gathered, int, 2 * i);
        stat_tmp->row_operations += g_array_index(buf_gathered, int, 2 * i + 1);
      }
      stat_tmp->empty_rows = mat->row_size - (mat->row_size * mpi_node_info->number_nodes - stat_tmp->empty_rows);
      g_array_free(buf_gathered, TRUE);
      lil_matrix_snf_output_info(str, stat_tmp, NULL, snf);
    } else {
      lil_matrix_snf_output_info(str, NULL, NULL, snf);
    }
  }
  if (mat) {
    if (MPI_ROOT_RANK == mpi_node_info->rank) {
      printf("  ");
    }
    lil_matrix_mpi_debug_print_info(mpi_node_info, mat, (stat && snf ? stat_tmp : NULL));
  }
  fflush(stdout);
}

static void lil_matrix_mpi_bcast_bool_from_root (bool *val)
{
  MPI_Bcast(val, sizeof(bool), MPI_CHAR, MPI_ROOT_RANK, MPI_COMM_WORLD);
}

static void lil_matrix_mpi_bcast_int_from_root (int *val)
{
  MPI_Bcast(val, 1, MPI_INT, MPI_ROOT_RANK, MPI_COMM_WORLD);
}

/*
 *array_ptr is NULL except for the variable of rank_from.
 */
static void lil_matrix_mpi_bcast_int_array (MPI_NODE_INFO *mpi_node_info, GArray **array_ptr, int rank_from)
{
  int len;
  if (mpi_node_info->rank == rank_from) {
    len = (*array_ptr)->len;
  }
  MPI_Bcast(&len, 1, MPI_INT, rank_from, MPI_COMM_WORLD);
  if (mpi_node_info->rank != rank_from) {
    *array_ptr = g_array_sized_new(false, false, sizeof(int), len);
    (*array_ptr)->len = len;
  }
  if (len > 0) {
    MPI_Bcast((*array_ptr)->data, len, MPI_INT, rank_from, MPI_COMM_WORLD);
  }
}

/* *string of root is GString and *string of other nodes are NULL */
static void lil_matrix_mpi_bcast_string (MPI_NODE_INFO *mpi_node_info, GString **string, int rank_from)
{
  int len;
  if (mpi_node_info->rank == rank_from) {
    len = (*string)->len;
  }
  MPI_Bcast(&len, 1, MPI_INT, rank_from, MPI_COMM_WORLD);
  if (mpi_node_info->rank != rank_from) {
    *string = g_string_sized_new(len);
    (*string)->len = len;
  }
  if (len > 0) {
    MPI_Bcast((*string)->str, len, MPI_CHAR, rank_from, MPI_COMM_WORLD);
  }
}

static void lil_matrix_mpi_bcast_string_from_root (MPI_NODE_INFO *mpi_node_info, GString **string)
{
  lil_matrix_mpi_bcast_string(mpi_node_info, string, MPI_ROOT_RANK);
}

static bool lil_matrix_mpi_gather_and_operation (MPI_NODE_INFO *mpi_node_info, bool val)
{
  bool ret, *vals = NULL;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    vals = (bool *) malloc(sizeof(bool) * mpi_node_info->number_nodes);
  }
  MPI_Gather(&val, sizeof(bool), MPI_CHAR, vals, sizeof(bool), MPI_CHAR, MPI_ROOT_RANK, MPI_COMM_WORLD);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    int i;
    ret = true;
    for (i = 0; i < mpi_node_info->number_nodes; i++) {
      if (!vals[i]) {
        ret = false;
        break;
      }
    }
  }
  lil_matrix_mpi_bcast_bool_from_root(&ret);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    free(vals);
  }
  return ret;
}

/* static void lil_matrix_mpi_sync_row_changed_recreated (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, LIL_SNF_ARG *arg) */
/* { */
/*   int i; */
/*   GArray *row_changed_ary[mpi_node_info->number_nodes], *row_recreated_ary[mpi_node_info->number_nodes]; */
/*   row_changed_ary[mpi_node_info->rank] = arg->cache->row_changed; */
/*   row_recreated_ary[mpi_node_info->rank] = arg->cache->row_recreated; */
/*   arg->cache->row_changed = g_array_new(false, false, sizeof(int)); */
/*   arg->cache->row_recreated = g_array_new(false, false, sizeof(int)); */
/*   for (i = 0; i < mpi_node_info->number_nodes; i++) { */
/*     lil_matrix_mpi_bcast_int_array(mpi_node_info, &(row_changed_ary[i]), i); */
/*     lil_matrix_mpi_bcast_int_array(mpi_node_info, &(row_recreated_ary[i]), i); */
/*   } */
/*   for (i = 0; i < mpi_node_info->number_nodes; i++) { */
/*     if (row_changed_ary[i]->len > 0) { */
/*       g_array_append_vals(arg->cache->row_changed, row_changed_ary[i]->data, row_changed_ary[i]->len); */
/*     } */
/*     g_array_free(row_changed_ary[i], TRUE); */
/*     if (row_recreated_ary[i]->len > 0) { */
/*       g_array_append_vals(arg->cache->row_recreated, row_recreated_ary[i]->data, row_recreated_ary[i]->len); */
/*     } */
/*     g_array_free(row_recreated_ary[i], TRUE); */
/*   } */
/* } */

static void lil_matrix_mpi_remove_cache_not_in_range (GPtrArray *column_index_cache, int row_num_start, int row_num_end)
{
  int i, ind;
  GArray *row_nums;
  bool find_p;
  for (i = 0; i < column_index_cache->len; i++) {
    row_nums = g_ptr_array_index(column_index_cache, i);
    if (row_nums && row_nums->len > 0) {
      ind = array_find_number(row_nums, row_num_end, &find_p);
      if (ind >= 0) {
        if (find_p) {
          ind += 1;
        }
        if (ind < row_nums->len) {
          g_array_remove_range(row_nums, ind, row_nums->len - ind);
        }
      }
    }
    if (row_nums && row_nums->len > 0) {
      ind = array_find_number(row_nums, row_num_start, &find_p);
      if (ind > 0) {
        g_array_remove_range(row_nums, 0, ind);
      } else if (ind == ARRAY_ALL_LESS) {
        g_array_remove_range(row_nums, 0, row_nums->len);
      }
    }
    /* if (row_nums && row_nums->len > 0) { */
    /*   if (g_array_index(row_nums, int, 0) < row_num_start) { */
    /*     printf("Invalid start row number: %d, row_num_start=%d, column=%d\n", g_array_index(row_nums, int, 0), row_num_start, i); */
    /*     int j; */
    /*     for (j = 0; j < row_nums->len; j++) { */
    /*       printf("%d ", g_array_index(row_nums, int, j)); */
    /*     } */
    /*     printf("\n"); */
    /*   } */
    /* if (g_array_index(row_nums, int, row_nums->len - 1) > row_num_end) { */
    /*   printf("Invalid end row number: %d, row_num_end=%d, column=%d\n", g_array_index(row_nums, int, row_nums->len - 1), row_num_end, i); */
    /*   int j; */
    /*   for (j = 0; j < row_nums->len; j++) { */
    /*     printf("%d ", g_array_index(row_nums, int, j)); */
    /*   } */
    /*   printf("\n"); */
    /* } */
  }
}

static GPtrArray *lil_matrix_mpi_column_index_cache_nodes_merge (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, GPtrArray *column_index_cache)
{
  GPtrArray *merged, *tmp;
  int i, j, k, rank, v, len;
  GArray *dump, *buf, *row_nums;
  dump = g_array_new(false, false, sizeof(int));
  for (i = 0; i < column_index_cache->len; i++) {
    row_nums = g_ptr_array_index(column_index_cache, i);
    if (row_nums && row_nums->len > 0) {
      g_array_append_val(dump, row_nums->len);
      g_array_append_vals(dump, row_nums->data, row_nums->len);
    } else {
      v = 0;
      g_array_append_val(dump, v);
    }
  }
  tmp = g_ptr_array_sized_new(mpi_node_info->number_nodes);
  for (rank = 0; rank < mpi_node_info->number_nodes; rank++) {
    if (mpi_node_info->rank == rank) {
      buf = dump;
    } else {
      buf = NULL;
    }
    lil_matrix_mpi_bcast_int_array(mpi_node_info, &buf, rank);
    g_ptr_array_add(tmp, buf);
  }
  merged = lil_matrix_column_index_cache_new(mat->column_size);
  for (i = 0; i < tmp->len; i++) {
    buf = g_ptr_array_index(tmp, i);
    k = 0;
    for (j = 0; j < merged->len; j++) {
      len = g_array_index(buf, int, k);
      k += 1;
      if (len > 0) {
        row_nums = g_ptr_array_index(merged, j);
        if (!row_nums) {
          g_ptr_array_index(merged, j) = g_array_new(false, false, sizeof(int));
          row_nums = g_ptr_array_index(merged, j);
        }
        g_array_append_vals(row_nums, &g_array_index(buf, int, k), len);
        k += len;
      }
    }
    g_array_free(buf, TRUE);
  }
  g_ptr_array_free(tmp, TRUE);
  return merged;
}

static void lil_matrix_mpi_update_cache (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, LIL_SNF_ARG *arg, mpz_t min_val)
{
  GPtrArray *column_index_cache_node;
  /* lil_matrix_mpi_sync_row_changed_recreated(mpi_node_info, mat, arg); */
  lil_matrix_mpi_remove_cache_not_in_range(arg->cache->column_index, mpi_node_info->row_start, mpi_node_info->row_end);
  lil_matrix_update_cache_row_range(mat, arg, mpi_node_info->row_start, mpi_node_info->row_end, min_val);
  column_index_cache_node = arg->cache->column_index;
  arg->cache->column_index = lil_matrix_mpi_column_index_cache_nodes_merge(mpi_node_info, mat, column_index_cache_node);
  lil_snf_argument_cache_column_index_clear(column_index_cache_node);
  if (min_val) {
    int i;
    GPtrArray *min_val_ary;
    __mpz_struct *val;
    min_val_ary = lil_matrix_mpi_gather_mpz_to_root(mpi_node_info, min_val);
    if (min_val_ary) {
      if (min_val_ary->len == 0) {
        fprintf(stderr, "No minimum absolute value of elements.\n");
        abort_with_line_number();
      }
      for (i = 0; i < mpi_node_info->number_nodes; i++) {
        if (i != MPI_ROOT_RANK) {
          val = g_ptr_array_index(min_val_ary, i);
          if (mpz_cmp_ui(val, 0) > 0 && (mpz_cmp_ui(min_val, 0) == 0 || mpz_cmp(min_val, val) > 0)) {
            mpz_set(min_val, val);
          }
        }
      }
      lil_matrix_mpz_array_free(min_val_ary);
    }
    lil_matrix_mpi_bcast_mpz_from_root(mpi_node_info, min_val);
  }
}

/* Ignore pivot->abs. */
static GArray *lil_matrix_snf_pivot_array_dump_to_array (GPtrArray *pivot_array_of_node)
{
  int i, len;
  GArray *nums;
  LIL_MATRIX_PIVOT *pivot;
  if (pivot_array_of_node && pivot_array_of_node->len > 0) {
    len = pivot_array_of_node->len * 3;
    nums = g_array_sized_new(false, false, sizeof(int), len);
    nums->len = len;
    for (i = 0; i < pivot_array_of_node->len; i++) {
      pivot = g_ptr_array_index(pivot_array_of_node, i);
      g_array_index(nums, int, 3 * i) = pivot->row;
      g_array_index(nums, int, 3 * i + 1) = pivot->column;
      g_array_index(nums, int, 3 * i + 2) = pivot->increase;
    }
  } else {
    nums = g_array_sized_new(false, false, sizeof(int), 0);
  }
  return nums;
}

static GPtrArray *lil_matrix_snf_pivot_array_load_to_array (GArray *nums)
{
  int i;
  GPtrArray *pivot_array;
  LIL_MATRIX_PIVOT *pivot;
  if (nums->len > 0) {
    pivot_array = g_ptr_array_new();
    for (i = 0; i < nums->len; i += 3) {
      pivot = lil_matrix_pivot_new(g_array_index(nums, int, i), g_array_index(nums, int, i + 1), NULL);
      pivot->increase = g_array_index(nums, int, i + 2);
      g_ptr_array_add(pivot_array, pivot);
    }
    return pivot_array;
  } else {
    return NULL;
  }
}

/* pivot_array_of_node is freed in this function. */
static GPtrArray *lil_matrix_mpi_gather_pivot_array (MPI_NODE_INFO *mpi_node_info, LIL_SNF_ARG *arg, GPtrArray *pivot_array_of_node, int el_row_num_node, int *el_row_num)
{
  int i, rank, el_row_num_ary[mpi_node_info->number_nodes * 2], buf[2], el_row_num_min, len;
  GPtrArray *pivot_array, *pivot_array_tmp, *buf_nodes;
  GArray *nums;
  buf[0] = el_row_num_node;
  buf[1] = pivot_array_of_node->len;
  MPI_Gather(buf, 2, MPI_INT, el_row_num_ary, 2, MPI_INT, MPI_ROOT_RANK, MPI_COMM_WORLD);
  el_row_num_min = 0;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    el_row_num_min = 0;
    rank = 0;
    while (rank < mpi_node_info->number_nodes && el_row_num_min == 0) {
      if (el_row_num_ary[rank * 2 + 1] > 0) {
        el_row_num_min = el_row_num_ary[rank * 2];
      }
      rank += 1;
    }
    for (; rank < mpi_node_info->number_nodes; rank++) {
      if (el_row_num_ary[rank * 2 + 1] > 0 && el_row_num_ary[rank * 2] > 0 && el_row_num_min > el_row_num_ary[rank * 2]) {
        el_row_num_min = el_row_num_ary[rank * 2];
      }
    }
  }
  lil_matrix_mpi_bcast_int_from_root(&el_row_num_min);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    int req_ind, index;
    GArray *requests;
    MPI_Status status;
    req_ind = 0;
    requests = g_array_sized_new(false, false, sizeof(MPI_Request), mpi_node_info->number_nodes);
    buf_nodes = g_ptr_array_new();
    for (rank = 0; rank < mpi_node_info->number_nodes; rank++) {
      if (rank != MPI_ROOT_RANK && (el_row_num_min > 2 || el_row_num_min == el_row_num_ary[rank * 2])) {
        len = el_row_num_ary[rank * 2 + 1] * 3;
        nums = g_array_sized_new(false, false, sizeof(int), len);
        nums->len = len;
        g_ptr_array_add(buf_nodes, nums);
        MPI_Irecv(nums->data, nums->len, MPI_INT, rank, rank, MPI_COMM_WORLD, &g_array_index(requests, MPI_Request, req_ind));
        requests->len += 1;
        req_ind += 1;
      }
    }
    if (el_row_num_min <= 2 && el_row_num_min != el_row_num_node) {
      pivot_array_of_node = lil_matrix_snf_pivot_array_free(pivot_array_of_node, -1);
      pivot_array = g_ptr_array_new();
    } else {
      pivot_array = pivot_array_of_node;
      pivot_array_of_node = NULL;
    }
    for (req_ind = 0; req_ind < requests->len; req_ind++) {
      MPI_Waitany(requests->len, (MPI_Request *) requests->data, &index, &status);
      nums = g_ptr_array_index(buf_nodes, index);
      pivot_array_tmp = lil_matrix_snf_pivot_array_load_to_array(nums);
      if (pivot_array_tmp) {
        for (i = 0; i < pivot_array_tmp->len; i++) {
          g_ptr_array_add(pivot_array, g_ptr_array_index(pivot_array_tmp, i));
        }
        g_ptr_array_free(pivot_array_tmp, TRUE);
      }
      g_array_free(nums, TRUE);
    }
    g_ptr_array_free(buf_nodes, TRUE);
    g_array_free(requests, TRUE);
  } else {
    pivot_array = NULL;
    if (el_row_num_min > 2 || el_row_num_min == el_row_num_node) {
      nums = lil_matrix_snf_pivot_array_dump_to_array(pivot_array_of_node);
      MPI_Send(nums->data, nums->len, MPI_INT, MPI_ROOT_RANK, mpi_node_info->rank, MPI_COMM_WORLD);
      g_array_free(nums, TRUE);
    }
    pivot_array_of_node = lil_matrix_snf_pivot_array_free(pivot_array_of_node, -1);
  }
  *el_row_num = el_row_num_min;
  return pivot_array;
}

static void lil_matrix_mpi_bcast_pivot_array (MPI_NODE_INFO *mpi_node_info, GPtrArray **pivot_array_ptr)
{
  GArray *nums = NULL;
  if (MPI_ROOT_RANK == mpi_node_info->rank) {
    nums = lil_matrix_snf_pivot_array_dump_to_array(*pivot_array_ptr);
  }
  lil_matrix_mpi_bcast_int_array(mpi_node_info, &nums, MPI_ROOT_RANK);
  if (MPI_ROOT_RANK != mpi_node_info->rank) {
    *pivot_array_ptr = lil_matrix_snf_pivot_array_load_to_array(nums);
  }
  g_array_free(nums, TRUE);
}

/* Nodes except for root do not set *sum_increase. */
static GPtrArray *lil_matrix_mpi_minimal_non_zero_element (MPI_NODE_INFO *mpi_node_info, int *el_row_num, int *count_empty_rows, LIL_MATRIX *mat, int row_num_start, int row_num_end, mpz_t min_abs_val, LIL_SNF_ARG *arg, int *sum_increase)
{
  GPtrArray *pivot_array, *pivot_array_of_node;
  LIL_MATRIX_PIVOT *pivot;
  int i;
  *el_row_num = 0;
  pivot_array_of_node = lil_matrix_minimal_non_zero_element_candidates(el_row_num, count_empty_rows, mat, row_num_start, row_num_end, min_abs_val, arg);
  if (!pivot_array_of_node) {
    pivot_array_of_node = g_ptr_array_new();
  }
  pivot_array = lil_matrix_mpi_gather_pivot_array(mpi_node_info, arg, pivot_array_of_node, *el_row_num, el_row_num);
  if (pivot_array) {            /* case of MPI_ROOT_NODE */
    for (i = 0; i < pivot_array->len; i++) {
      pivot = g_ptr_array_index(pivot_array, i);
      mpz_set(pivot->abs, min_abs_val);
    }
    lil_matrix_pivot_array_sort(pivot_array);
    pivot_array = lil_matrix_snf_pivot_rows_select(pivot_array, arg, *el_row_num);
    if (sum_increase) {
      *sum_increase = lil_matrix_pivots_sum_increase(pivot_array);
    }
  }
  if (pivot_array && pivot_array->len == 0) {
    g_ptr_array_free(pivot_array, TRUE);
    pivot_array = NULL;
  }
  lil_matrix_mpi_bcast_pivot_array(mpi_node_info, &pivot_array);
  return pivot_array;
}

static void lil_matrix_mpi_row_dump_to_gstring (LIL_MATRIX *mat, int row_num, GSList *row_node, GString *buf)
{
  GSList *el_node;
  int len;
  g_string_append_len(buf, (char *) &row_num, sizeof(int));
  len = g_slist_length(row_node);
  g_string_append_len(buf, (char *) &len, sizeof(int));
  if (mat->use_gmp_p) {
    fprintf(stderr, "Not implemented\n");
    abort_with_line_number();
  } else {
    LIL_MATRIX_ELEMENT *el;
    el_node = g_slist_nth(row_node, 0);
    while (el_node) {
      el = (LIL_MATRIX_ELEMENT *) el_node->data;
      g_string_append_len(buf, (char *) &(el->column), sizeof(int));
      g_string_append_len(buf, (char *) &(el->value), sizeof(int));
      el_node = g_slist_next(el_node);
    }
  }
}

/*
  number of rows,
  row number, size of data, column, value, ...,
  row number, size of data, column, value, ...,
  ...
*/
static GString *lil_matrix_mpi_rows_dump_to_gstring (LIL_MATRIX *mat, GArray *row_nums)
{
  GSList *row_list_node, *row_node;
  GString *buf_current_node;
  int row_num, row_num_next, row_nums_ind;
  buf_current_node = g_string_sized_new(MAX_DUMP_DATA_SIZE); /* Enough length of string */
  row_nums_ind = 0;
  row_num_next = g_array_index(row_nums, int, row_nums_ind);
  row_num = 0;
  row_list_node = g_slist_nth(mat->rows, row_num);
  g_string_append_len(buf_current_node, (char *) &(row_nums->len), sizeof(int));
  while (row_list_node) {
    row_node = (GSList *) row_list_node->data;
    if (row_num_next == row_num) {
      lil_matrix_mpi_row_dump_to_gstring(mat, row_num, row_node, buf_current_node);
      row_nums_ind += 1;
      if (row_nums_ind >= row_nums->len) {
        break;
      }
      row_num_next = g_array_index(row_nums, int, row_nums_ind);
    }
    row_list_node = g_slist_next(row_list_node);
    row_num += 1;
  }
  return buf_current_node;
}

static GString *lil_matrix_mpi_rows_dump_to_gstring2 (LIL_MATRIX *mat, int row_start, int row_end, int *row_end_dumped)
{
  GSList *row_list_node, *row_node;
  GString *buf_current_node;
  int row_num, len;
  len = row_end - row_start + 1;
  buf_current_node = g_string_sized_new(MAX_DUMP_DATA_SIZE); /* Enough length of string */
  row_num = row_start;
  row_list_node = g_slist_nth(mat->rows, row_start);
  g_string_append_len(buf_current_node, (char *) &len, sizeof(int));
  *row_end_dumped = row_end;
  while (row_num <= row_end) {
    row_node = (GSList *) row_list_node->data;
    lil_matrix_mpi_row_dump_to_gstring(mat, row_num, row_node, buf_current_node);
    if (buf_current_node->len > MAX_DUMP_DATA_SIZE) {
      *row_end_dumped = row_num;
      break;
    }
    row_list_node = g_slist_next(row_list_node);
    row_num += 1;
  }
  return buf_current_node;
}

/* buf_size is size of loaded data with type char */
static GSList *lil_matrix_mpi_row_load_gstring_int (int *buf_ptr, int num_els, int *buf_size)
{
  int i;
  GSList *row_node;
  row_node = NULL;
  for (i = 0; i < num_els; i++) {
    row_node = g_slist_append(row_node, lil_matrix_element_new(buf_ptr[i * 2], buf_ptr[i * 2 + 1]));  
  }
  *buf_size = num_els * 2 * sizeof(int);
  return row_node;
}

static int lil_matrix_mpi_rows_load (LIL_MATRIX *mat, char *buf)
{
  int i, num_rows, row_num, row_num_cur, num_els, buf_size;
  GSList *row_list_node;
  num_rows = *((int *) buf);
  buf += sizeof(int);
  row_num_cur = 0;
  row_list_node = g_slist_nth(mat->rows, row_num_cur);
  for (i = 0; i < num_rows; i++) {
    row_num = *((int *) buf);
    if (row_num < 0) {
      return -row_num;
    }
    buf += sizeof(int);
    num_els = *((int *) buf);
    buf += sizeof(int);
    while (row_num_cur < row_num) {
      row_list_node = g_slist_next(row_list_node);
      row_num_cur += 1;
    }
    if (row_list_node->data) {
      fprintf(stderr, "Row %d has been already set: %d elements\n", row_num_cur, g_slist_length(row_list_node->data));
      abort_with_line_number();
    }
    if (mat->use_gmp_p) {
      fprintf(stderr, "Not implemented\n");
      abort_with_line_number();
    } else {
      row_list_node->data = lil_matrix_mpi_row_load_gstring_int((int *) buf, num_els, &buf_size);
      buf += buf_size;
    }
  }
  return 0;
}

/* row_nums must be sorted */
static void lil_matrix_mpi_spread_rows (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, GArray *row_nums)
{
  int i, *buf_size, buf_size_max, row_num, size_next, buf_size_current;
  GArray *rows_current_node;
  GString *buf_current_node;
  char *buf_dump, *buf;
  rows_current_node = g_array_sized_new(false, false, sizeof(int), row_nums->len);
  for (i = 0; i < row_nums->len; i++) {
    row_num = g_array_index(row_nums, int, i);
    if (row_num > mpi_node_info->row_end) {
      break;
    } else if (row_num >= mpi_node_info->row_start) {
      g_array_append_val(rows_current_node, row_num);
    } 
  }
  buf_current_node = lil_matrix_mpi_rows_dump_to_gstring(mat, rows_current_node);
  g_array_free(rows_current_node, TRUE);
  buf_size = (int *) malloc(mpi_node_info->number_nodes * sizeof(int));
  MPI_Allgather(&(buf_current_node->len), 1, MPI_INT, buf_size, 1, MPI_INT, MPI_COMM_WORLD);
  buf_size_max = buf_size[0];
  for (i = 1; i < mpi_node_info->number_nodes; i++) {
    if (buf_size_max < buf_size[i]) {
      buf_size_max = buf_size[i];
    }
  }
  buf_dump = (char* ) malloc(sizeof(char) * buf_size_max * mpi_node_info->number_nodes);
  buf_size_current = buf_current_node->len;
  if (buf_size_current < buf_size_max) {
    g_string_set_size(buf_current_node, buf_size_max);
    memset(buf_current_node->str + buf_size_current, 0, sizeof(char) * (buf_size_max - buf_size_current));
  }
  MPI_Allgather(buf_current_node->str, buf_current_node->len, MPI_CHAR, buf_dump, buf_size_max, MPI_CHAR, MPI_COMM_WORLD);
  buf = buf_dump;
  for (i = 0; i < mpi_node_info->number_nodes; i++) {
    if (i != mpi_node_info->rank) {
      size_next = lil_matrix_mpi_rows_load(mat, buf);
      if (size_next > 0) {
        fprintf(stderr, "Not implemented\n");
        abort_with_line_number();
      }
    }
    buf += buf_size_max;
  }
  g_string_free(buf_current_node, TRUE);
  free(buf_size);
  free(buf_dump);
}

static void lil_matrix_mpi_gather_full_matrix (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat)
{
  int len, len_all[mpi_node_info->number_nodes], rank, row_end_dumped;
  GString *dump = NULL;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    len = 0;
  } else {
    dump = lil_matrix_mpi_rows_dump_to_gstring2(mat, mpi_node_info->row_start, mpi_node_info->row_end, &row_end_dumped);
    len = dump->len;
    len += sizeof(int);
  }
  MPI_Gather(&len, 1, MPI_INT, len_all, 1, MPI_INT, MPI_ROOT_RANK, MPI_COMM_WORLD);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    int size_next;
    MPI_Status status;
    for (rank = 0; rank < mpi_node_info->number_nodes; rank++) {
      if (rank != MPI_ROOT_RANK) {
        size_next = len_all[rank];
        while (size_next != 0) {
          dump = g_string_sized_new(size_next);
          dump->len = size_next;
          MPI_Recv(dump->str, dump->len, MPI_CHAR, rank, rank, MPI_COMM_WORLD, &status);
          size_next = lil_matrix_mpi_rows_load(mat, dump->str);
          g_string_free(dump, TRUE);
        }
      }
    }
  } else {
    while (row_end_dumped != mpi_node_info->row_end) {
      GString *dump2;
      int size_next;
      dump2 = lil_matrix_mpi_rows_dump_to_gstring2(mat, row_end_dumped + 1, mpi_node_info->row_end, &row_end_dumped);
      size_next = -dump2->len - sizeof(int);
      g_string_append_len(dump, (char *) &size_next, sizeof(int));
      MPI_Send(dump->str, dump->len, MPI_CHAR, MPI_ROOT_RANK, mpi_node_info->rank, MPI_COMM_WORLD);
      g_string_free(dump, TRUE);
      dump = dump2;
    }
    MPI_Send(dump->str, dump->len, MPI_CHAR, MPI_ROOT_RANK, mpi_node_info->rank, MPI_COMM_WORLD);
    g_string_free(dump, TRUE);
  }
}

static void lil_matrix_mpi_spread_pivot_rows (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, GPtrArray *pivot_array)
{
  int i;
  GArray *row_nums;
  LIL_MATRIX_PIVOT *pivot;
  row_nums = g_array_sized_new(false, false, sizeof(int), pivot_array->len);
  for (i = 0; i < pivot_array->len; i++) {
    pivot = g_ptr_array_index(pivot_array, i);
    g_array_append_val(row_nums, pivot->row);
  }
  lil_matrix_mpi_spread_rows(mpi_node_info, mat, row_nums);
  g_array_free(row_nums, TRUE);
}

static bool lil_matrix_mpi_snf_partially_row_reduce (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, GPtrArray *pivot_array, LIL_SNF_ARG *arg)
{
  bool ret;
  ret = lil_matrix_snf_partially_row_reduce(mat, pivot_array, arg, mpi_node_info->row_start, mpi_node_info->row_end);
  return lil_matrix_mpi_gather_and_operation(mpi_node_info, ret);
}

static void lil_matrix_mpi_remove_temporary_rows (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat)
{
  int row_num;
  GSList *row_list_node;
  row_num = 0;
  row_list_node = g_slist_nth(mat->rows, row_num);
  while (row_num < mat->row_size) {
    if (row_num < mpi_node_info->row_start || row_num > mpi_node_info->row_end) {
      if (row_list_node->data) {
        lil_matrix_row_data_clear((GSList *) row_list_node->data, mat->use_gmp_p);
        row_list_node->data = NULL;
      }
    }
    row_list_node = g_slist_next(row_list_node);
    row_num += 1;
  }
}

static void lil_matrix_mpi_bcast_rows_from_root (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, GArray *row_nums)
{
  int i, size_next;
  GString *dump = NULL;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    dump = lil_matrix_mpi_rows_dump_to_gstring(mat, row_nums);
  } else {
    int row_num, n;
    GSList *row_list_node;
    row_num = 0;
    row_list_node = g_slist_nth(mat->rows, row_num);
    for (i = 0; i < row_nums->len; i++) {
      n = g_array_index(row_nums, int, i);
      while (row_num < n) {
        row_list_node = g_slist_next(row_list_node);
        row_num += 1;
      }
      if (row_list_node->data) {
        lil_matrix_row_data_clear((GSList *) row_list_node->data, mat->use_gmp_p);
        row_list_node->data = NULL;
      }
    }
  }
  lil_matrix_mpi_bcast_string_from_root(mpi_node_info, &dump);
  if (mpi_node_info->rank != MPI_ROOT_RANK) {
    size_next = lil_matrix_mpi_rows_load(mat, dump->str);
    if (size_next > 0) {
      fprintf(stderr, "Not implemented\n");
      abort_with_line_number();
    }
  }
  g_string_free(dump, TRUE);
}

static void lil_matrix_mpi_bcast_pivot_rows_from_root (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, GPtrArray *pivot_array)
{
  int i;
  LIL_MATRIX_PIVOT *pivot;
  GArray *row_nums;
  row_nums = g_array_sized_new(false, false, sizeof(int), pivot_array->len);
  for (i = 0; i < pivot_array->len; i++) {
    pivot = g_ptr_array_index(pivot_array, i);
    g_array_append_val(row_nums, pivot->row);
  }
  lil_matrix_mpi_bcast_rows_from_root(mpi_node_info, mat, row_nums);
  g_array_free(row_nums, TRUE);
}

/* Change pivot rows of root. Rows of other nodes are not changed. */
static bool lil_matrix_mpi_snf_partially_column_reduce_after_row_reduction(MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, GPtrArray *pivot_array, LIL_MATRIX *column_ops)
{
  bool ret;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    ret = lil_matrix_snf_partially_column_reduce_after_row_reduction(mat, pivot_array, column_ops);
  }
  lil_matrix_mpi_bcast_bool_from_root(&ret);
  lil_matrix_mpi_bcast_pivot_rows_from_root(mpi_node_info, mat, pivot_array);
  return ret;
}

static bool lil_matrix_mpi_snf_divisible_p (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, bool *divisible, int *row, int *col, mpz_t q, int row_pivot, int column_pivot)
{
  int k, rank;
  bool ret_div[2], all_ret_div[mpi_node_info->number_nodes * 2], ret;
  ret_div[0] = lil_matrix_snf_divisible_p(mat, &(ret_div[1]), row, col, q, row_pivot, column_pivot);
  MPI_Allgather(ret_div, sizeof(bool) * 2, MPI_CHAR, all_ret_div, sizeof(bool) * 2, MPI_CHAR, MPI_COMM_WORLD);
  ret = true;
  *divisible = true;
  rank = -1;
  for (k = 0; k < mpi_node_info->number_nodes; k++) {
    if (!all_ret_div[2 * k]) {
      ret = false;
    }
    if (!all_ret_div[2 * k + 1] && *divisible) {
      rank = k;
      *divisible = false;
    }
  }
  if (!(*divisible)) {
    GArray *nums = NULL;
    if (rank < 0) {
      fprintf(stderr, "Invalid result\n");
      abort_with_line_number();
    } else if (mpi_node_info->rank == rank) {
      nums = g_array_sized_new(false, false, sizeof(int), 2);
      g_array_append_val(nums, *row);
      g_array_append_val(nums, *col);
    }
    lil_matrix_mpi_bcast_int_array(mpi_node_info, &nums, rank);
    lil_matrix_mpi_bcast_mpz(mpi_node_info, q, rank);
    if (mpi_node_info->rank != rank) {
      *row = g_array_index(nums, int, 0);
      *col = g_array_index(nums, int, 1);
    }
    g_array_free(nums, TRUE);
  }
  return ret;
}

static void lil_matrix_mpi_test_matrix (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, LIL_MATRIX *mat_full)
{
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    if (mat->row_size != mat_full->row_size || mat->column_size != mat_full->column_size) {
      fprintf(stderr, "Invalid matrix size: %d x %d, %d x %d\n", mat->row_size, mat->column_size, mat_full->row_size, mat_full->column_size);
      abort_with_line_number();
    }
  }
  lil_matrix_mpi_gather_full_matrix(mpi_node_info, mat);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    if (!lil_matrix_equal_p(mat, mat_full)) {
      lil_matrix_dump_to_file(mat, "dump_mat.txt", LIL_MATRIX_DATA_TEXT);
      lil_matrix_dump_to_file(mat_full, "dump_mat_full.txt", LIL_MATRIX_DATA_TEXT);
      fprintf(stderr, "Invalid matrix\n");
      abort_with_line_number();
    }
  }
  lil_matrix_mpi_remove_temporary_rows(mpi_node_info, mat);
}

static void lil_matrix_mpi_test_pivot_array (LIL_MATRIX *mat, LIL_SNF_ARG *arg, mpz_t min_abs_val, GPtrArray *pivot_array, LIL_MATRIX *mat_full, LIL_SNF_ARG *arg_full)
{
  GArray *row_nums, *row_nums2;
  GPtrArray *pivot_array2;
  mpz_t min_abs_val2;
  bool invalid_column_index, invalid_pivot_array;
  int i, j, el_row_num2, count_empty_rows2, sum_increase2;
  LIL_MATRIX_PIVOT *pivot, *pivot2;
  invalid_column_index = false;
  invalid_pivot_array = false;
  if (min_abs_val) {
    mpz_init(min_abs_val2);
  }
  lil_matrix_update_cache(mat_full, arg_full, (min_abs_val ? min_abs_val2 : NULL));
  if (arg->cache->column_index->len != arg_full->cache->column_index->len) {
    fprintf(stderr, "Length of column index cache does not match\n");
    abort_with_line_number();
  }
  if (min_abs_val && mpz_cmp(min_abs_val, min_abs_val2) != 0) {
    gmp_fprintf(stderr, "min_abs_val does not coincide: min_abs_val=%Zd min_abs_val2=%Zd\n", min_abs_val, min_abs_val2);
    abort_with_line_number();
  }
  for (i = 0; i < arg->cache->column_index->len; i++) {
    row_nums = g_ptr_array_index(arg->cache->column_index, i);
    row_nums2 = g_ptr_array_index(arg_full->cache->column_index, i);
    if (row_nums && row_nums2) {
      if (row_nums->len != row_nums2->len) {
        fprintf(stderr, "Invalid number of cache of column %d: row_nums->len=%d, row_nums2->len=%d\n", i, row_nums->len, row_nums2->len);
        invalid_column_index = true;
      }
      for (j = 0; j < row_nums->len; j++) {
        if (g_array_index(row_nums, int, j) != g_array_index(row_nums2, int, j)) {
          fprintf(stderr, "Invalid row number array j=%d g_array_index(row_nums, int, j)=%d g_array_index(row_nums2, int, j)=%d\n", i, g_array_index(row_nums, int, j), g_array_index(row_nums2, int, j));
          invalid_column_index = true;
        }
      }
    } else if (!row_nums && row_nums2) {
      if (row_nums2->len > 0) {
        fprintf(stderr, "Invalid cache of column %d: row_nums=NULL, row_nums2->len=%d\n", i, row_nums2->len);
        invalid_column_index = true;
      }
    } else if (row_nums && !row_nums2) {
      if (row_nums->len > 0) {
        fprintf(stderr, "Invalid cache of column %d: row_nums->len=%d, row_nums2=NULL\n", i, row_nums->len);
        invalid_column_index = true;
      }
    }
  }
  pivot_array2 = lil_matrix_minimal_non_zero_element(&el_row_num2, &count_empty_rows2, mat_full, 0, mat_full->row_size - 1, min_abs_val2, arg_full, &sum_increase2);
  if (pivot_array && pivot_array2 && pivot_array->len != pivot_array2->len) {
    fprintf(stderr, "Number of pivot_array does not coincide: pivot_array->len=%d, pivot_array2->len=%d\n", pivot_array->len, pivot_array2->len);
    invalid_pivot_array = true;
  }
  if (pivot_array && pivot_array2) {
    int min_len;
    min_len = MIN(pivot_array->len, pivot_array2->len);
    for (i = 0; i < min_len; i++) {
      pivot = g_ptr_array_index(pivot_array, i);
      pivot2 = g_ptr_array_index(pivot_array2, i);
      if (pivot->row != pivot2->row || pivot->column != pivot2->column || mpz_cmp(pivot->abs, pivot2->abs) != 0 || pivot->increase != pivot2->increase) {
        gmp_fprintf(stderr, "pivot:  row=%d column=%d abs=%Zd increase=%d\n", pivot->row, pivot->column, pivot->abs, pivot->increase);
        gmp_fprintf(stderr, "pivot2: row=%d column=%d abs=%Zd increase=%d\n", pivot2->row, pivot2->column, pivot2->abs, pivot->increase);
        invalid_pivot_array = true;
      }
    }
    if (min_len < pivot_array->len) {
      for (i = min_len; i < pivot_array->len; i++) {
        pivot = g_ptr_array_index(pivot_array, i);
        gmp_fprintf(stderr, "pivot:  row=%d column=%d abs=%Zd increase=%d\n", pivot->row, pivot->column, pivot->abs, pivot->increase);
      }
    } else if (min_len > pivot_array2->len) {
      for (i = min_len; i < pivot_array2->len; i++) {
        pivot2 = g_ptr_array_index(pivot_array2, i);
        gmp_fprintf(stderr, "pivot2: row=%d column=%d abs=%Zd increase=%d\n", pivot2->row, pivot2->column, pivot2->abs, pivot2->increase);
      }
    }
  }
  if (min_abs_val) {
    mpz_clear(min_abs_val2);
  }
  if (invalid_column_index || invalid_pivot_array) {
    if (invalid_pivot_array) {
      fprintf(stderr, "pivot_array : ");
      for (i = 0; i < pivot_array->len; i++) {
        pivot = g_ptr_array_index(pivot_array, i);
        gmp_fprintf(stderr, "(%d %d %Zd %d) ", pivot->row, pivot->column, pivot->abs, pivot->increase);
      }
      fprintf(stderr, "\n");
      fprintf(stderr, "pivot_array2: ");
      for (i = 0; i < pivot_array2->len; i++) {
        pivot = g_ptr_array_index(pivot_array2, i);
        gmp_fprintf(stderr, "(%d %d %Zd %d) ", pivot->row, pivot->column, pivot->abs, pivot->increase);
      }
      fprintf(stderr, "\n");
    }
    abort_with_line_number();
  }
}

static void lil_matrix_mpi_test_divisible (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *pivot_array, bool divisible, int col, int row, mpz_t q, LIL_MATRIX *mat_full, LIL_SNF_ARG *arg_full)
{
  LIL_MATRIX_PIVOT *pivot;
  int row2, col2;
  bool divisible2;
  mpz_t q2;
  mpz_init(q2);
  pivot = g_ptr_array_index(pivot_array, 0);
  lil_matrix_snf_partially_column_reduce_after_row_reduction(mat_full, pivot_array, NULL);
  if (!lil_matrix_snf_divisible_p(mat_full, &divisible2, &row2, &col2, q2, pivot->row, pivot->column)) {
    fprintf(stderr, "Invalid full matrix. (%d, %d) element is zero.\n", pivot->row, pivot->column);
    abort_with_line_number();
  }
  if ((divisible && !divisible2) || (divisible && !divisible2)) {
    fprintf(stderr, "Invalid divisible: %s %s\n", (divisible ? "true" : "false"), (divisible2 ? "true" : "false"));
  }
  if (!divisible) {
    if (mpz_cmp(q, q2) != 0) {
      gmp_fprintf(stderr, "Invalid value of remainder: %Zd %Zd\n", q, q2);
      abort_with_line_number();
    } else if (col != col2 || row != row2) {
      fprintf(stderr, "Invalid value of index: row=%d col=%d row2=%d col2=%d\n", row, col, row2, col2);
      abort_with_line_number();
    }
  }
  mpz_clear(q2);
}

static GPtrArray *lil_matrix_mpi_snf_partially (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, LIL_SNF_ARG *arg, LIL_MATRIX *column_ops, GArray *column_pivots_seq, int *count_empty_rows, int *sum_increase, LIL_MATRIX *mat_full, bool exec_test)
{
  int i, el_row_num, loop_count, row, col;
  bool divisible;
  mpz_t q, min_abs_val;
  GPtrArray *pivot_array, *snf_vals;
  LIL_MATRIX_PIVOT *pivot;
  LIL_SNF_ARG *arg_full;
  mpz_init(min_abs_val);
  pivot_array = NULL;
  pivot = NULL;
  arg_full = NULL;
  mpz_init(q);
  for (loop_count = 0; ; loop_count++) {
    if (exec_test) {
      lil_matrix_mpi_test_matrix(mpi_node_info, mat, mat_full);
      if (mpi_node_info->rank == MPI_ROOT_RANK) {
        fprintf(stderr, "[Test loop_count=%d] lil_matrix_mpi_test_matrix passed\n", loop_count);
      }
    }
    if (pivot_array) {
      pivot_array = lil_matrix_snf_pivot_array_free(pivot_array, -1);
    }
    lil_matrix_mpi_update_cache(mpi_node_info, mat, arg, min_abs_val);
    pivot_array = lil_matrix_mpi_minimal_non_zero_element(mpi_node_info, &el_row_num, count_empty_rows, mat, 0, mat->row_size - 1, min_abs_val, arg, sum_increase);
    if (exec_test && mpi_node_info->rank == MPI_ROOT_RANK) {
      if (!arg_full) {
        arg_full = lil_snf_argument_new();
        arg_full->max_estimate_increase = arg->max_estimate_increase;
        arg_full->min_pivot_number = arg->min_pivot_number;
        arg_full->precise_estimate = arg->precise_estimate;
        arg_full->reduce_repeatedly = arg->reduce_repeatedly;
        lil_snf_argument_cache_init(arg_full, CACHE_BOTH, mat_full);
      }
      lil_matrix_mpi_test_pivot_array(mat, arg, min_abs_val, pivot_array, mat_full, arg_full);
      fprintf(stderr, "[Test loop_count=%d] lil_matrix_mpi_test_pivot_array passed\n", loop_count);
    }
    if (!pivot_array) {
      if (arg->debug && mpi_node_info->rank == MPI_ROOT_RANK) {
        fprintf(stderr, "[Loop %d] Pivots: empty\n", loop_count);
      }
      break;
    }
    if (mpi_node_info->rank == MPI_ROOT_RANK) {
      arg->stat->increasing = (el_row_num > 2);
    }
    lil_matrix_mpi_bcast_bool_from_root(&arg->stat->increasing);
    if (arg->debug && mpi_node_info->rank == MPI_ROOT_RANK) {
      fprintf(stderr, "[Loop %d] Pivots: %d\n", loop_count, pivot_array->len);
      for (i = 0; i < pivot_array->len; i++) {
        pivot = g_ptr_array_index(pivot_array, i);
        gmp_fprintf(stderr, "  Pivot: %d %d, Value: %Zd, Column elements: %d\n", pivot->row, pivot->column, pivot->abs, el_row_num);
      }
    }
    if (arg->cache->type == CACHE_BOTH && !lil_snf_argument_cache_column_index_updated_p(arg)) {
      fprintf(stderr, "Not updated column index cache; row_changed=%d, row_recreated=%d\n",
              arg->cache->row_changed->len, arg->cache->row_recreated->len);
      abort_with_line_number();
    }
    lil_matrix_mpi_spread_pivot_rows(mpi_node_info, mat, pivot_array);
    for (i = 0; i < pivot_array->len; i++) {
      pivot = g_ptr_array_index(pivot_array, i);
      lil_snf_argument_remove_row_cache(arg, pivot->row);
    }
    if (!lil_matrix_mpi_snf_partially_row_reduce(mpi_node_info, mat, pivot_array, arg)) {
      lil_matrix_mpi_remove_temporary_rows(mpi_node_info, mat);
      if (exec_test && mpi_node_info->rank == MPI_ROOT_RANK) {
        lil_matrix_snf_partially_row_reduce(mat_full, pivot_array, arg_full, 0, mat_full->row_size - 1);
      }
      continue;
    }
    if (exec_test && mpi_node_info->rank == MPI_ROOT_RANK) {
      for (i = 0; i < pivot_array->len; i++) {
        pivot = g_ptr_array_index(pivot_array, i);
        lil_snf_argument_remove_row_cache(arg_full, pivot->row);
      }
      if (!lil_matrix_snf_partially_row_reduce(mat_full, pivot_array, arg_full, 0, mat_full->row_size - 1)) {
        fprintf(stderr, "Returned value of lil_matrix_snf_partially_row_reduce does not match\n");
        abort_with_line_number();
      }
    }
    if (!lil_matrix_mpi_snf_partially_column_reduce_after_row_reduction(mpi_node_info, mat, pivot_array, column_ops)) {
      lil_matrix_mpi_remove_temporary_rows(mpi_node_info, mat);
      if (exec_test && mpi_node_info->rank == MPI_ROOT_RANK) {
        lil_matrix_snf_partially_column_reduce_after_row_reduction(mat_full, pivot_array, NULL);
      }
      continue;
    }
    /* Check only remainder of first element of pivot_array because all pivots have same absolute values */
    pivot = g_ptr_array_index(pivot_array, 0);
    if (!lil_matrix_mpi_snf_divisible_p(mpi_node_info, mat, &divisible, &row, &col, q, pivot->row, pivot->column)) {
      fprintf(stderr, "Invalid matrix. (%d, %d) element is zero.\n", pivot->row, pivot->column);
      abort_with_line_number();
    }
    if (exec_test && mpi_node_info->rank == MPI_ROOT_RANK) {
      lil_matrix_mpi_test_divisible(mat, arg, pivot_array, divisible, col, row, q, mat_full, arg_full);
      fprintf(stderr, "[Test loop_count=%d] lil_matrix_mpi_test_divisible passed\n", loop_count);
    }
    if (!divisible) {
      GArray *row_nums;
      row_nums = g_array_sized_new(false, false, sizeof(int), TRUE);
      g_array_append_val(row_nums, row);
      lil_matrix_mpi_spread_rows(mpi_node_info, mat, row_nums);
      g_array_free(row_nums, TRUE);
      lil_matrix_add_rows(mat, pivot->row, row, 1);
      mpz_neg(q, q);
      lil_matrix_add_columns_mpz(mat, col, pivot->column, q);
      if (column_ops) {
        lil_matrix_add_rows_mpz(column_ops, col, pivot->column, q);
      }
      lil_snf_argument_cache_reset(arg, CACHE_NOT_CHANGED, mat);
      lil_matrix_mpi_remove_temporary_rows(mpi_node_info, mat);
      /* After taking elementary divisors, we remove temporary rows. */
      if (exec_test && mpi_node_info->rank == MPI_ROOT_RANK) {
        lil_matrix_add_rows(mat_full, pivot->row, row, 1);
        lil_matrix_add_columns_mpz(mat_full, col, pivot->column, q);
        lil_snf_argument_cache_reset(arg_full, CACHE_NOT_CHANGED, mat_full);
      }
    } else {
      break;
    }
  }
  mpz_clear(q);
  mpz_clear(min_abs_val);
  if (pivot_array) {
    if (mpi_node_info->rank == MPI_ROOT_RANK) {
      if (column_pivots_seq) {
        lil_matrix_snf_save_column_numbers(column_pivots_seq, pivot_array);
      }
      snf_vals = lil_matrix_snf_take_elementary_divisors(mat, pivot_array);
      if (arg->cache->type == CACHE_BOTH && arg->validate > 0) {
        lil_matrix_snf_validate_snf_values(mat, pivot_array);
      }
    } else {
      snf_vals = NULL;
    }
    if (exec_test && mpi_node_info->rank == MPI_ROOT_RANK) {
      lil_matrix_snf_make_pivot_rows_empty(mat_full, arg_full, pivot_array);
    }
    lil_matrix_snf_make_pivot_rows_empty(mat, arg, pivot_array);
    pivot_array = lil_matrix_snf_pivot_array_free(pivot_array, -1);
  } else {
    snf_vals = NULL;
  }
  lil_matrix_mpi_remove_temporary_rows(mpi_node_info, mat);
  if (arg_full) {
    lil_snf_argument_cache_clear(arg_full);
    lil_snf_argument_free(arg_full);
  }
  return snf_vals;
}

static void lil_matrix_mpi_snf_validation (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, LIL_SNF_ARG *arg)
{
  bool test_p;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    test_p = (arg->validate > 0 && arg->stat->elementary_divisors - arg->stat->last_validating > arg->validate);
  }
  lil_matrix_mpi_bcast_bool_from_root(&test_p);
  if (test_p) {
    GString *str;
    lil_matrix_mpi_bcast_int_from_root(&(arg->stat->step));
    str = g_string_sized_new(100);
    g_string_printf(str, "rank %d", mpi_node_info->rank);
    lil_matrix_snf_output_validate(mat, arg, str->str);
    g_string_free(str, TRUE);
    if (arg->cache) {
      lil_matrix_snf_cache_validate(mat, arg);
    }
    if (mpi_node_info->rank == MPI_ROOT_RANK) {
      arg->stat->last_validating = arg->stat->elementary_divisors;
    }
  }
}

static GString *lil_matrix_mpi_ptr_array_of_int_array_dump (GPtrArray *inclusion_index)
{
  GString *dump;
  GArray *nums;
  int i;
  dump = g_string_sized_new(inclusion_index->len * 100);
  g_string_append_len(dump, (char *) &(inclusion_index->len), sizeof(gsize));
  for (i = 0; i < inclusion_index->len; i++) {
    nums = g_ptr_array_index(inclusion_index, i);
    g_string_append_len(dump, (char *) &(nums->len), sizeof(gsize));
    g_string_append_len(dump, nums->data, sizeof(int) * nums->len);
  }
  return dump;
}

static GPtrArray *lil_matrix_mpi_ptr_array_of_int_array_load (GString *dump)
{
  GPtrArray *inclusion_index;
  GArray *nums;
  int i, len, len_inclusion;
  char *ptr;
  ptr = dump->str;
  len_inclusion = (int) (*((gsize *) ptr));
  ptr += sizeof(gsize);
  inclusion_index = g_ptr_array_sized_new(len_inclusion);
  for (i = 0 ; i < len_inclusion; i++) {
    len = (int) (*((gsize *) ptr));
    ptr += sizeof(gsize);
    nums = g_array_sized_new(false, false, sizeof(int), len);
    g_array_append_vals(nums, ptr, len);
    ptr += sizeof(int) * len;
    g_ptr_array_add(inclusion_index, nums);
  }
  return inclusion_index;
}

/* Return NULL on nodes except for root. */
static GPtrArray *lil_matrix_mpi_snf_create_inclusion_index_multithread (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray *target_rows)
{
  GPtrArray *inclusion_index_tmp, *inclusion_index;
  GString *buf_current_node;
  gsize len_all[mpi_node_info->number_nodes], len;
  inclusion_index_tmp = g_ptr_array_new();
  lil_matrix_snf_create_inclusion_index_multithread_for_range(mat, arg, inclusion_index_tmp, target_rows, mpi_node_info->row_start, mpi_node_info->row_end);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    len = 0;
    inclusion_index = inclusion_index_tmp;
    inclusion_index_tmp = NULL;
    buf_current_node = NULL;
  } else {
    buf_current_node = lil_matrix_mpi_ptr_array_of_int_array_dump(inclusion_index_tmp);
    len = buf_current_node->len;
    lil_matrix_snf_inclusion_index_clear(inclusion_index_tmp, true);
    inclusion_index = NULL;
    inclusion_index_tmp = NULL;
  }
  MPI_Gather(&len, sizeof(gsize), MPI_CHAR, len_all, sizeof(gsize), MPI_CHAR, MPI_ROOT_RANK, MPI_COMM_WORLD);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    int req_ind, rank, index, i;
    MPI_Status status;
    GArray *requests, *nums;
    GPtrArray *buf_nodes;
    GString *dump;
    req_ind = 0;
    requests = g_array_sized_new(false, false, sizeof(MPI_Request), mpi_node_info->number_nodes);
    buf_nodes = g_ptr_array_new();
    for (rank = 0; rank < mpi_node_info->number_nodes; rank++) {
      if (rank != MPI_ROOT_RANK) {
        len = len_all[rank];
        dump = g_string_sized_new(len);
        dump->len = len;
        g_ptr_array_add(buf_nodes, dump);
        MPI_Irecv(dump->str, len, MPI_CHAR, rank, rank, MPI_COMM_WORLD, &g_array_index(requests, MPI_Request, req_ind));
        requests->len += 1;
        req_ind += 1;
      }
    }
    for (req_ind = 0; req_ind < requests->len; req_ind++) {
      MPI_Waitany(requests->len, (MPI_Request *) requests->data, &index, &status);
      dump = g_ptr_array_index(buf_nodes, index);
      inclusion_index_tmp = lil_matrix_mpi_ptr_array_of_int_array_load(dump);
      for (i = 0; i < inclusion_index_tmp->len; i++) {
        nums = g_ptr_array_index(inclusion_index_tmp, i);
        g_ptr_array_add(inclusion_index, nums);
      }
      g_ptr_array_free(inclusion_index_tmp, true);
      g_string_free(dump, TRUE);
    }
    g_ptr_array_free(buf_nodes, TRUE);
    g_array_free(requests, TRUE);
  } else {
    MPI_Send(buf_current_node->str, len, MPI_CHAR, MPI_ROOT_RANK, mpi_node_info->rank, MPI_COMM_WORLD);
    g_string_free(buf_current_node, TRUE);
  }
  return inclusion_index;
}

static void lil_matrix_mpi_snf_reduction_by_inclusion_step (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *inclusion_index)
{
  GArray *pivot_row_nums, *nums;
  GPtrArray *row_nums_splitted, *pivot_splitted, *rows_target;
  GString *dump = NULL;
  int i, num;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    lil_matrix_snf_split_inclusion_index(mat, inclusion_index, &pivot_row_nums, &pivot_splitted, &row_nums_splitted);
    dump = lil_matrix_mpi_ptr_array_of_int_array_dump(row_nums_splitted);
  }
  lil_matrix_mpi_bcast_int_array(mpi_node_info, &pivot_row_nums, MPI_ROOT_RANK);
  lil_matrix_mpi_spread_rows(mpi_node_info, mat, pivot_row_nums);
  lil_matrix_mpi_bcast_string_from_root(mpi_node_info, &dump);
  if (mpi_node_info->rank != MPI_ROOT_RANK) {
    row_nums_splitted = lil_matrix_mpi_ptr_array_of_int_array_load(dump);
  }
  g_string_free(dump, TRUE);
  rows_target = g_ptr_array_new();
  for (i = 0; i < row_nums_splitted->len; i++) {
    nums = g_ptr_array_index(row_nums_splitted, i);
    num = g_array_index(nums, int, 0);
    if (num >= mpi_node_info->row_start && num <= mpi_node_info->row_end) {
      g_ptr_array_add(rows_target, nums);
    }
  }
  lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread(mat, arg, rows_target, NULL);
  g_ptr_array_free(rows_target, TRUE);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    lil_matrix_snf_reduction_by_inclusion_at_each_row(mat, arg, pivot_splitted, 0, mat->row_size - 1, NULL, false);
    /* lil_matrix_snf_reduction_by_inclusion_at_each_row(mat, arg, pivot_splitted, 0, mat->row_size - 1, NULL, true); /\* Too late *\/ */
    lil_matrix_snf_reduction_by_inclusion_targets_free(pivot_splitted);
  }
  g_array_sort(pivot_row_nums, compare_integers);
  lil_matrix_mpi_bcast_rows_from_root(mpi_node_info, mat, pivot_row_nums);
  g_array_free(pivot_row_nums, TRUE);
  lil_matrix_snf_reduction_by_inclusion_targets_free(row_nums_splitted);
  lil_matrix_mpi_remove_temporary_rows(mpi_node_info, mat);
}

static void lil_matrix_mpi_snf_reduction_by_inclusion (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray *target_rows)
{
  GPtrArray *inclusion_index;
  lil_matrix_mpi_update_cache(mpi_node_info, mat, arg, NULL);
  inclusion_index = lil_matrix_mpi_snf_create_inclusion_index_multithread(mpi_node_info, mat, arg, target_rows);
  lil_matrix_mpi_snf_reduction_by_inclusion_step(mpi_node_info, mat, arg, inclusion_index);
  if (inclusion_index) {    /* For root node */
    lil_matrix_snf_inclusion_index_clear(inclusion_index, true);
  }
}

static int lil_matrix_mpi_dumped_rows_save_int (GString *dump, FILE *fp)
{
  int i, j, num_rows, num_els, *nums;
  char *buf;
  buf = dump->str;
  num_rows = *((int *) buf);
  buf += sizeof(int);
  for (i = 0; i < num_rows; i++) {
    int row_num;
    row_num = *((int *) buf);
    if (row_num < 0) {
      return -row_num;
    }
    buf += sizeof(int);
    num_els = *((int *) buf);
    buf += sizeof(int);
    nums = (int *) buf;
    for (j = 0; j < num_els; j++) {
      fprintf(fp, (j == 0 ? "%d %d" : " %d %d"), nums[j * 2], nums[j * 2 + 1]);
    }
    buf += sizeof(int) * num_els * 2;
    fprintf(fp, "\n");
  }
  return 0;
}

static void lil_matrix_mpi_dump (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, FILE *fp)
{
  int len, len_all[mpi_node_info->number_nodes], rank, row_end_dumped;
  GString *dump = NULL;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    fprintf(fp, "# %d %d %d\n", mat->row_size, mat->column_size, mat->modulo);
    lil_matrix_rows_dump_text(mat, 0, mpi_node_info->row_end, fp);
    len = 0;
  } else {
    dump = lil_matrix_mpi_rows_dump_to_gstring2(mat, mpi_node_info->row_start, mpi_node_info->row_end, &row_end_dumped);
    len = dump->len;
    len += sizeof(int);
  }
  MPI_Gather(&len, 1, MPI_INT, len_all, 1, MPI_INT, MPI_ROOT_RANK, MPI_COMM_WORLD);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    int size_next, tag;
    MPI_Status status;
    for (rank = 0; rank < mpi_node_info->number_nodes; rank++) {
      if (rank != MPI_ROOT_RANK) {
        tag = 0;
        size_next = len_all[rank];
        while (size_next != 0) {
          dump = g_string_sized_new(size_next);
          dump->len = size_next;
          MPI_Recv(dump->str, dump->len, MPI_CHAR, rank, tag, MPI_COMM_WORLD, &status);
          if (mat->use_gmp_p) {
            fprintf(stderr, "Not implemented\n");
            abort_with_line_number();
          } else {
            size_next = lil_matrix_mpi_dumped_rows_save_int(dump, fp);
          }
          g_string_free(dump, TRUE);
          tag += 1;
        }
      }
    }
  } else {
    GString *dump2;
    int size_next, tag;
    tag = 0;
    while (row_end_dumped != mpi_node_info->row_end) {
      dump2 = lil_matrix_mpi_rows_dump_to_gstring2(mat, row_end_dumped + 1, mpi_node_info->row_end, &row_end_dumped);
      size_next = -dump2->len - sizeof(int);
      g_string_append_len(dump, (char *) &size_next, sizeof(int));
      MPI_Send(dump->str, dump->len, MPI_CHAR, MPI_ROOT_RANK, tag, MPI_COMM_WORLD);
      g_string_free(dump, TRUE);
      dump = dump2;
      tag += 1;
    }
    MPI_Send(dump->str, dump->len, MPI_CHAR, MPI_ROOT_RANK, tag, MPI_COMM_WORLD);
    g_string_free(dump, TRUE);
  }
}

static void lil_matrix_mpi_dump_to_file (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, char *path)
{
  FILE *fp;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    fp = fopen(path, "wb");
    if (fp == NULL) {
      fprintf(stderr, "Can not open file %s\n", path);
      abort_with_line_number();
    }
  } else {
    fp = NULL;
  }
  lil_matrix_mpi_dump(mpi_node_info, mat, fp);
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    fclose(fp);
  }
}

static void lil_matrix_mpi_snf_intermediate_dump (MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray *snf, LIL_MATRIX *column_ops, GArray *column_pivots_seq)
{
  GString *path;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    GString *dir;
    dir = lil_matrix_snf_intermediate_result_dump(arg, snf, column_ops, column_pivots_seq);
    path = g_string_sized_new(dir->len * 2);
    g_string_printf(path, "%s/mat.txt", dir->str);
    g_string_free(dir, TRUE);
  } else {
    path = NULL;
  }
  lil_matrix_mpi_dump_to_file(mpi_node_info, mat, (path ? path->str : NULL));
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    g_string_free(path, TRUE);
  }
}

static void ignore_sigterm (int sig)
{
}

static void lil_matrix_mpi_snf_signal_set (MPI_NODE_INFO *mpi_node_info)
{
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    lil_matrix_snf_signal_set();
  } else {
    signal_sigterm = false;
    if (SIG_ERR == signal(SIGTERM, ignore_sigterm)) {
      fprintf(stderr, "Failed to set signal handler.\n");
      exit(1);
    }
    if (SIG_ERR == signal(SIGINT, ignore_sigterm)) {
      fprintf(stderr, "Failed to set signal handler.\n");
      exit(1);
    }
  }
}

/* In this function we does not delete empty rows. */
static GArray *lil_matrix_mpi_snf_with_full_matrix (LIL_MATRIX *mat, LIL_SNF_ARG *arg, MPI_NODE_INFO *mpi_node_info, LIL_MATRIX *mat_full, bool exec_test)
{
  int sum_increase, cumulative_sum_increase;
  GArray *snf, *column_pivots_seq, *snf_full;
  LIL_MATRIX *column_ops;
  bool delete_at_min = false, reduce_by_inclusion = false, finish, output_info, divide_p, reduce_p, signal_p;
  GPtrArray *snf_vals;
  if (arg->signal_use_p) {
    lil_matrix_mpi_snf_signal_set(mpi_node_info);
  }
  signal_p = false;
  lil_snf_argument_cache_init(arg, CACHE_BOTH, mat);
  if (MPI_ROOT_RANK == mpi_node_info->rank) {
    cumulative_sum_increase = 0;
    delete_at_min = arg->reduce_by_divisors > 0 || arg->auto_delete_duplicates;
    lil_matrix_snf_load_directory(mat, arg, &snf, &column_ops, &column_pivots_seq);
    if (exec_test) {
      snf_full = g_array_new(FALSE, FALSE, sizeof(mpz_t));
      __mpz_struct *free_part_number;
      free_part_number = lil_matrix_snf_array_new_value_init(snf_full);
      mpz_set_si(free_part_number, 0);
      free_part_number = lil_matrix_snf_array_new_value_init(snf_full);
      mpz_set_si(free_part_number, (mat->row_size > mat->column_size ? mat->column_size : mat->row_size));
    } else {
      snf_full = NULL;
    }
  } else {
    snf = NULL;
    snf_full = NULL;
    column_ops = NULL;
    column_pivots_seq = NULL;
  }

  if (arg->info > 0) {
    lil_matrix_mpi_snf_output_info(mpi_node_info, "Matrix", NULL, mat, NULL);
    if (mpi_node_info->rank == MPI_ROOT_RANK) {
      int i;
      printf("  Row numbers:\n");
      for (i = 0; i < mpi_node_info->row_end_numbers->len; i++) {
        if (i == 0) {
          printf("    %d: %d %d\n", i, 0, g_array_index(mpi_node_info->row_end_numbers, int, i));
        } else {
          printf("    %d: %d %d\n", i, g_array_index(mpi_node_info->row_end_numbers, int, i - 1) + 1, g_array_index(mpi_node_info->row_end_numbers, int, i));
        }
      }
    }
  }
  lil_matrix_mpi_snf_validation(mpi_node_info, mat, arg);
  while (mat->column_size > 0 && mat->row_size > 0) {
    if (MPI_ROOT_RANK == mpi_node_info->rank) {
      arg->stat->step += 1;
      reduce_by_inclusion = arg->stat->increasing &&
        (delete_at_min || (arg->reduce_by_divisors > 0 &&
                           (arg->stat->elementary_divisors - arg->stat->last_reducing) >= arg->reduce_by_divisors));
    }
    snf_vals = lil_matrix_mpi_snf_partially(mpi_node_info, mat, arg, column_ops, column_pivots_seq, &arg->stat->empty_rows, &sum_increase, mat_full, exec_test);
    if (MPI_ROOT_RANK == mpi_node_info->rank) {
      finish = (!snf_vals ? true : false);
    }
    lil_matrix_mpi_bcast_bool_from_root(&finish);
    if (finish) {
      break;
    }
    if (MPI_ROOT_RANK == mpi_node_info->rank) {
      output_info = (arg->info > 0 && arg->stat->elementary_divisors - arg->stat->last_output >= arg->info ? true : false);
    }
    lil_matrix_mpi_bcast_int_from_root(&cumulative_sum_increase);
    lil_matrix_mpi_bcast_int_from_root(&arg->stat->elementary_divisors);
    lil_matrix_mpi_bcast_bool_from_root(&output_info);

    if (MPI_ROOT_RANK == mpi_node_info->rank) {
      __mpz_struct *val;
      cumulative_sum_increase += sum_increase;
      arg->stat->elementary_divisors += snf_vals->len;
      val = (__mpz_struct *) g_ptr_array_index(snf_vals, 0);
      divide_p = (mpz_cmp_ui(val, 1) != 0 ? true : false); /* If val != 1, snf_vals->len should be 1 */
      if (divide_p && snf_vals->len != 1) {
        fprintf(stderr, "Invalid snf_vals\n");
        abort_with_line_number();
      }
    }
    lil_matrix_mpi_bcast_bool_from_root(&divide_p);
    if (divide_p) {
      mpz_t val;
      mpz_init(val);
      if (MPI_ROOT_RANK == mpi_node_info->rank) {
        mpz_set(val, (__mpz_struct *) g_ptr_array_index(snf_vals, 0));
      }
      lil_matrix_mpi_bcast_mpz_from_root(mpi_node_info, val);
      if (mpi_node_info->rank != MPI_ROOT_RANK) {
        lil_matrix_divide_elements_mpz(mat, val);
        lil_snf_argument_cache_reset(arg, CACHE_NOT_CHANGED, mat);
      }
      mpz_clear(val);
    }
    if (mpi_node_info->rank == MPI_ROOT_RANK) {
      if (exec_test) {
        lil_matrix_snf_divide_snf_values(mat_full, arg, snf_full, snf_vals);
      }
      lil_matrix_snf_concat_clear_results(mat, arg, snf, &snf_vals);
      reduce_p = reduce_by_inclusion || (arg->auto_delete_duplicates && cumulative_sum_increase > arg->reduce_by_increase);
    }
    lil_matrix_mpi_bcast_bool_from_root(&reduce_p);
    if (output_info) {
      lil_matrix_mpi_snf_output_info(mpi_node_info, "Calculation", arg->stat, mat, snf);
      arg->stat->last_output = arg->stat->elementary_divisors;
    }
    if (reduce_p) {
      if (arg->info > 0) {
        lil_matrix_mpi_snf_output_info(mpi_node_info, "Before reducing elements", arg->stat, mat, snf);
      }
      lil_matrix_mpi_snf_reduction_by_inclusion(mpi_node_info, mat, arg, NULL);
      if (arg->info > 0) {
        lil_matrix_mpi_snf_output_info(mpi_node_info, "Finish reducing", arg->stat, mat, snf);
      }
      lil_snf_argument_cache_reset(arg, CACHE_NOT_CHANGED, mat);
      if (mpi_node_info->rank == MPI_ROOT_RANK) {
        arg->stat->last_reducing = arg->stat->elementary_divisors;
        if (delete_at_min) {
          delete_at_min = false;
        }
        cumulative_sum_increase = 0;
      }
    }
    lil_matrix_mpi_snf_validation(mpi_node_info, mat, arg);
    if (mpi_node_info->rank == MPI_ROOT_RANK) {
      if (arg->signal_use_p && signal_sigterm) {
        signal_p = true;
      }
    }
    lil_matrix_mpi_bcast_bool_from_root(&signal_p);
    if (signal_p) {
      lil_matrix_mpi_snf_intermediate_dump(mpi_node_info, mat, arg, snf, column_ops, column_pivots_seq);
      MPI_Finalize();
      exit(0);
    }
  }
  if (column_ops) {
    lil_matrix_snf_column_operations_matrix_dump_clear(column_ops, column_pivots_seq, arg->column_ops_output);
  }
  if (MPI_ROOT_RANK == mpi_node_info->rank) {
    lil_matrix_snf_set_free_part_number(snf);
    if (exec_test) {
      lil_matrix_snf_array_free(snf_full);
    }
  }
  lil_snf_argument_cache_clear(arg);
  MPI_Barrier(MPI_COMM_WORLD);
  return snf;
}

GArray *lil_matrix_mpi_snf (LIL_MATRIX *mat, LIL_SNF_ARG *arg, MPI_NODE_INFO *mpi_node_info)
{
  return lil_matrix_mpi_snf_with_full_matrix(mat, arg, mpi_node_info, NULL, false);
}

GArray *lil_matrix_mpi_snf_test (LIL_MATRIX *mat, char *path, LIL_SNF_ARG *arg, MPI_NODE_INFO *mpi_node_info)
{
  GArray *snf;
  LIL_MATRIX *mat_full;
  if (mpi_node_info->rank == MPI_ROOT_RANK) {
    mat_full = lil_matrix_load_file(path, LIL_MATRIX_DATA_TEXT);
  } else {
    mat_full = NULL;
  }
  snf = lil_matrix_mpi_snf_with_full_matrix(mat, arg, mpi_node_info, mat_full, true);
  if (mat_full) {
    lil_matrix_clear(mat_full);
  }
  return snf;
}
