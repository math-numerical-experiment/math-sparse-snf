#ifndef _LILMATRIX_MPI_H_
#define _LILMATRIX_MPI_H_

#include "../lilmatrix.h"
#include <mpi.h>
#include <linux/kernel.h>
#include <sys/sysinfo.h>

#define MPI_ROOT_RANK 0

#define COMMAND_HOSTNAME "hostname"
#define COMMAND_CPU_NUMBER "bash -c \"cat /proc/cpuinfo | grep processor | wc -l\""

#define DEFAULT_MEMORY_RESERVE (1024.0 * 1.5)

typedef struct {
  int rank;
  int number_nodes;
  int cpu;
  double memory;
  double memory_reserve;
  char *hostname;
  int row_start;
  int row_end;
  GArray *row_end_numbers;
} MPI_NODE_INFO;

void mpi_node_info_set (MPI_NODE_INFO *mpi_node_info);
void mpi_node_info_clear (MPI_NODE_INFO *mpi_node_info);
LIL_MATRIX *lil_matrix_mpi_split_load_file (MPI_NODE_INFO *mpi_node_info, const char *path);
GArray *lil_matrix_mpi_snf (LIL_MATRIX *mat, LIL_SNF_ARG *arg, MPI_NODE_INFO *mpi_node_info);
GArray *lil_matrix_mpi_snf_test (LIL_MATRIX *mat, char *path, LIL_SNF_ARG *arg, MPI_NODE_INFO *mpi_node_info);

#endif /* _LILMATRIX_MPI_H_ */
