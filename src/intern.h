#ifndef _INTERN_H_
#define _INTERN_H_

typedef struct {
  int row;
  int column;
  mpz_t abs;
  int increase;
} LIL_MATRIX_PIVOT;

typedef struct {
  int column;
  int size;
  mpz_t value;
} LIL_MATRIX_MIN_VALUE_CACHE;

int compare_integers(gconstpointer a, gconstpointer b);
void lil_matrix_debug_fprint_matrix_size (FILE *fp, LIL_MATRIX *mat, CalcStat *stat);
void lil_matrix_number_of_non_zero_elements (LIL_MATRIX *mat, mpz_t count);
void lil_matrix_mpi_total_number_of_elements (LIL_MATRIX *mat, CalcStat *stat, mpz_t total);
void lil_matrix_percentage_of_non_zero_elements (LIL_MATRIX *mat, CalcStat *astat, mpz_t total, mpz_t non_zero_count, mpf_t non_zero_percentage);
void lil_snf_argument_cache_init (LIL_SNF_ARG *arg, LIL_MATRIX_CACHE_TYPE type, LIL_MATRIX *mat);
void lil_snf_argument_cache_clear (LIL_SNF_ARG *arg);
void lil_matrix_thread_pool_free ();
void lil_matrix_snf_output_info (char *str, CalcStat *stat, LIL_MATRIX *mat, GArray *snf);
__mpz_struct *lil_matrix_snf_array_new_value_init (GArray *snf);
void lil_matrix_snf_array_append (GArray *snf, mpz_t val);
void lil_matrix_snf_divide_snf_values (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray *snf, GPtrArray *snf_vals);
void lil_matrix_snf_concat_clear_results (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray *snf, GPtrArray **snf_vals_ptr);
void lil_matrix_snf_validation (LIL_MATRIX *mat, LIL_SNF_ARG *arg);
void lil_matrix_snf_signal_set ();
void lil_matrix_snf_set_free_part_number (GArray *snf);
bool lil_matrix_text_file_get_matrix_arguments (int mat_args[], const char *path);
LIL_MATRIX *lil_matrix_load_file_row_range (const char *path, int row_start, int row_end);
void lil_matrix_snf_load_directory (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GArray **snf, LIL_MATRIX **column_ops, GArray **column_pivots_seq);
void lil_matrix_snf_save_column_numbers (GArray *column_pivots_seq, GPtrArray *pivot_array);
GPtrArray *lil_matrix_snf_take_elementary_divisors (LIL_MATRIX *mat, GPtrArray *pivot_array);
void lil_matrix_snf_make_pivot_rows_empty (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *pivot_array);
void lil_matrix_snf_column_operations_matrix_dump_clear (LIL_MATRIX *mat, GArray *column_pivots_seq, char *path);
void lil_matrix_pivot_array_sort (GPtrArray *pivot_array);
GPtrArray *lil_matrix_minimal_non_zero_element_candidates (int *el_row_num, int *count_empty_rows, LIL_MATRIX *mat, int row_num_start, int row_num_end, mpz_t min_abs_val, LIL_SNF_ARG *arg);
GPtrArray *lil_matrix_minimal_non_zero_element (int *el_row_num, int *count_empty_rows, LIL_MATRIX *mat, int row_num_start, int row_num_end, mpz_t min_abs_val, LIL_SNF_ARG *arg, int *sum_increase);

LIL_MATRIX_PIVOT *lil_matrix_pivot_new (int row, int column, mpz_t abs);
int lil_matrix_pivots_sum_increase (GPtrArray *pivot_array);
gpointer lil_matrix_snf_pivot_array_free (GPtrArray *pivot_array, int index_start);
GPtrArray *lil_matrix_snf_pivot_rows_select (GPtrArray *pivot_array, LIL_SNF_ARG *arg, int num_elements);
void lil_matrix_update_cache (LIL_MATRIX *mat, LIL_SNF_ARG *arg, mpz_t min_val);
void lil_matrix_update_cache_row_range (LIL_MATRIX *mat, LIL_SNF_ARG *arg, int row_num_start, int row_num_end, mpz_t min_val);
int array_find_number (GArray *array, int num, bool *find_p);
GPtrArray *lil_matrix_column_index_cache_new (int column_size);
void lil_snf_argument_cache_column_index_clear (GPtrArray *column_index);
void lil_matrix_column_index_cache_save (GPtrArray *column_index_cache, GString *path);
bool lil_snf_argument_cache_column_index_updated_p (LIL_SNF_ARG *arg);
void lil_snf_argument_remove_row_cache (LIL_SNF_ARG *arg, int row_pivot);
LIL_MATRIX_ELEMENT *lil_matrix_element_new (int column, int value);
bool lil_matrix_snf_partially_row_reduce (LIL_MATRIX *mat, GPtrArray *pivot_array, LIL_SNF_ARG *arg, int row_start_num, int row_end_num);
void lil_matrix_row_data_clear (GSList *row, bool use_gmp);
bool lil_matrix_snf_divisible_p (LIL_MATRIX *mat, bool *divisible, int *i, int *j, mpz_t q, int row_pivot, int column_pivot);
bool lil_matrix_snf_partially_column_reduce_after_row_reduction (LIL_MATRIX *mat, GPtrArray *pivot_array, LIL_MATRIX *column_ops);
void lil_snf_argument_cache_reset (LIL_SNF_ARG *arg, LIL_MATRIX_CACHE_TYPE type, LIL_MATRIX *mat);
void lil_matrix_snf_validate_snf_values (LIL_MATRIX *mat, GPtrArray *pivot_array);
void lil_matrix_snf_cache_validate (LIL_MATRIX *mat, LIL_SNF_ARG *arg);
void lil_matrix_snf_output_validate (LIL_MATRIX *mat, LIL_SNF_ARG *arg, char *str);
void lil_matrix_snf_create_inclusion_index_multithread_for_range (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *inclusion_index, GArray *target_rows, int row_num_start, int row_num_end);
void lil_matrix_snf_inclusion_index_clear (GPtrArray *inclusion_index, bool free_index);
void lil_matrix_snf_split_inclusion_index (LIL_MATRIX *mat, GPtrArray *inclusion_index, GArray **out_pivot_row_nums, GPtrArray **out_pivot_splitted, GPtrArray **out_row_nums_splitted);
void lil_matrix_snf_reduction_by_inclusion_at_each_row (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *row_nums_target, int row_num_start, int row_num_end, GArray *row_changed, bool test_inclusion);
void lil_matrix_snf_reduction_by_inclusion_targets_free (GPtrArray *targets);
GPtrArray *lil_matrix_snf_reduction_by_inclusion_at_each_row_multithread (LIL_MATRIX *mat, LIL_SNF_ARG *arg, GPtrArray *row_nums_target, GPtrArray *row_nums_to_split);
GString *lil_matrix_snf_intermediate_result_dump (LIL_SNF_ARG *arg, GArray *snf, LIL_MATRIX *column_ops, GArray *column_pivots_seq);
void lil_matrix_rows_dump_text (LIL_MATRIX *mat, int row_start, int row_end, FILE *fp);

#define MPF_PREC 128

bool signal_sigterm;

#endif /* _INTERN_H_ */
