#include <gcutter.h>
#include <lilmatrix.h>
#include <intern.h>

void test_cache_column_index ()
{
  LIL_MATRIX *mat;
  LIL_SNF_ARG *arg;
  GArray *nums;
  int i, j;
  int data[] = {
    3,  0, 0, -2, -1, 0, 0,
    0,  0, 0,  0,  1, 2, 0,
    -1, 1, 0,  0,  0, 0, 3,
    0,  0, 0,  0,  0, 2, 0,
    2,  1, 0,  3,  0, 0, 0,
    0,  0, -1, 0, -1, 0, 0
  };
  int cache[7][6] = {
    {0, 2, 4, 0, 0, 0},
    {2, 4, 0, 0, 0, 0},
    {5, 0, 0, 0, 0, 0},
    {0, 4, 0, 0, 0, 0},
    {0, 1, 5, 0, 0, 0},
    {1, 3, 0, 0, 0, 0},
    {2, 0, 0, 0, 0, 0}
  };
  mat = lil_matrix_new2(6, 7, 10, data);
  arg = lil_snf_argument_new();
  lil_snf_argument_cache_init(arg, CACHE_BOTH, mat);
  lil_matrix_update_cache(mat, arg, NULL);
  for (i = 0; i < arg->cache->column_index->len; i++) {
    nums = g_ptr_array_index(arg->cache->column_index, i);
    for (j = 0; j < nums->len; j++) {
      cut_assert_equal_int(cache[i][j], g_array_index(nums, int, j));
    }
  }
}

void test_cache_inclusion ()
{
  LIL_MATRIX *mat;
  LIL_SNF_ARG *arg;
  GArray *nums;
  int i, j;
  int data[] = {
    0,  0, 3,  0,  1, 0, 0,
    3,  8, 1, -2, -1, 2, 0,
    -1, 0, 0,  0,  0, 0, 3,
    0,  0, 0,  0,  0, 2, 0,
    2,  2, 2,  0,  1, 0, 0,
    1,  1, 1,  0, -1, 0, 0
  };
  int cache[4][4] = {
    {0, 1, 4, 5},
    {3, 1, 0, 0},
    {4, 1, 5, 0},
    {5, 1, 4, 0},
  };
  GPtrArray *inclusion_index;
  mat = lil_matrix_new2(6, 7, 10, data);
  arg = lil_snf_argument_new();
  lil_snf_argument_cache_init(arg, CACHE_BOTH, mat);
  lil_matrix_update_cache(mat, arg, NULL);
  inclusion_index = g_ptr_array_new();
  lil_matrix_snf_create_inclusion_index_multithread_for_range(mat, arg, inclusion_index, NULL, 0, mat->row_size - 1);
  for (i = 0; i < inclusion_index->len; i++) {
    nums = g_ptr_array_index(inclusion_index, i);
    for (j = 0; j < nums->len; j++) {
      cut_assert_equal_int(cache[i][j], g_array_index(nums, int, j));
    }
  }

  lil_matrix_snf_inclusion_index_clear(inclusion_index, true);
}
