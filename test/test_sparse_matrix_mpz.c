#include <gcutter.h>
#include <lilmatrix.h>

#define MODULO_USE_MPZ 0

void test_mpz_initialization_set_and_get_elements ()
{
  LIL_MATRIX *mat;
  int row_data[] = { 0, 10, 2, 30, 1, 20 };
  int row_data2[] = { 0, -10, 2, -30, 1, -20 };

  mat = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 3, row_data);
  lil_matrix_row_set(mat, 1, 3, row_data2);

  cut_assert_equal_int(10, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(20, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(30, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(-10, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(-20, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-30, lil_matrix_element(mat, 1, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_initialization_matrix_get_mpz ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 1, 2 };
  int row_data2[] = { 0, 4, 2, -5 };
  mpz_t val;
  mpz_init(val);

  mat = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 1, row_data1);
  lil_matrix_row_set(mat, 1, 2, row_data2);

  lil_matrix_element_mpz(val, mat, 0, 0);
  cut_assert(mpz_cmp_si(val, 0) == 0);
  lil_matrix_element_mpz(val, mat, 0, 1);
  cut_assert(mpz_cmp_si(val, 2) == 0);
  lil_matrix_element_mpz(val, mat, 0, 2);
  cut_assert(mpz_cmp_si(val, 0) == 0);
  lil_matrix_element_mpz(val, mat, 1, 0);
  cut_assert(mpz_cmp_si(val, 4) == 0);
  lil_matrix_element_mpz(val, mat, 1, 1);
  cut_assert(mpz_cmp_si(val, 0) == 0);
  lil_matrix_element_mpz(val, mat, 1, 2);
  cut_assert(mpz_cmp_si(val, -5) == 0);

  lil_matrix_clear(mat);
  mpz_clear(val);
}

void test_mpz_initialization_matrix_new ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 1, 2 };
  int row_data2[] = { 0, 4, 2, -5 };

  mat = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 1, row_data1);
  lil_matrix_row_set(mat, 1, 2, row_data2);

  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(2, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(4, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(0, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_initialization_matrix_new2 ()
{
  LIL_MATRIX *mat1, *mat2;
  int row_data1[] = { 1, 2 };
  int row_data2[] = { 0, 4, 2, -5 };
  int data[] = { 0, 2, 0, 4, 0, -5 };

  mat1 = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat1, 0, 1, row_data1);
  lil_matrix_row_set(mat1, 1, 2, row_data2);
  mat2 = lil_matrix_new2(2, 3, MODULO_USE_MPZ, data);

  cut_assert(lil_matrix_equal_p(mat1, mat2));
  cut_assert(lil_matrix_valid_p(mat1));
  cut_assert(lil_matrix_valid_p(mat2));

  lil_matrix_clear(mat1);
}

void test_mpz_initialization_matrix_new3 ()
{
  int data1[] = { 0, 3 };
  int data2[] = { 3, 8, 9, 20 };
  LIL_MATRIX *mat;

  mat = lil_matrix_new(0, 0, MODULO_USE_MPZ);
  lil_matrix_row_append (mat, 1, data1);
  lil_matrix_row_append (mat, 2, data2);

  cut_assert_equal_int(2, mat->row_size);
  cut_assert_equal_int(10, mat->column_size);

  lil_matrix_clear(mat);
}

void test_mpz_initialization_matrix_row_append ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 1, 2 };
  int row_data2[] = { 0, 4, 2, -5 };

  mat = lil_matrix_new(1, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 1, row_data1);
  cut_assert_equal_int(1, mat->row_size);
  lil_matrix_row_append(mat, 2, row_data2);
  cut_assert_equal_int(2, mat->row_size);

  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(2, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(4, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(0, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_initialization_matrix_copy ()
{
  LIL_MATRIX *mat1, *mat2;
  int row_data1[] = { 1, 2 };
  int row_data2[] = { 0, 4, 2, -5 };

  mat1 = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat1, 0, 1, row_data1);
  lil_matrix_row_set(mat1, 1, 2, row_data2);
  mat2 = lil_matrix_copy(mat1);

  cut_assert(lil_matrix_equal_p(mat1, mat2));
  cut_assert(lil_matrix_valid_p(mat1));
  cut_assert(lil_matrix_valid_p(mat2));

  lil_matrix_clear(mat1);
  lil_matrix_clear(mat2);
}

void test_mpz_initialization_matrix_dump_load_binary ()
{
  FILE *fp;
  LIL_MATRIX *mat1, *mat2;
  int data[] = {
    3, 0, 0, -2, -1, 0, 0,
    0, 0, 0, 0, 1, 2, 0,
    -1, 1, 0, 0, 0, 0, 3,
    0, 0, 0, 0, 0, 0, 0,
    2, 1, 0, 3, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0
  };
  mat1 = lil_matrix_new2(6, 7, 10, data);

  if ( (fp = tmpfile()) == NULL ) {
    fprintf(stderr, "Can not open temporary file\n");
    abort_with_line_number();
  }
  lil_matrix_dump(mat1, fp, LIL_MATRIX_DATA_BINARY);
  fflush(fp);
  rewind(fp);
  mat2 = lil_matrix_load(fp, LIL_MATRIX_DATA_BINARY);
  fclose(fp);

  cut_assert(lil_matrix_equal_p(mat1, mat2));
  cut_assert(lil_matrix_valid_p(mat1));
  cut_assert(lil_matrix_valid_p(mat2));

  lil_matrix_clear(mat1);
  lil_matrix_clear(mat2);
}

void test_mpz_initialization_matrix_dump_load_text ()
{
  FILE *fp;
  LIL_MATRIX *mat1, *mat2;
  int data[] = {
    3, 0, 0, -2, -1, 0, 0,
    0, 0, 0, 0, 1, 2, 0,
    -1, 1, 0, 0, 0, 0, 3,
    0, 0, 0, 0, 0, 0, 0,
    2, 1, 0, 3, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0
  };
  mat1 = lil_matrix_new2(6, 7, 10, data);

  if ( (fp = tmpfile()) == NULL ) {
    fprintf(stderr, "Can not open temporary file\n");
    abort_with_line_number();
  }
  lil_matrix_dump(mat1, fp, LIL_MATRIX_DATA_TEXT);
  fflush(fp);
  rewind(fp);
  mat2 = lil_matrix_load(fp, LIL_MATRIX_DATA_TEXT);
  fclose(fp);

  cut_assert(lil_matrix_equal_p(mat1, mat2));
  cut_assert(lil_matrix_valid_p(mat1));
  cut_assert(lil_matrix_valid_p(mat2));

  lil_matrix_clear(mat1);
  lil_matrix_clear(mat2);
}

void test_mpz_equal_p ()
{
  LIL_MATRIX *mat1, *mat2;
  int row_data1[] = { 1, 2 };
  int row_data2[] = { 0, 4, 2, -5 };

  mat1 = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  mat2 = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat1, 0, 1, row_data1);
  lil_matrix_row_set(mat1, 1, 2, row_data2);
  lil_matrix_row_set(mat2, 0, 1, row_data1);
  lil_matrix_row_set(mat2, 1, 2, row_data2);

  cut_assert(lil_matrix_equal_p(mat1, mat2));
  cut_assert(lil_matrix_valid_p(mat1));
  cut_assert(lil_matrix_valid_p(mat2));

  lil_matrix_clear(mat1);
  lil_matrix_clear(mat2);
}

void test_mpz_equal_p2 ()
{
  LIL_MATRIX *mat1, *mat2;
  int data[] = {
    3, 0, 0, -2, -1, 0, 0,
    0, 0, 0, 0, 1, 2, 0,
    -1, 1, 0, 0, 0, 0, 3,
    0, 0, 0, 0, 0, 0, 0,
    2, 1, 0, 3, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0
  };
  mat1 = lil_matrix_new2(6, 7, 10, data);
  mat2 = lil_matrix_new2(6, 7, 10, data);

  cut_assert(lil_matrix_equal_p(mat1, mat2));
  cut_assert(lil_matrix_valid_p(mat1));
  cut_assert(lil_matrix_valid_p(mat2));

  lil_matrix_clear(mat1);
  lil_matrix_clear(mat2);
}

void test_mpz_equal_not_p ()
{
  LIL_MATRIX *mat1, *mat2;
  int row_data1[] = { 1, 2 };
  int row_data2[] = { 0, 4, 2, -5 };
  int row_data3[] = { 1, 2 };
  int row_data4[] = { 0, 4, 2, -3 };

  mat1 = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  mat2 = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat1, 0, 1, row_data1);
  lil_matrix_row_set(mat1, 1, 2, row_data2);
  lil_matrix_row_set(mat2, 0, 1, row_data3);
  lil_matrix_row_set(mat2, 1, 2, row_data4);

  cut_assert(!lil_matrix_equal_p(mat1, mat2));
  cut_assert(lil_matrix_valid_p(mat1));
  cut_assert(lil_matrix_valid_p(mat2));

  lil_matrix_clear(mat1);
  lil_matrix_clear(mat2);
}

void test_mpz_transform_transpose ()
{
  int data[] = {
    3, 0, 0, -2, -1, 0, 0,
    0, 0, 0, 0, 1, 2, 0,
    -1, 1, 0, 0, 0, 0, 3,
    2, 1, 0, 3, 0, 0, 0
  };
  LIL_MATRIX *mat;
  int column_size, row_size, i, j;
  row_size = 4;
  column_size = 7;
  mat = lil_matrix_new2(row_size, column_size, MODULO_USE_MPZ, data);
  for (i = 0; i < row_size; i++) {
    for (j = 0; j < column_size; j++) {
      cut_assert_equal_int(lil_matrix_element(mat, i, j), data[i * column_size + j]);
    }
  }
  lil_matrix_transpose(mat);
  for (i = 0; i < row_size; i++) {
    for (j = 0; j < column_size; j++) {
      cut_assert_equal_int(lil_matrix_element(mat, j, i), data[i * column_size + j]);
    }
  }
  cut_assert(lil_matrix_valid_p(mat));
  lil_matrix_clear(mat);
}

void test_mpz_transform_set_an_element_zero ()
{
  LIL_MATRIX *mat;
  int row_data[10] = { 0, 10, 3, 13, 4, 14, 1, 11, 2, 12 };
  int row_data2[4] = { 3, 0, 1, 0 };

  mat = lil_matrix_new(2, 5, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 5, row_data);
  lil_matrix_row_set(mat, 0, 2, row_data2);

  cut_assert(g_slist_length((GSList *) g_slist_nth(mat->rows, 0)->data) == 3);
  cut_assert_equal_int(10, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(12, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 3));
  cut_assert_equal_int(14, lil_matrix_element(mat, 0, 4));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_transform_delete_row ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, 10, 2, 30, 1, 20 };
  int row_data2[] = { 0, -10, 2, -30, 1, -20 };
  int row_data3[] = { 0, 10, 1, 20, 2, 30 };

  mat = lil_matrix_new(3, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 3, row_data1);
  lil_matrix_row_set(mat, 1, 3, row_data2);
  lil_matrix_row_set(mat, 2, 3, row_data3);
  lil_matrix_row_delete(mat, 1);

  cut_assert_equal_int(mat->row_size, 2);
  cut_assert_equal_int(mat->column_size, 3);

  cut_assert_equal_int(10, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(20, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(30, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(10, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(20, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(30, lil_matrix_element(mat, 1, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_transform_delete_column ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, 10, 2, 30, 1, 20 };
  int row_data2[] = { 0, -10, 2, -30, 1, -20 };
  int row_data3[] = { 0, 10, 1, 20, 2, 30 };

  mat = lil_matrix_new(3, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 3, row_data1);
  lil_matrix_row_set(mat, 1, 3, row_data2);
  lil_matrix_row_set(mat, 2, 3, row_data3);
  lil_matrix_column_delete(mat, 1);

  cut_assert_equal_int(mat->row_size, 3);
  cut_assert_equal_int(mat->column_size, 2);

  cut_assert_equal_int(10, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(30, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(-10, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(-30, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(10, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(30, lil_matrix_element(mat, 2, 1));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_transform_delete_column2 ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, 10, 2, 30, 1, 20 };
  int row_data2[] = { 2, -30, 1, -20 };
  int row_data3[] = { 1, 20, 2, 30 };

  mat = lil_matrix_new(3, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 3, row_data1);
  lil_matrix_row_set(mat, 1, 2, row_data2);
  lil_matrix_row_set(mat, 2, 2, row_data3);
  lil_matrix_column_delete(mat, 0);

  cut_assert_equal_int(mat->row_size, 3);
  cut_assert_equal_int(mat->column_size, 2);

  cut_assert_equal_int(20, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(30, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(-20, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(-30, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(20, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(30, lil_matrix_element(mat, 2, 1));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_transform_delete_column3 ()
{
  int data[] = {
    3, 0, 0, -2, -1, 0, 0,
    0, 0, 0, 0, 1, 2, 0,
    -1, 1, 0, 0, 0, 0, 3,
    2, 1, 0, 3, 0, 0, 0
  };
  LIL_MATRIX *mat, *mat2;
  int column_size, col_delete, row, col, c;
  column_size = 7;
  mat = lil_matrix_new2(4, column_size, MODULO_USE_MPZ, data);
  for (col_delete = 0; col_delete < column_size; col_delete++) {
    mat2 = lil_matrix_copy(mat);
    lil_matrix_column_delete(mat2, col_delete);
    for (row = 0; row < mat2->row_size; row++) {
      for (col = 0; col < mat2->column_size; col++) {
	c = (col >= col_delete ? col + 1 : col);
	cut_assert_equal_int(data[row * mat->column_size + c], lil_matrix_element(mat2, row, col));
      }
    }
    cut_assert(lil_matrix_valid_p(mat2));
    lil_matrix_clear(mat2);
  }
  lil_matrix_clear(mat);
}

void test_mpz_transform_exchange_row ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, -1, 1, 2, 2, 3 };
  int row_data2[] = { 0, 4, 1, -5, 2, -6 };
  int row_data3[] = { 0, -7, 1, 8, 2, 9 };

  mat = lil_matrix_new(3, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 3, row_data1);
  lil_matrix_row_set(mat, 1, 3, row_data2);
  lil_matrix_row_set(mat, 2, 3, row_data3);
  lil_matrix_row_exchange(mat, 0, 2);

  cut_assert_equal_int(-7, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(8, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(9, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(4, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-6, lil_matrix_element(mat, 1, 2));
  cut_assert_equal_int(-1, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(2, lil_matrix_element(mat, 2, 1));
  cut_assert_equal_int(3, lil_matrix_element(mat, 2, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

/* void test_mpz_transform_exchange_column () */
/* { */
/*   LIL_MATRIX *mat; */
/*   int row_data1[] = { 0, -1, 1, 2, 2, 3 }; */
/*   int row_data2[] = { 0, 4, 1, -5, 2, -6 }; */
/*   int row_data3[] = { 0, -7, 1, 8, 2, 9 }; */

/*   mat = lil_matrix_new(3, 3, MODULO_USE_MPZ); */
/*   lil_matrix_row_set(mat, 0, 3, row_data1); */
/*   lil_matrix_row_set(mat, 1, 3, row_data2); */
/*   lil_matrix_row_set(mat, 2, 3, row_data3); */
/*   lil_matrix_column_exchange(mat, 0, 2); */

/*   cut_assert_equal_int(3, lil_matrix_element(mat, 0, 0)); */
/*   cut_assert_equal_int(2, lil_matrix_element(mat, 0, 1)); */
/*   cut_assert_equal_int(-1, lil_matrix_element(mat, 0, 2)); */
/*   cut_assert_equal_int(-6, lil_matrix_element(mat, 1, 0)); */
/*   cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 1)); */
/*   cut_assert_equal_int(4, lil_matrix_element(mat, 1, 2)); */
/*   cut_assert_equal_int(9, lil_matrix_element(mat, 2, 0)); */
/*   cut_assert_equal_int(8, lil_matrix_element(mat, 2, 1)); */
/*   cut_assert_equal_int(-7, lil_matrix_element(mat, 2, 2)); */
/*   cut_assert(lil_matrix_valid_p(mat)); */

/*   lil_matrix_clear(mat); */
/* } */

/* void test_mpz_transform_exchange_column2 () */
/* { */
/*   int data[] = { */
/*     3, 0, 0, -2, -1, 0, 0, */
/*     0, 0, 0, 0, 1, 2, 0, */
/*     -1, 1, 0, 0, 0, 0, 3, */
/*     2, 1, 0, 3, 0, 0, 0 */
/*   }; */
/*   LIL_MATRIX *mat, *mat2; */
/*   int column_size, col1, col2, row, col, c; */
/*   column_size = 7; */
/*   mat = lil_matrix_new2(4, column_size, MODULO_USE_MPZ, data); */
/*   for (col1 = 0; col1 < column_size - 1; col1++) { */
/*     for (col2 = (col1 + 1); col2 < column_size; col2++) { */
/*       mat2 = lil_matrix_copy(mat); */
/*       lil_matrix_column_exchange(mat2, col1, col2); */
/*       for (row = 0; row < mat2->row_size; row++) { */
/*       	for (col = 0; col < mat2->column_size; col++) { */
/*       	  if (col == col1) { */
/*       	    c = col2; */
/*       	  } else if (col == col2){ */
/*       	    c = col1; */
/*       	  } else { */
/*       	    c = col; */
/*       	  } */
/*       	  cut_assert_equal_int(data[row * mat->column_size + c], lil_matrix_element(mat2, row, col)); */
/*       	} */
/*       } */
/*       cut_assert(lil_matrix_valid_p(mat2)); */
/*       lil_matrix_clear(mat2); */
/*     } */
/*   } */
/*   lil_matrix_clear(mat); */
/* } */

void test_mpz_calculation_divide_elements ()
{
  int elements[] = {
    8, -2, 0,
    -4, 3,  2
  };
  LIL_MATRIX *mat;
  mat = lil_matrix_new2(2, 3, MODULO_USE_MPZ, elements);
  lil_matrix_divide_elements(mat, 3);

  cut_assert_equal_int(2, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(-1, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(1, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(0, lil_matrix_element(mat, 1, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_calculation_divide_elements_mpz ()
{
  int elements[] = {
    8, -2, 0,
    -4, 3,  2
  };
  LIL_MATRIX *mat;
  mpz_t num;
  mpz_init(num);
  mpz_set_si(num, 3);
  mat = lil_matrix_new2(2, 3, MODULO_USE_MPZ, elements);
  lil_matrix_divide_elements_mpz(mat, num);

  cut_assert_equal_int(2, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(-1, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(1, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(0, lil_matrix_element(mat, 1, 2));
  cut_assert(lil_matrix_valid_p(mat));

  mpz_clear(num);
  lil_matrix_clear(mat);
}

void test_mpz_calculation_multiple_factor_to_row ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, -1, 1, 2, 2, 3 };
  int row_data2[] = { 0, 4, 1, -5, 2, -6 };
  int row_data3[] = { 0, -7, 1, 8, 2, 9 };

  mat = lil_matrix_new(3, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 3, row_data1);
  lil_matrix_row_set(mat, 1, 3, row_data2);
  lil_matrix_row_set(mat, 2, 3, row_data3);
  lil_matrix_row_mul_scalar(mat, 1, -1);

  cut_assert_equal_int(-1, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(2, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(3, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(-4, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(5, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(6, lil_matrix_element(mat, 1, 2));
  cut_assert_equal_int(-7, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(8, lil_matrix_element(mat, 2, 1));
  cut_assert_equal_int(9, lil_matrix_element(mat, 2, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_calculation_multiple_factor_to_column ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, -1, 1, 2, 2, 3 };
  int row_data2[] = { 0, 4, 1, -5, 2, -6 };
  int row_data3[] = { 0, -7, 1, 8, 2, 9 };

  mat = lil_matrix_new(3, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 3, row_data1);
  lil_matrix_row_set(mat, 1, 3, row_data2);
  lil_matrix_row_set(mat, 2, 3, row_data3);
  lil_matrix_column_mul_scalar(mat, 1, -1);

  cut_assert_equal_int(-1, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(-2, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(3, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(4, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(5, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-6, lil_matrix_element(mat, 1, 2));
  cut_assert_equal_int(-7, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(-8, lil_matrix_element(mat, 2, 1));
  cut_assert_equal_int(9, lil_matrix_element(mat, 2, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_calculation_add_rows ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, -1, 1, 2, 2, 3 };
  int row_data2[] = { 0, 4, 1, -5, 2, -6 };
  int row_data3[] = { 0, -7, 1, 8, 2, 9 };

  mat = lil_matrix_new(3, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 3, row_data1);
  lil_matrix_row_set(mat, 1, 3, row_data2);
  lil_matrix_row_set(mat, 2, 3, row_data3);
  lil_matrix_add_rows(mat, 0, 2, -2);

  cut_assert_equal_int(13, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(-14, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(-15, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(4, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-6, lil_matrix_element(mat, 1, 2));
  cut_assert_equal_int(-7, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(8, lil_matrix_element(mat, 2, 1));
  cut_assert_equal_int(9, lil_matrix_element(mat, 2, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_calculation_add_rows2 ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 1, 2 };
  int row_data2[] = { 0, 4, 2, -5 };

  mat = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 1, row_data1);
  lil_matrix_row_set(mat, 1, 2, row_data2);
  lil_matrix_add_rows(mat, 0, 1, -3);

  cut_assert_equal_int(-12, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(2, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(15, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(4, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(0, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_calculation_add_rows3 ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, 4, 2, -5 };
  int row_data2[] = { 0, 2, 1, 2 };

  mat = lil_matrix_new(2, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 2, row_data1);
  lil_matrix_row_set(mat, 1, 2, row_data2);
  lil_matrix_add_rows(mat, 0, 1, -2);

  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(-4, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(2, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(2, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(0, lil_matrix_element(mat, 1, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_calculation_add_rows4 ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, 3, 1, 6 };
  int row_data2[] = { 0, -6, 1, -12 };

  mat = lil_matrix_new(2, 2, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 2, row_data1);
  lil_matrix_row_set(mat, 1, 2, row_data2);
  lil_matrix_add_rows(mat, 1, 0, 2);

  cut_assert_equal_int(3, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(6, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(0, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(0, lil_matrix_element(mat, 1, 1));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_calculation_add_rows5 ()
{
  int data[] = {
    3, 0, 0, -2,
    -1, 0, 0, 0,
    0, 0, 0, 1,
    2, 0, -1, 1,
    0, 0, 0, 0,
    3, 2, 1, 0,
    3, 0, 0, 0
  };
  LIL_MATRIX *mat, *mat2;
  int row1, row2, row, col, val, factor;
  factor = 2;
  mat = lil_matrix_new2(7, 4, MODULO_USE_MPZ, data);
  for (row1 = 0; row1 < mat->row_size - 1; row1++) {
    for (row2 = 0; row2 < mat->row_size; row2++) {
      if (row1 != row2) {
	mat2 = lil_matrix_copy(mat);
	lil_matrix_add_rows(mat2, row1, row2, factor);
	for (row = 0; row < mat2->row_size; row++) {
	  for (col = 0; col < mat2->column_size; col++) {
	    val = data[row * mat->column_size + col];
	    if (row == row1) {
	      val += (2 * data[row2 * mat->column_size + col]);
	    }
	    cut_assert_equal_int(val, lil_matrix_element(mat2, row, col));
	  }
	}
	cut_assert(lil_matrix_valid_p(mat2));
	lil_matrix_clear(mat2);
      }
    }
  }
  lil_matrix_clear(mat);
}

void test_mpz_calculation_add_rows_mpz ()
{
  LIL_MATRIX *mat;
  int elements[] = {
    -1, 2, 3,
    4, -5, -6,
    -7, 8, 9
  };
  mpz_t num;
  mpz_init(num);
  mpz_set_si(num, -2);
  mat = lil_matrix_new2(3, 3, MODULO_USE_MPZ, elements);
  lil_matrix_add_rows_mpz(mat, 0, 2, num);

  cut_assert_equal_int(13, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(-14, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(-15, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(4, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-6, lil_matrix_element(mat, 1, 2));
  cut_assert_equal_int(-7, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(8, lil_matrix_element(mat, 2, 1));
  cut_assert_equal_int(9, lil_matrix_element(mat, 2, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
  mpz_clear(num);
}

void test_mpz_calculation_add_columns ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, -1, 1, 2, 2, 3 };
  int row_data2[] = { 0, 4, 1, -5, 2, -6 };
  int row_data3[] = { 0, -7, 1, 8, 2, 9 };

  mat = lil_matrix_new(3, 3, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 3, row_data1);
  lil_matrix_row_set(mat, 1, 3, row_data2);
  lil_matrix_row_set(mat, 2, 3, row_data3);
  lil_matrix_add_columns(mat, 0, 2, -2);

  cut_assert_equal_int(-7, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(2, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(3, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(16, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-6, lil_matrix_element(mat, 1, 2));
  cut_assert_equal_int(-25, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(8, lil_matrix_element(mat, 2, 1));
  cut_assert_equal_int(9, lil_matrix_element(mat, 2, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_calculation_add_columns2 ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, -1 };
  int row_data2[] = { 1, -5 };
  int row_data3[] = { 0, -7 };

  mat = lil_matrix_new(3, 2, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 1, row_data1);
  lil_matrix_row_set(mat, 1, 1, row_data2);
  lil_matrix_row_set(mat, 2, 1, row_data3);
  lil_matrix_add_columns(mat, 1, 0, -3);

  cut_assert_equal_int(-1, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(3, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(0, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-7, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(21, lil_matrix_element(mat, 2, 1));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_calculation_add_columns3 ()
{
  LIL_MATRIX *mat;
  int row_data1[] = { 0, -1, 1, -3 };
  int row_data2[] = { 1, -5 };
  int row_data3[] = { 0, -7 };

  mat = lil_matrix_new(3, 2, MODULO_USE_MPZ);
  lil_matrix_row_set(mat, 0, 2, row_data1);
  lil_matrix_row_set(mat, 1, 1, row_data2);
  lil_matrix_row_set(mat, 2, 1, row_data3);
  lil_matrix_add_columns(mat, 1, 0, -3);

  cut_assert_equal_int(-1, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(0, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(0, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-7, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(21, lil_matrix_element(mat, 2, 1));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
}

void test_mpz_calculation_add_columns4 ()
{
  int data[] = {
    3, 0, 0, -2, -1, 0, 0,
    0, 0, 0, 0, 1, 2, 0,
    -1, 1, 0, 0, 0, 0, 3,
    2, 1, 0, 3, 0, 0, 0
  };
  LIL_MATRIX *mat, *mat2;
  int column_size, col1, col2, row, col, val, factor;
  column_size = 7;
  factor = 2;
  mat = lil_matrix_new2(4, column_size, MODULO_USE_MPZ, data);
  for (col1 = 0; col1 < column_size - 1; col1++) {
    for (col2 = 0; col2 < column_size; col2++) {
      if (col1 != col2) {
	mat2 = lil_matrix_copy(mat);
	lil_matrix_add_columns(mat2, col1, col2, factor);
	for (row = 0; row < mat2->row_size; row++) {
	  for (col = 0; col < mat2->column_size; col++) {
	    val = data[row * mat->column_size + col];
	    if (col == col1) {
	      val += (2 * data[row * mat->column_size + col2]);
	    }
	    cut_assert_equal_int(val, lil_matrix_element(mat2, row, col));
	  }	
	}
	cut_assert(lil_matrix_valid_p(mat2));
	lil_matrix_clear(mat2);
      }
    }
  }
  lil_matrix_clear(mat);
}

void test_mpz_calculation_add_columns_mpz ()
{
  LIL_MATRIX *mat;
  int elements[] = {
    -1, 2, 3,
    4, -5, -6,
    -7, 8, 9
  };
  mpz_t num;
  mpz_init(num);
  mpz_set_si(num, -2);

  mat = lil_matrix_new2(3, 3, MODULO_USE_MPZ, elements);
  lil_matrix_add_columns_mpz(mat, 0, 2, num);

  cut_assert_equal_int(-7, lil_matrix_element(mat, 0, 0));
  cut_assert_equal_int(2, lil_matrix_element(mat, 0, 1));
  cut_assert_equal_int(3, lil_matrix_element(mat, 0, 2));
  cut_assert_equal_int(16, lil_matrix_element(mat, 1, 0));
  cut_assert_equal_int(-5, lil_matrix_element(mat, 1, 1));
  cut_assert_equal_int(-6, lil_matrix_element(mat, 1, 2));
  cut_assert_equal_int(-25, lil_matrix_element(mat, 2, 0));
  cut_assert_equal_int(8, lil_matrix_element(mat, 2, 1));
  cut_assert_equal_int(9, lil_matrix_element(mat, 2, 2));
  cut_assert(lil_matrix_valid_p(mat));

  lil_matrix_clear(mat);
  mpz_clear(num);
}

/* static void check_snf(int row_size, int column_size, int *elements, int snf_size, int *snf_result) */
/* { */
/*   GArray *snf; */
/*   LIL_MATRIX *mat; */
/*   int i; */

/*   mat = lil_matrix_new2(row_size, column_size, MODULO_USE_MPZ, elements); */
/*   snf = lil_matrix_snf2(mat); */

/*   cut_assert_equal_int(snf_size, snf->len); */
/*   for (i = 0; i < snf_size; i++) { */
/*     cut_assert_equal_int(snf_result[i], g_array_index(snf, int, i)); */
/*   } */

/*   g_array_free(snf, FALSE); */
/*   lil_matrix_clear(mat); */
/* } */

/* void test_mpz_snf_calculate () */
/* { */
/*   int elements[] = { */
/*     1, 2, 3, */
/*     4, 5, 6, */
/*     7, 8, 9 */
/*   }; */
/*   int result[] = { 1, 3, 0 }; */
/*   check_snf(3, 3, elements, 3, result); */
/* } */

/* void test_mpz_snf_calculate2 () */
/* { */
/*   int elements[] = { */
/*     8, -2, 0,  0, 1, */
/*     0, 0,  2,  1, 0, */
/*     0, 2,  0,  0, 0, */
/*     2, -1, -3, 0, 3, */
/*   }; */
/*   int result[] = { 1, 1, 1, 2 }; */
/*   check_snf(4, 5, elements, 4, result); */
/* } */
