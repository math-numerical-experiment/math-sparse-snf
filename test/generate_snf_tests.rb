require "erb"
require "tempfile"

def matrix_array_get(row, column, max_num)
  row.times.map do |i|
    column.times.map do |j|
      if rand < 0.2
        0
      else
        n = rand(max_num)
        if rand < 0.5
          -n
        else
          n
        end
      end
    end
  end
end

header =<<HEADER
#include <gcutter.h>
#include <lilmatrix.h>

static void check_snf(int row_size, int column_size, int *elements, int snf_size, int *snf_result)
{
  GArray *snf;
  LIL_MATRIX *mat;
  int i;

  mat = lil_matrix_new2(row_size, column_size, elements);
  snf = lil_matrix_snf2(mat);

  cut_assert_equal_int(snf_size, snf->len);
  for (i = 0; i < snf_size; i++) {
    cut_assert_equal_int(snf_result[i], g_array_index(snf, int, i));
  }

  g_array_free(snf, FALSE);
  lil_matrix_clear(mat);
}

HEADER

puts header

test_count = 1
[[10, 10, 100, 3], [10, 12, 100, 5]].each do |ary|
  row_size, column_size, test_num, max_num = ary

  template =<<TEMPLATE
void test_snf_calculate_<%= now %>_<%= test_count %> ()
{
  int elements[] = { <%= mat.flatten.join(",") %>};
  int result[] = { <%= snf_result.join(",") %> };
  check_snf(<%= mat.size %>, <%= mat[0].size %>, elements, <%= snf_result.size %>, result);
}
TEMPLATE

  now = Time.now.to_i
  test_num.times do |i|
    mat = matrix_array_get(row_size, column_size, max_num)
    tmp = Tempfile.new("generate_test")
    tmp.puts <<GAP
m:=#{mat.inspect};
Print(SmithNormalFormIntegerMat(m));
QUIT;
GAP
    tmp.close
    result = `gap -q #{tmp.path}`
    snf = eval(result)
    snf_result = [snf.size, snf[0].size].min.times.map do |j|
      snf[j][j]
    end
    snf_result.delete_if { |i| i == 0 }
    e = ERB.new(template)
    puts e.result(binding)
    test_count += 1
  end
end
