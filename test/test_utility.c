#include <gcutter.h>
#include <lilmatrix.h>
#include <intern.h>

GArray *get_array_example1 ()
{
  GArray *ary;
  ary = g_array_sized_new(false, false, sizeof(int), 6);
  ary->len = 6;
  /* { 1, 3, 4, 5, 8, 10}; */
  g_array_index(ary, int, 0) = 1;
  g_array_index(ary, int, 1) = 3;
  g_array_index(ary, int, 2) = 4;
  g_array_index(ary, int, 3) = 5;
  g_array_index(ary, int, 4) = 8;
  g_array_index(ary, int, 5) = 10;
  return ary;
}

void test_utility_array_find_number1 ()
{
  int ind;
  bool find_p;
  GArray *ary;
  ary = get_array_example1();
  ind = array_find_number(ary, 3, &find_p);
  cut_assert_equal_int(1, ind);
  cut_assert(find_p);
  g_array_free(ary, TRUE);
}

void test_utility_array_find_number2 ()
{
  int ind;
  bool find_p;
  GArray *ary;
  ary = get_array_example1();
  ind = array_find_number(ary, 2, &find_p);
  cut_assert_equal_int(1, ind);
  cut_assert(!find_p);
  g_array_free(ary, TRUE);
}

void test_utility_array_find_number3 ()
{
  int ind;
  bool find_p;
  GArray *ary;
  ary = get_array_example1();
  ind = array_find_number(ary, 1, &find_p);
  cut_assert_equal_int(0, ind);
  cut_assert(find_p);
  g_array_free(ary, TRUE);
}

void test_utility_array_find_number4 ()
{
  int ind;
  bool find_p;
  GArray *ary;
  ary = get_array_example1();
  ind = array_find_number(ary, 0, &find_p);
  cut_assert_equal_int(0, ind);
  cut_assert(!find_p);
  g_array_free(ary, TRUE);
}

void test_utility_array_find_number5 ()
{
  int ind;
  bool find_p;
  GArray *ary;
  ary = get_array_example1();
  ind = array_find_number(ary, 100, &find_p);
  cut_assert_equal_int(ARRAY_ALL_LESS, ind);
  cut_assert(!find_p);
  g_array_free(ary, TRUE);
}

void test_utility_array_find_number6 ()
{
  int ind;
  bool find_p;
  GArray *ary;
  ary = get_array_example1();
  ind = array_find_number(ary, 10, &find_p);
  cut_assert_equal_int(5, ind);
  cut_assert(find_p);
  g_array_free(ary, TRUE);
}

void test_utility_array_find_number7 ()
{
  int ind, val;
  bool find_p;
  GArray *ary;
  ary = g_array_new(false, false, sizeof(int));
  val = 1;
  g_array_append_val(ary, val);
  val = 2;
  g_array_append_val(ary, val);
  ind = array_find_number(ary, 1, &find_p);
  cut_assert_equal_int(0, ind);
  cut_assert(find_p);
  g_array_free(ary, TRUE);
}

void test_utility_array_find_number8 ()
{
  int ind, val;
  bool find_p;
  GArray *ary;
  ary = g_array_new(false, false, sizeof(int));
  val = 1;
  g_array_append_val(ary, val);
  val = 2;
  g_array_append_val(ary, val);
  ind = array_find_number(ary, 2, &find_p);
  cut_assert_equal_int(1, ind);
  cut_assert(find_p);
  g_array_free(ary, TRUE);
}

void test_utility_array_find_number_empty ()
{
  int ind;
  bool find_p;
  GArray *ary;
  ary = g_array_new(false, false, sizeof(int));
  ind = array_find_number(ary, 0, &find_p);
  cut_assert_equal_int(ARRAY_EMPTY, ind);
  cut_assert(!find_p);
  g_array_free(ary, TRUE);
}
