# sparse_snf

C programs to calculate Smith normal forms of sparse matrices,
which is used to calculate finite type invariants of nanowords in math-npw.

## Requirements

- glib-2.0
- argtable2
- gmp
- mpi

On Ubunt 13.10, we install packages to compile this program;

    apt-get install libargtable2-dev libgmp-dev libglib2.0-dev libopenmpi-dev openmpi-bin

In addition, we need some macros of autoconf;

    apt-get install autoconf-archive

## Compile

Just once, to create configure script, we execute

    ./autogen.sh

Then, we compile source files by the following commands;

    ./configure
    make

In the directory "src", commands are created.
The following commands show help messages;

    src/sparse_snf --help
    src/mpi/sparse_snf_mpi --help

## License

GPLv3

## Author

Takayuki YAMAGUCHI <d@ytak.info>
